extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;
extern crate structopt;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;
use structopt::StructOpt;

fn average(times: &[u64]) -> u64 {
    times.iter().sum::<u64>() / times.len() as u64
}

quick_main!(run);

fn run() -> Result<()> {
    //
    // Process command line arguments
    //
    let arguments = VXGIArguments::from_args();

    // Initialize application (window, context, input, ui)
    let mut app = AppBuilder::new().with_title("VXGI example").build();
    let mut input = Input::default();

    app.renderer.set_disable(EnableOption::DepthTest);
    app.renderer.set_disable(EnableOption::FramebufferSRGB);

    // Load scene
    let shape_list =
        ShapeList::from_files(arguments.files.as_slice(), None).chain_err(|| "Could not create shape list")?;

    // - Camera
    let mut camera = FreeCamera::new(
        Point3::new(-0.51, 0.15, 0.0),
        Rad(0.0),
        Rad(0.0),
        Vector3::new(0.0, 1.0, 0.0),
        0.01,
        Rad(0.7175409424302455f32),
        app.width as usize,
        app.height as usize,
        0.01,
        10.0,
    ).chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    // - Sun
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(1.8877113),
                height_angle: Rad(1.129871),
            },
            8192,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Could not render shadowmap")?;

    // Deferred
    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;

    // - Camera
    camera.set_viewport(app.width, app.height);
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let mut timer_query = TimerQuery::new();

    let number_of_runs = 10;

    for voxel_structure in [VoxelStructure::VolumeTexture, VoxelStructure::SparseVoxelOctree].iter() {
        for anisotropic in [false, true].iter() {
            for hdr in [false, true].iter() {
                for levels in 7..=9 {
                    print!(
                        "{},{},{},{}: ",
                        if *voxel_structure == VoxelStructure::VolumeTexture {
                            "Volume Texture"
                        } else {
                            "SVO"
                        },
                        if *anisotropic { "Anisotropic" } else { "Isotropic" },
                        if *hdr { "HDR" } else { "LDR" },
                        levels
                    );

                    if (levels == 9 && *hdr && *anisotropic) || (levels == 9 && *hdr) {
                        println!("-,-,-,-,-");
                        continue;
                    }

                    let mut options = VXGIOptions::new();
                    options.set_levels(levels);
                    if *anisotropic {
                        options.set_anisotropic();
                    } else {
                        options.set_isotropic();
                    }
                    if *hdr {
                        options.set_hdr();
                    } else {
                        options.set_ldr();
                    }
                    options.voxel_structure = *voxel_structure;

                    let mut vxgi_module: Box<VXGIModule> = match (voxel_structure, anisotropic) {
                        (VoxelStructure::VolumeTexture, false) => {
                            Box::new(IsotropicTextureGI::new(options).chain_err(|| "Could not create VXGI module")?)
                        }
                        (VoxelStructure::VolumeTexture, true) => {
                            Box::new(AnisotropicTextureGI::new(options).chain_err(|| "Could not create VXGI module")?)
                        }
                        (VoxelStructure::SparseVoxelOctree, false) => {
                            Box::new(IsotropicSVOGI::new(options).chain_err(|| "Could not create VXGI module")?)
                        }
                        (VoxelStructure::SparseVoxelOctree, true) => {
                            Box::new(AnisotropicSVOGI::new(options).chain_err(|| "Could not create VXGI module")?)
                        }
                    };

                    unsafe {
                        gl::Finish();
                    }

                    // Voxelize
                    let mut runs = Vec::new();
                    for _ in 0..number_of_runs {
                        timer_query.begin();
                        vxgi_module
                            .voxelize(&mut app.renderer, &[&shape_list], true)
                            .chain_err(|| "Could not voxelize geometry")?;
                        runs.push(timer_query.end_ns());
                    }
                    print!("& {:.2} ", average(&runs) as f64 / 1_000_000f64);

                    // Inject radiance
                    let mut runs = Vec::new();
                    for _ in 0..number_of_runs {
                        timer_query.begin();
                        vxgi_module.inject_radiance(&mut app.renderer, &sun).chain_err(|| "")?;
                        runs.push(timer_query.end_ns());
                    }
                    print!("& {:.2} ", average(&runs) as f64 / 1_000_000f64);

                    for cones_num in [3, 9, 18].iter() {
                        // Clear default framebuffer
                        app.renderer.clear_default_framebuffer(ClearBuffers::Color);

                        options.calculate_indirect_specular = 0;
                        options.calculate_ambient_occlusion = 0;
                        options.set_cones(*cones_num, ConeDistribution::ConcentricRegular);
                        vxgi_module.update_options(options);

                        deferred_collector
                            .render(&mut app.renderer, &[&shape_list], &camera)
                            .chain_err(|| "Could not render deferred textures")?;

                        unsafe {
                            gl::Finish();
                        }

                        let mut runs = Vec::new();
                        for _ in 0..number_of_runs {
                            timer_query.begin();
                            vxgi_module
                                .render(&mut app.renderer, &deferred_collector, &camera, &sun)
                                .chain_err(|| "Could not create finalrender")?;
                            runs.push(timer_query.end_ns());

                            app.swap_buffers();
                        }
                        print!("& {:.2} ", average(&runs) as f64 / 1_000_000f64);
                    }

                    println!("\\\\ ");

                    app.swap_buffers();
                }
            }
        }
    }

    Ok(())
}
