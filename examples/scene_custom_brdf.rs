extern crate cgmath;
extern crate glutin;
extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;

use cgmath::{Deg, Point3, Rad, Vector3};
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new().with_title("Scene example").build();
    let mut input = Input::default();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_enable(EnableOption::DepthTest);
    app.renderer.set_enable(EnableOption::CullFace);

    // Load scene
    let files = std::env::args().skip(1).map(|s| s.clone()).collect::<Vec<String>>();
    if files.len() == 0 {
        panic!("usage: gltf-display <PATH>");
    }

    let mut shape_list = ShapeList::from_files(
        files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    ).chain_err(|| "Could not create shape list")?;

    for shape in &mut shape_list.primitive_parameters {
        shape.has_diffuse_ibl = 1;
        shape.has_specular_ibl = 1;
    }

    shape_list
        .update_buffers()
        .chain_err(|| "Could not update shape list buffers")?;

    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(0.0),
            height_angle: Deg(45.0).into(),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    ).chain_err(|| "Could not create camera")?;

    let walter_geometry = "
    float cook_torrance_geometry(vec3 N, vec3 V, vec3 L, float roughness) {
        vec3 H = normalize(V + L);

        float NdotV= dot(N, V);
        float NdotL= dot(N, L);
        float HdotV= dot(H, V);
        float HdotL= dot(H, L);
        float NdotV_clamped = max(NdotV, 0.0);

        float a = 1.0 / (roughness * tan(acos(NdotV_clamped)));
        float a_squared= a * a;
        float a_term;
        if (a < 1.6) {
            a_term= (3.535 * a + 2.181 * a_squared) / (1.0 + 2.276 * a + 2.577 * a_squared);
        } else {
            a_term= 1.0;
        }
        return step(0.0, HdotL/NdotL) * step(0.0, HdotV/NdotV) * a_term * a_term;
    }
    ";

    let mut scene_renderer =
        SceneRenderer::with_custom_brdf(&mut app.renderer, &build_brdf(None, None, Some(walter_geometry)))
            .chain_err(|| "Could not create scene renderer")?;

    // Lights
    let sun_module = SunModule::new().chain_err(|| "Could not initialize Sun module")?;
    let mut sun = sun_module
        .create_sun(
            Vector3::new(2.0, 2.0, 2.0),
            SunPosition {
                y_angle: Rad(0.0),
                height_angle: Rad(3.14159265 / 2.0),
            },
            2048,
        )
        .chain_err(|| "Could not initialize the Sun")?;

    let mut light_group = LightGroup::new(vec![Light::new(
        2048,
        Point3::new(0.0, 1.0, 0.0),
        10.0,
        Vector3::new(1.0, 1.0, 1.0),
    ).chain_err(|| "Could not create light")?])
        .chain_err(|| "Could not create light group")?;

    // IBL
    let skybox = Skybox::from_file(&mut app.renderer, &"assets/hdr/papermill.hdr", 1024)
        .chain_err(|| "Could not create skybox")?;

    let (ibl_diffuse_texture, ibl_specular_texture) =
        create_diffuse_specular_ibl(&mut app.renderer, &skybox.skybox, 32, 128)
            .chain_err(|| "Could not create ibl textures")?;
    shape_list.ibl_diffuse_texture = Some(ibl_diffuse_texture);
    shape_list.ibl_specular_texture = Some(ibl_specular_texture);

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Coult not render sun's shadowmap")?;

    light_group
        .render_shadowmaps(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Could not render shadowmaps")?;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1000000000.0;

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        skybox
            .render(&mut app.renderer, &camera)
            .chain_err(|| "Could not render skybox")?;

        scene_renderer
            .render(&mut app.renderer, &[&shape_list], &camera, Some(&light_group), None)
            .chain_err(|| "Could not render the scene")?;

        app.swap_buffers();

        for event in app.poll_events() {
            input.process_event(&event);
            camera
                .process_event(&event, &input, delta_time)
                .chain_err(|| "Could not process event in camera")?;

            match event {
                Event::WindowEvent {
                    event: glutin::WindowEvent::Resized(width, height),
                    ..
                } => if width != 0 && height != 0 {
                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: width as usize,
                        height: height as usize,
                    });
                },
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        previous_time = current_time;
    }

    Ok(())
}
