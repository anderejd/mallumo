mod app_ui {
    use mallumo::conrod::*;
    use mallumo::*;
    use std;

    widget_ids! {
        struct Ids {
            canvas,
            slider_camera_angle_x_title,
            slider_camera_angle_x,
            slider_camera_angle_y_title,
            slider_camera_angle_y,
        }
    }

    pub struct Variables {
        pub camera_angle_x: f32,
        pub camera_angle_y: f32,
        pub update_camera: bool,
    }

    pub struct AppUi {
        ui: Ui,
        ids: self::Ids,
        image_map: image::Map<Texture2D>,
        pub variables: self::Variables,
    }

    impl AppUi {
        pub fn new(width: usize, height: usize) -> AppUi {
            let mut ui = UiBuilder::new([width as f64, height as f64]).build();

            let ids = self::Ids::new(ui.widget_id_generator());

            // Add fonts to ui
            // ...

            // The image map describing widget->image mappings
            let image_map = image::Map::<Texture2D>::new();

            AppUi {
                ui: ui,
                ids: ids,
                image_map: image_map,
                variables: Variables {
                    camera_angle_x: std::f32::consts::PI / 4.0f32,
                    camera_angle_y: 1.0766133,
                    update_camera: false,
                },
            }
        }

        pub fn get_ui<'a>(&'a self) -> &'a Ui {
            &self.ui
        }

        pub fn get_image_map<'a>(&'a self) -> &'a image::Map<Texture2D> {
            &self.image_map
        }

        pub fn process_event(&mut self, event: glutin::Event, window: &glutin::GlWindow) {
            let input = match convert_event(event, &window) {
                None => return,
                Some(input) => input,
            };

            self.ui.handle_event(input);
        }

        pub fn update_ui(&mut self) {
            let ui = &mut self.ui.set_widgets();

            widget::Canvas::new()
                .w(200.0)
                .pad(10.0)
                .top_left_of(ui.window)
                .color(Color::Rgba(0.2, 0.2, 0.2, 1.0))
                .set(self.ids.canvas, ui);

            if let Some(camera_angle_x) = widget::Slider::new(self.variables.camera_angle_x, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .rgb(1.0, 0.3, 0.6)
                .set(self.ids.slider_camera_angle_x, ui)
            {
                if self.variables.camera_angle_x != camera_angle_x {
                    self.variables.update_camera = true;
                }
                self.variables.camera_angle_x = camera_angle_x;
            }

            if let Some(camera_angle_y) = widget::Slider::new(self.variables.camera_angle_y, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .down(30.0)
                .rgb(0.0, 0.3, 1.0)
                .set(self.ids.slider_camera_angle_y, ui)
            {
                if self.variables.camera_angle_y != camera_angle_y {
                    self.variables.update_camera = true;
                }
                self.variables.camera_angle_y = camera_angle_y;
            }
        }
    }
}

extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

use std::path::Path;
use std::str::*;

quick_main!(run);

fn run() -> Result<()> {
    // Constants
    let diffuse_cones: Vec<f32> = vec![
        0.000000,
        0.654741,
        0.755853,
        0.713840, //
        -0.654588,
        0.654741,
        -0.377927,
        0.713840, //
        0.654588,
        0.654741,
        -0.377927,
        0.713840, // 

        // 0.000000, 0.500000, 0.866025, 0.523599, // 
        // -0.823639, 0.500000, 0.267617, 0.523599, // 
        // -0.509037, 0.500000, -0.700629, 0.523599, // 
        // 0.509037, 0.500000, -0.700629, 0.523599, // 
        // 0.823639, 0.500000, 0.267617, 0.523599, // 
        // 0.000000, 1.000000, 0.000000, 0.523599, //

        // 0.000000, 1.000000, 0.000000, 0.307178, 0.000000, 0.302370, 0.953191, 0.307178, -0.612699, 0.302370, 0.730186,
        // 0.307178, -0.938710, 0.302370, 0.165520, 0.307178, -0.825487, 0.302370, -0.476595, 0.307178, -0.326010,
        // 0.302370, -0.895706, 0.307178, 0.326010, 0.302370, -0.895706, 0.307178, 0.825487, 0.302370, -0.476595,
        // 0.307178, 0.938710, 0.302370, 0.165520, 0.307178, 0.612699, 0.302370, 0.730186, 0.307178, -0.104988, 0.796530,
        // 0.595414, 0.307178, -0.568137, 0.796530, 0.206785, 0.307178, -0.463150, 0.796530, -0.388629, 0.307178,
        // 0.104988, 0.796530, -0.595414, 0.307178, 0.568137, 0.796530, -0.206785, 0.307178, 0.463150, 0.796530, 0.388629,
        // 0.307178,
    ];

    let levels: usize = 8;

    let mut screen_position: [f32; 2] = [0.0, 0.0];

    let mut app = AppBuilder::new()
        .with_title("SVOGI example")
        // .with_fullscreen(true)
        // .with_dimensions(1920, 1080)
        .build();
    let mut input = Input::default();

    let mut gui_renderer = GuiRenderer::new(app.width, app.height, app.gl_window.hidpi_factor() as f64)
        .chain_err(|| "Unable to create GUI renderer")?;
    let mut app_ui = app_ui::AppUi::new(app.width, app.height);

    let gi_globals = MutableBuffer::new(&[2u32.pow(levels as u32), levels as u32, 0u32, 0u32])
        .chain_err(|| "Could not creat GI globals buffer")?;

    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;

    // Render cone pipeline
    let cone_vert = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/debug/cone/cone_deferred.vert")],
    ).chain_err(|| "Failed to compile cone vertex shader")?;
    let cone_geom = Shader::new(
        ShaderType::Geometry,
        &[include_str!("../assets/shaders/debug/cone/cone.geom")],
    ).chain_err(|| "Failed to compile cone geometry shader")?;
    let cone_frag = Shader::new(
        ShaderType::Fragment,
        &[include_str!("../assets/shaders/debug/cone/cone.frag")],
    ).chain_err(|| "Failed to compile cone fragment shader")?;
    let cone_pipeline = PipelineBuilder::new()
        .vertex_shader(&cone_vert)
        .geometry_shader(&cone_geom)
        .fragment_shader(&cone_frag)
        .build()
        .chain_err(|| "Unable to build cone render pipeline")?;

    // Store mips in cone pipeline
    let store_cone_mips_vert = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/debug/cone/store_cone_mips.vert")],
    ).chain_err(|| "Failed to compile store cone mips vertex shader")?;
    let store_cone_mips_pipeline = PipelineBuilder::new()
        .vertex_shader(&store_cone_mips_vert)
        .build()
        .chain_err(|| "Unable to build store cone mips pipeline")?;

    // Render cone mip maps pipeline
    let render_cone_mips_vert = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/debug/nodepool/cone_to_nodes.vert")],
    ).chain_err(|| "Failed to compile cone vertex shader")?;
    let render_cone_mips_geom = Shader::new(
        ShaderType::Geometry,
        &[include_str!("../assets/shaders/debug/nodepool/nodepool_to_voxels.geom")],
    ).chain_err(|| "Failed to compile cone geometry shader")?;
    let render_cone_mips_frag = Shader::new(
        ShaderType::Fragment,
        &[include_str!(
            "../assets/shaders/debug/nodepool/nodepool_to_voxels_blue.frag"
        )],
    ).chain_err(|| "Failed to compile cone fragment shader")?;
    let render_cone_mips_pipeline = PipelineBuilder::new()
        .vertex_shader(&render_cone_mips_vert)
        .geometry_shader(&render_cone_mips_geom)
        .fragment_shader(&render_cone_mips_frag)
        .build()
        .chain_err(|| "Unable to build cone mips render pipeline")?;

    let diffuse_cones_buffer =
        ImmutableBuffer::new(&diffuse_cones).chain_err(|| "Could not create diffuse cones buffer")?;

    let mut cones_globals_buffers = MutableBuffer::new_empty(8).chain_err(|| "Could not create global cones buffer")?;

    // Render GI pipeline
    let gi_final_render_vert = Shader::new(
        ShaderType::Vertex,
        &[include_str!(
            "../assets/shaders/global_illumination/svogi_final_render.vert"
        )],
    ).chain_err(|| "Failed to compile gi vertex deferred shader")?;
    let gi_final_render_frag = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            &build_brdf(None, None, None),
            include_str!("../assets/shaders/global_illumination/svogi_final_render.frag"),
        ],
    ).chain_err(|| "Failed to compile gi fragment shader")?;
    let gi_pipeline = PipelineBuilder::new()
        .vertex_shader(&gi_final_render_vert)
        .fragment_shader(&gi_final_render_frag)
        .build()
        .chain_err(|| "Unable to build svogi render pipeline")?;

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_enable(EnableOption::DepthTest);

    // Camera
    // let mut camera = SphereCamera::new(
    //     SpherePosition {
    //         radius: 0.25,
    //         y_angle: Deg(90.0).into(),
    //         height_angle: Deg(75.0).into(),
    //     },
    //     Point3::new(0.0, 0.0, 0.0),
    //     Vector3::new(0.0, 1.0, 0.0),
    //     Deg(45.0).into(),
    //     app.width as usize,
    //     app.height as usize,
    //     0.0001,
    //     100.0,
    // ).chain_err(|| "Could not create camera")?;

    let mut camera = FreeCamera::new(
        Point3::new(0.0, -0.25, 0.0),
        Rad(0.0),
        Rad(0.0),
        Vector3::new(0.0, 1.0, 0.0),
        0.01,
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.01,
        10.0,
    ).chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Unable to update buffer")?;

    let mut scene_renderer = SceneRenderer::new(&mut app.renderer).chain_err(|| "Could not create scene renderer")?;

    // Load Scene
    let files = std::env::args().skip(1).map(|s| s.clone()).collect::<Vec<String>>();

    if files.len() == 0 {
        bail!("usage: gltf-display <PATH>");
    }

    let shape_list = ShapeList::from_files(
        files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    ).chain_err(|| "Could not create shape list")?;

    let teapot_path: String = "./assets/teapot/teapot.obj".to_string();
    let mut dynamic_shape_list = ShapeList::from_file(
        &teapot_path,
        Some(Unitization {
            box_min: Point3::new(-0.050, -0.050, -0.050),
            box_max: Point3::new(0.050, 0.050, 0.050),
            unitize_if_fits: true,
        }),
    ).chain_err(|| "Could not create shape list")?;
    dynamic_shape_list.primitive_parameters[0].emission = cgmath::Vector4::new(0.0, 0.0, 0.8628, 1.0);
    dynamic_shape_list.primitive_parameters[1].emission = cgmath::Vector4::new(1.0, 0.0, 0.0, 1.0);
    dynamic_shape_list.primitive_parameters[0].model_matrix =
        cgmath::Matrix4::from_translation(Vector3::new(0.40, -0.30, 0.0))
            * dynamic_shape_list.primitive_parameters[0].model_matrix;
    dynamic_shape_list.primitive_parameters[1].model_matrix =
        cgmath::Matrix4::from_translation(Vector3::new(0.40, -0.30, 0.0))
            * dynamic_shape_list.primitive_parameters[1].model_matrix;
    dynamic_shape_list.update_buffers().chain_err(|| "")?;

    // Lights
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(app_ui.variables.camera_angle_x),
                height_angle: Rad(app_ui.variables.camera_angle_y),
            },
            4096,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .expect("Could not render shadowmaps");

    app.swap_buffers();

    let voxel_fragment_list_module =
        VoxelFragmentListModule::new().chain_err(|| "Could not initialize voxel fragment list module")?;
    let mut voxel_fragment_list_generator = voxel_fragment_list_module
        .create_voxel_fragment_list_generator(levels)
        .chain_err(|| "Could not create voxel fragment list generator with 9 levels")?;
    let voxel_fragment_list = voxel_fragment_list_generator
        .voxelize(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Could not voxelize shape list")?;

    let nodepool_module = NodepoolModule::new().chain_err(|| "Could not initialize nodepool module")?;
    let nodepool_generator = nodepool_module
        .create_nodepool_generator(levels)
        .chain_err(|| "Could not create nodepool generator")?;
    let mut nodepool = nodepool_generator
        .create_nodepool(&mut app.renderer, &voxel_fragment_list)
        .chain_err(|| "Could not create nodepool")?;

    let brickpool_module = BrickpoolModule::new().chain_err(|| "Could not intialize brickpool module")?;
    let mut brickpool = brickpool_module
        .create_brickpool(&mut app.renderer, &voxel_fragment_list, &mut nodepool)
        .chain_err(|| "Could not create brickpool")?;

    brickpool
        .inject_sun_light(&mut app.renderer, &mut nodepool, &sun)
        .chain_err(|| "Could not inject sun light to brickpool")?;

    app.swap_buffers();

    app.renderer.set_enable(EnableOption::DepthTest);
    app.renderer.set_enable(EnableOption::CullFace);
    app.renderer
        .set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
    app.renderer.set_linear_blending_factors(
        LinearBlendingFactor::SourceAlpha,
        LinearBlendingFactor::OneMinusSourceAlpha,
        LinearBlendingFactor::SourceAlpha,
        LinearBlendingFactor::OneMinusSourceAlpha,
    );

    let mut enabled_levels = vec![false; levels];
    let mut voxels_level = levels;
    let mut rendering_mode = 0;
    let mut brickpool_rendering = BrickpoolType::Albedo;
    let mut hide_gui = false;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let move_time = (current_time as f32) / 1_000_000_000.0f32 / 10.0f32;
        //let delta_time = (current_time - previous_time) as f32 / 1000000000.0;
        let delta_time = 0.0333;

        // Process the events
        for app_event in app.poll_events() {
            app_ui.process_event(app_event.clone(), &app.gl_window);

            match app_event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&app_event);
                        camera
                            .process_event(&app_event, &input, delta_time)
                            .chain_err(|| "Could not process event in camera")?;
                    }

                    screen_position[0] = 0.5265625f32;
                    screen_position[1] = 0.10694444f32;
                    // screen_position[0] = position.0 as f32 / app.width as f32;
                    // screen_position[1] = (app.height as f32 - position.1 as f32) / app.height as f32;
                    cones_globals_buffers
                        .set_sub_data(&screen_position, 0)
                        .chain_err(|| "")?;
                }
                _ => {
                    input.process_event(&app_event);
                    camera
                        .process_event(&app_event, &input, delta_time)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match app_event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.width = w as usize;
                    app.height = h as usize;

                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });

                    deferred_collector.resize(app.width, app.height);

                    gui_renderer
                        .resize(w as usize, h as usize)
                        .chain_err(|| "Could not resize GUI")?;
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => if input.state == ElementState::Pressed {
                    if let Some(keycode) = input.virtual_keycode {
                        match keycode {
                            glutin::VirtualKeyCode::Key1 => enabled_levels[0] = !enabled_levels[0],
                            glutin::VirtualKeyCode::Key2 => enabled_levels[1] = !enabled_levels[1],
                            glutin::VirtualKeyCode::Key3 => enabled_levels[2] = !enabled_levels[2],
                            glutin::VirtualKeyCode::Key4 => enabled_levels[3] = !enabled_levels[3],
                            glutin::VirtualKeyCode::Key5 => enabled_levels[4] = !enabled_levels[4],
                            glutin::VirtualKeyCode::Key6 => enabled_levels[5] = !enabled_levels[5],
                            glutin::VirtualKeyCode::Key7 => enabled_levels[6] = !enabled_levels[6],
                            glutin::VirtualKeyCode::Key8 => enabled_levels[7] = !enabled_levels[7],
                            glutin::VirtualKeyCode::Key9 => enabled_levels[8] = !enabled_levels[8],
                            glutin::VirtualKeyCode::Numpad1 => voxels_level = 0,
                            glutin::VirtualKeyCode::Numpad2 => voxels_level = 1,
                            glutin::VirtualKeyCode::Numpad3 => voxels_level = 2,
                            glutin::VirtualKeyCode::Numpad4 => voxels_level = 3,
                            glutin::VirtualKeyCode::Numpad5 => voxels_level = 4,
                            glutin::VirtualKeyCode::Numpad6 => voxels_level = 5,
                            glutin::VirtualKeyCode::Numpad7 => voxels_level = 6,
                            glutin::VirtualKeyCode::Numpad8 => voxels_level = 7,
                            glutin::VirtualKeyCode::Numpad9 => voxels_level = 8,
                            glutin::VirtualKeyCode::B => rendering_mode = 1, // Brickpool
                            glutin::VirtualKeyCode::S => rendering_mode = 0, // Scene
                            glutin::VirtualKeyCode::Q => brickpool_rendering = BrickpoolType::Albedo,
                            glutin::VirtualKeyCode::R => brickpool_rendering = BrickpoolType::Emission,
                            glutin::VirtualKeyCode::T => brickpool_rendering = BrickpoolType::Irradiance,
                            glutin::VirtualKeyCode::H => hide_gui = !hide_gui,
                            _ => {}
                        }
                    }
                },
                Event::WindowEvent {
                    event: WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => (),
            };
        }

        // Render part of the loop
        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        // Dynamic VFL
        // if (i % 5 == 0) {
        //     dynamic_shape_list.primitive_parameters[0].model_matrix = cgmath::Matrix4::from_translation((move_time.sin() / 2.0) * Vector3::new(1.0, 0.0, 0.0)) * dynamic_model_matrix;
        //     dynamic_shape_list.update_buffers().chain_err(|| "")?;

        //     let teapot_vfl = voxel_fragment_list_generator
        //         .voxelize(&mut app.renderer, &[&dynamic_shape_list])
        //         .chain_err(|| "Could not voxelize shape list")?;

        //     nodepool.recalculate_dynamic(&mut app.renderer, &teapot_vfl).chain_err(|| "Could not add teapot to nodepool")?;
        // }

        // Uncomment for real-time per frame light
        if app_ui.variables.update_camera == true {
            sun.set_position(SunPosition {
                y_angle: Rad(app_ui.variables.camera_angle_x),
                height_angle: Rad(app_ui.variables.camera_angle_y),
            }).chain_err(|| "Could not set Sun's poisition")?;

            sun.render_shadowmap(&mut app.renderer, &[&shape_list])
                .chain_err(|| "Could not render shadowmaps")?;

            brickpool
                .inject_sun_light(&mut app.renderer, &mut nodepool, &sun)
                .chain_err(|| "Could not inject sun light to brickpool")?;

            app_ui.variables.update_camera = false;
        }

        // app.renderer.unbind_all().chain_err(|| "Could not unbind")?;

        if rendering_mode == 1 {
            brickpool
                .render_debug(
                    &mut app.renderer,
                    &camera,
                    &mut nodepool,
                    voxels_level,
                    brickpool_rendering,
                )
                .chain_err(|| "Could not render brickpool")?;
        } else {
            deferred_collector
                .render(&mut app.renderer, &[&shape_list], &camera)
                .chain_err(|| "Could not render deferred textures")?;

            unsafe {
                gl::BlitNamedFramebuffer(
                    deferred_collector.framebuffer.get_id().into(),
                    0,
                    0,
                    0,
                    app.width as i32,
                    app.height as i32,
                    0,
                    0,
                    app.width as i32,
                    app.height as i32,
                    gl::DEPTH_BUFFER_BIT,
                    gl::NEAREST,
                );
            }

            app.renderer.set_depth_mask(DepthMask::Disabled);

            let mut draw_command = DrawCommand::arrays(&gi_pipeline, 0, 3)
                .uniform(&gi_globals, 0)
                .uniform(camera.get_buffer(), 1)
                .uniform(sun.get_buffer(), 2)
                .storage(&nodepool.next_buffer, 0)
                .storage(&nodepool.position_buffer, 1)
                .storage_read(&diffuse_cones_buffer, 2)
                .texture_2d(&deferred_collector.position, 0)
                .texture_2d(&deferred_collector.albedo, 1)
                .texture_2d(&deferred_collector.orm, 2)
                .texture_2d(&deferred_collector.normal, 3)
                .texture_2d(&deferred_collector.emission, 4)
                .texture_2d(sun.get_shadowmap(), 5)
                .texture_3d(&brickpool.albedos, 6)
                .texture_3d(&brickpool.irradiances, 7);

            app.renderer.draw(&mut draw_command);

            app.renderer.set_depth_mask(DepthMask::Enabled);
        }

        for i in 0..levels {
            if enabled_levels[i] {
                nodepool
                    .render_debug(&mut app.renderer, &camera, i, true)
                    .chain_err(|| "")?;
            }
        }

        // OPTIONAL: Debug render cones
        app.renderer.clear_default_framebuffer(ClearBuffers::Depth);

        app.renderer.set_enable(EnableOption::DepthTest);
        app.renderer.set_disable(EnableOption::CullFace);

        {
            let ray_positions = MutableBuffer::new_empty(4 * 256 * std::mem::size_of::<f32>())
                .chain_err(|| "Could not create ray positions buffer")?;
            let ray_positions_count =
                MutableBuffer::new(&[0u32]).chain_err(|| "Could not create ray positions count positions buffer")?;

            {
                // Store mips of the cone
                let mut store_count_mips_command = DrawCommand::arrays(&store_cone_mips_pipeline, 0, 1)
                    .mode(DrawMode::Points)
                    .uniform(camera.get_buffer(), 0)
                    .uniform(&cones_globals_buffers, 1)
                    .uniform(&gi_globals, 2)
                    .storage(&nodepool.next_buffer, 1)
                    .storage(&nodepool.position_buffer, 2)
                    .storage(&ray_positions, 3)
                    .texture_2d(&deferred_collector.position, 0)
                    .texture_2d(&deferred_collector.normal, 1)
                    .atomic_counter(&ray_positions_count, 3)
                    .barriers(MemoryBarriers::All);

                app.renderer.draw(&mut store_count_mips_command);
            }

            let ray_points_count = ray_positions_count
                .get::<u32>(1, 0)
                .chain_err(|| "Could not get value of atomic buffer")?[0] as usize;

            let render_ray_pipeline = &*fetch_program("anisotropic_vxgi/debug/cone_vis");
            let mut render_ray_command = DrawCommand::arrays(render_ray_pipeline, 0, ray_points_count)
                .mode(DrawMode::Points)
                .uniform(camera.get_buffer(), 0)
                .storage(&ray_positions, 0);

            app.renderer.draw(&mut render_ray_command);
        }

        // Update UI
        if hide_gui == false {
            app_ui.update_ui();
            gui_renderer
                .render(&mut app.renderer, &app_ui.get_ui(), &app_ui.get_image_map())
                .chain_err(|| "Could not render GUI")?;
        }

        app.swap_buffers();

        // println!("{:?}", camera.get_position());
        // println!("{:?}", app_ui.variables.camera_angle_y);

        previous_time = current_time;
    }

    Ok(())
}
