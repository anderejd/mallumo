#![windows_subsystem = "windows"]

extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new()
        .with_title("Atmosphere")
        .with_dimensions(1280, 720)
        .build();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(0.0),
            height_angle: Rad(1.85),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    ).chain_err(|| "Could not create camera")?;

    let mut input = Input::default();

    let mut y_angle = Rad(0.0);
    let mut height_angle = Deg(-60.0).into();

    let sun_speed = 0.4;

    let atmosphere_module = AtmosphereModule::new(&mut app.renderer).chain_err(|| "")?;

    let atmosphere_vertex = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/deferred_render.vert")],
    ).chain_err(|| "Failed to compile render vertex shader")?;

    let atmosphere_fragment = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            include_str!("../assets/shaders/atmosphere/structures.glsl"),
            include_str!("../assets/shaders/atmosphere/constants.glsl"),
            "#define MALLUMO_ATMOSPHERE_GLOBALS 0\n",
            include_str!("../assets/shaders/atmosphere/render.glsl"),
            include_str!("../assets/shaders/libs/filmic_tonemapping.glsl"),
            include_str!("../assets/shaders/atmosphere/render.frag"),
        ],
    ).chain_err(|| "Failed to compile render fragment shader")?;

    let atmosphere_pipeline = PipelineBuilder::new()
        .vertex_shader(&atmosphere_vertex)
        .fragment_shader(&atmosphere_fragment)
        .build()
        .chain_err(|| "Unable to build draw direct irradiance pipeline")?;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1_000_000_000.0;

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);
        {
            let sun_dir = [sun_to_dir(y_angle, height_angle)];
            let draw_command = DrawCommand::arrays(&atmosphere_pipeline, 0, 3)
                .uniform(&atmosphere_module.globals_buffer, 0)
                .uniform(camera.get_buffer(), 1)
                .uniform_3fv(&sun_dir, 0)
                .texture_2d(&atmosphere_module.transmittance_texture, 0)
                .texture_3d(&atmosphere_module.scattering_texture, 1)
                .texture_2d(&atmosphere_module.irradiance_texture, 2);

            app.renderer.draw(&draw_command).chain_err(|| "Could not draw texture")?;
        }

        app.swap_buffers();

        for event in app.poll_events() {
            match event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&event);
                        camera
                            .process_event(&event, &input, 0.0333)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&event);
                    camera
                        .process_event(&event, &input, 0.0333)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(width, height),
                    ..
                } => {
                    app.width = width as usize;
                    app.height = height as usize;
                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: app.width,
                        height: app.height,
                    });
                }
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        let sun_delta = Rad(delta_time * sun_speed);
        if input.keyboard.D.is_pressed() {
            y_angle -= sun_delta;
        }
        if input.keyboard.A.is_pressed() {
            y_angle += sun_delta;
        }
        if input.keyboard.S.is_pressed() {
            height_angle -= sun_delta;
        }
        if input.keyboard.W.is_pressed() {
            height_angle += sun_delta;
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        previous_time = current_time;
    }

    Ok(())
}

fn sun_to_dir(y_angle: cgmath::Rad<f32>, h: cgmath::Rad<f32>) -> [f32; 3] {
    let x = Rad::sin(h) * Rad::sin(y_angle);
    let y = Rad::cos(h);
    let z = Rad::sin(h) * Rad::cos(y_angle);

    [x, y, z]
}
