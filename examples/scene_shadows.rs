mod app_ui {
    use mallumo::conrod::*;
    use mallumo::*;
    use std;

    widget_ids! {
        struct Ids {
            canvas,
            slider_camera_angle_x,
            slider_camera_angle_y,
        }
    }

    pub struct Variables {
        pub camera_angle_x: f32,
        pub camera_angle_y: f32,
        pub update_camera: bool,
    }

    pub struct AppUi {
        ui: Ui,
        ids: self::Ids,
        image_map: image::Map<Texture2D>,
        pub variables: self::Variables,
    }

    impl AppUi {
        pub fn new(width: usize, height: usize) -> AppUi {
            let mut ui = UiBuilder::new([width as f64, height as f64]).build();

            let ids = self::Ids::new(ui.widget_id_generator());

            // Add fonts to ui
            // ...

            // The image map describing widget->image mappings
            let image_map = image::Map::<Texture2D>::new();

            AppUi {
                ui: ui,
                ids: ids,
                image_map: image_map,
                variables: Variables {
                    camera_angle_x: std::f32::consts::PI / 4.0f32,
                    camera_angle_y: 1.0766133,
                    update_camera: false,
                },
            }
        }

        pub fn get_ui<'a>(&'a self) -> &'a Ui {
            &self.ui
        }

        pub fn get_image_map<'a>(&'a self) -> &'a image::Map<Texture2D> {
            &self.image_map
        }

        pub fn process_event(&mut self, event: glutin::Event, window: &glutin::GlWindow) {
            let input = match convert_event(event, &window) {
                None => return,
                Some(input) => input,
            };

            self.ui.handle_event(input);
        }

        pub fn update_ui(&mut self) {
            let ui = &mut self.ui.set_widgets();

            widget::Canvas::new()
                .w(200.0)
                .pad(10.0)
                .top_left_of(ui.window)
                .color(conrod::color::BLUE)
                .set(self.ids.canvas, ui);

            if let Some(camera_angle_x) = widget::Slider::new(self.variables.camera_angle_x, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .rgb(1.0, 0.3, 0.6)
                .set(self.ids.slider_camera_angle_x, ui)
            {
                if self.variables.camera_angle_x != camera_angle_x {
                    self.variables.update_camera = true;
                }
                self.variables.camera_angle_x = camera_angle_x;
            }

            if let Some(camera_angle_y) = widget::Slider::new(self.variables.camera_angle_y, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .down(30.0)
                .rgb(0.0, 0.3, 1.0)
                .set(self.ids.slider_camera_angle_y, ui)
            {
                if self.variables.camera_angle_y != camera_angle_y {
                    self.variables.update_camera = true;
                }
                self.variables.camera_angle_y = camera_angle_y;
            }
        }
    }
}

extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new().with_title("Shadows example").build();

    let mut scene_renderer = SceneRenderer::new(&mut app.renderer).chain_err(|| "Could not create scene renderer")?;

    let mut gui_renderer = GuiRenderer::new(app.width, app.height, app.gl_window.hidpi_factor() as f64)
        .chain_err(|| "Unable to create GUI renderer")?;
    let mut app_ui = app_ui::AppUi::new(app.width, app.height);

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_enable(EnableOption::DepthTest);
    app.renderer.set_clear_color(ClearColor {
        r: 1.0,
        g: 0.0,
        b: 0.0,
        a: 1.0,
    });
    app.renderer.set_enable(EnableOption::CullFace);

    // Scene
    let files = std::env::args().skip(1).map(|s| s.clone()).collect::<Vec<String>>();
    if files.len() == 0 {
        bail!("usage: gltf-display <PATH>");
    }

    let mut shape_list = ShapeList::from_files(
        files.as_slice(),
        None
        // Some(Unitization {
        //     box_min: Point3::new(-1.0, -1.0, -1.0),
        //     box_max: Point3::new(1.0, 1.0, 1.0),
        //     unitize_if_fits: true,
        // }),
    ).chain_err(|| "Could not create shape list")?;
    // for i in 0..shape_list.primitive_parameters.len() {
    //     shape_list.primitive_parameters[i].model_matrix =
    //         cgmath::Matrix4::from_nonuniform_scale(10.0, 10.0, 10.0) * shape_list.primitive_parameters[i].model_matrix;
    // }
    shape_list.update_buffers();

    // Camera
    let mut camera = FreeCamera::new(
        Point3::new(0.0, 0.0, 0.0),
        Rad(0.0),
        Deg(-89.0).into(),
        Vector3::new(0.0, 1.0, 0.0),
        1.0,
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.01,
        10000.0,
    ).chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    // Deferred
    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;

    // Lights
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(5.0, 5.0, 5.0),
            SunPosition {
                y_angle: Rad(app_ui.variables.camera_angle_x),
                height_angle: Rad(app_ui.variables.camera_angle_y),
            },
            6144,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Coult not render sun's shadowmap")?;

    let mut input = Input::default();

    let mut hide_gui = true;

    'render_loop: loop {
        if app_ui.variables.update_camera == true {
            sun.set_position(SunPosition {
                y_angle: Rad(app_ui.variables.camera_angle_x),
                height_angle: Rad(app_ui.variables.camera_angle_y),
            }).chain_err(|| "Could not set Sun's poisition")?;

            sun.render_shadowmap(&mut app.renderer, &[&shape_list])
                .chain_err(|| "Coult not render sun's shadowmap")?;

            app_ui.variables.update_camera = false;
        }

        sun.render_shadowmap(&mut app.renderer, &[&shape_list])
            .chain_err(|| "Coult not render sun's shadowmap")?;

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        scene_renderer
            .render(&mut app.renderer, &[&shape_list], &camera, None, Some(&sun))
            .chain_err(|| "Could not render the scene")?;

        if hide_gui == false {
            app_ui.update_ui();
            gui_renderer
                .render(&mut app.renderer, &app_ui.get_ui(), &app_ui.get_image_map())
                .chain_err(|| "Could not render GUI")?;
        }

        app.swap_buffers();

        for event in app.poll_events() {
            app_ui.process_event(event.clone(), &app.gl_window);

            match event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&event);
                        camera
                            .process_event(&event, &input, 0.0333)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&event);
                    camera
                        .process_event(&event, &input, 0.0333)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.width = w as usize;
                    app.height = h as usize;

                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });

                    deferred_collector
                        .resize(app.width, app.height)
                        .chain_err(|| "Could not resize deferred collector")?;

                    gui_renderer
                        .resize(w as usize, h as usize)
                        .chain_err(|| "Could not resize GUI")?;
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => if input.state == ElementState::Pressed {
                    if let Some(keycode) = input.virtual_keycode {
                        match keycode {
                            glutin::VirtualKeyCode::H => hide_gui = !hide_gui,
                            glutin::VirtualKeyCode::L => {
                                app_ui.variables.camera_angle_y += 0.0030;
                                app_ui.variables.update_camera = true;
                            }
                            //glutin::VirtualkeyCode::K => { app_ui.variables.camera_angle_y -= 0.017; },
                            _ => {}
                        };
                    }
                },
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;
    }

    Ok(())
}
