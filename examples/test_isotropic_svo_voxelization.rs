extern crate mallumo;

#[macro_use]
extern crate error_chain;
extern crate structopt;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;
use structopt::StructOpt;

quick_main!(run);

fn run() -> Result<()> {
    let arguments = VXGIArguments::from_args();

    if arguments.files.len() == 0 {
        println!("You must provide at least one scene file!");
        return Ok(());
    }

    let mut options = VXGIOptions::new();
    if arguments.hdr {
        options.set_hdr();
    }
    options.set_levels(arguments.levels);

    let mut app = AppBuilder::new().with_title("VXGI example").build();
    let mut input = Input::default();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_enable(EnableOption::DepthTest);
    app.renderer.set_disable(EnableOption::CullFace);

    let mut shape_list = ShapeList::from_files(
        arguments.files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    ).chain_err(|| "Could not create shape list")?;

    let teapot_path: String = "./assets/teapot/teapot.obj".to_string();
    let mut dynamic_shape_list = ShapeList::from_file(
        &teapot_path,
        Some(Unitization {
            box_min: Point3::new(-0.050, -0.050, -0.050),
            box_max: Point3::new(0.050, 0.050, 0.050),
            unitize_if_fits: true,
        }),
    ).chain_err(|| "Could not create shape list")?;
    dynamic_shape_list.primitive_parameters[0].emission = cgmath::Vector4::new(0.0, 0.0, 0.8628, 1.0);
    dynamic_shape_list.primitive_parameters[1].emission = cgmath::Vector4::new(1.0, 0.0, 0.0, 1.0);
    dynamic_shape_list.primitive_parameters[0].model_matrix =
        cgmath::Matrix4::from_translation(Vector3::new(0.50, -0.30, 0.0))
            * dynamic_shape_list.primitive_parameters[0].model_matrix;
    dynamic_shape_list.primitive_parameters[1].model_matrix =
        cgmath::Matrix4::from_translation(Vector3::new(0.50, -0.30, 0.0))
            * dynamic_shape_list.primitive_parameters[1].model_matrix;
    dynamic_shape_list.update_buffers().chain_err(|| "")?;

    // Camera
    let mut camera = FreeCamera::new(
        Point3::new(0.0, -0.25, 0.0),
        Rad(0.0),
        Rad(0.0),
        Vector3::new(0.0, 1.0, 0.0),
        0.01,
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.01,
        10.0,
    ).chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    // Sun
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(2.0, 2.0, 2.0),
            SunPosition {
                y_angle: Rad(std::f32::consts::PI / 4.0f32),
                height_angle: Rad(1.25),
            },
            4096,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list, &dynamic_shape_list])
        .chain_err(|| "Could not render shadowmap")?;

    // VXGI
    let mut vxgi_module = IsotropicSVOGI::new(options).chain_err(|| "Could not create VXGI module")?;

    vxgi_module
        .voxelize(&mut app.renderer, &[&shape_list], true)
        .chain_err(|| "Could not voxelize static geometry")?;

    let mut level: usize = options.levels() as usize;
    let mut ty = BrickpoolType::Albedo;

    let mut render_point_cloud = true;
    let mut render_nodepool = true;
    let mut render_brickpool = true;

    'render_loop: loop {
        for event in app.poll_events() {
            match event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&event);
                        camera
                            .process_event(&event, &input, 0.0333)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&event);
                    camera
                        .process_event(&event, &input, 0.0333)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.width = w as usize;
                    app.height = h as usize;

                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => if input.state == ElementState::Pressed {
                    if let Some(keycode) = input.virtual_keycode {
                        match keycode {
                            glutin::VirtualKeyCode::Key1 => level = 1,
                            glutin::VirtualKeyCode::Key2 => level = 2,
                            glutin::VirtualKeyCode::Key3 => level = 3,
                            glutin::VirtualKeyCode::Key4 => level = 4,
                            glutin::VirtualKeyCode::Key5 => level = 5,
                            glutin::VirtualKeyCode::Key6 => level = 6,
                            glutin::VirtualKeyCode::Key7 => level = 7,
                            glutin::VirtualKeyCode::Key8 => level = 8,
                            glutin::VirtualKeyCode::Key9 => level = 9,
                            glutin::VirtualKeyCode::Q => ty = BrickpoolType::Albedo,
                            glutin::VirtualKeyCode::R => ty = BrickpoolType::Irradiance,
                            glutin::VirtualKeyCode::P => render_point_cloud = !render_point_cloud,
                            glutin::VirtualKeyCode::N => render_nodepool = !render_nodepool,
                            glutin::VirtualKeyCode::B => render_brickpool = !render_brickpool,
                            _ => {}
                        };
                    }
                },
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        sun.render_shadowmap(&mut app.renderer, &[&shape_list, &dynamic_shape_list])
            .chain_err(|| "Could not render shadowmap")?;

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        vxgi_module.inject_radiance(&mut app.renderer, &sun).chain_err(|| "")?;

        if render_point_cloud {
            vxgi_module
                .render_point_cloud(&mut app.renderer, &camera)
                .chain_err(|| "")?;
        }

        if render_nodepool {
            vxgi_module
                .render_nodepool(&mut app.renderer, &camera)
                .chain_err(|| "")?;
        }

        if render_brickpool {
            vxgi_module
                .render_brickpool(&mut app.renderer, &camera, level - 1, ty)
                .chain_err(|| "")?;
        }

        app.swap_buffers();
    }

    Ok(())
}
