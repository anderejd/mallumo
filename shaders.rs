pub static SRC: Dir = Dir {
    path: r"",
    files: &[
File { 
                path: r"assets/shaders\deferred_render.frag", 
                contents: include_bytes!(r"assets/shaders\deferred_render.frag"),
                },
File { 
                path: r"assets/shaders\deferred_render.vert", 
                contents: include_bytes!(r"assets/shaders\deferred_render.vert"),
                },
File { 
                path: r"assets/shaders\main.frag", 
                contents: include_bytes!(r"assets/shaders\main.frag"),
                },
File { 
                path: r"assets/shaders\main.vert", 
                contents: include_bytes!(r"assets/shaders\main.vert"),
                },
File { 
                path: r"assets/shaders\main_vxgi.frag", 
                contents: include_bytes!(r"assets/shaders\main_vxgi.frag"),
                },
File { 
                path: r"assets/shaders\reprojection.frag", 
                contents: include_bytes!(r"assets/shaders\reprojection.frag"),
                },
File { 
                path: r"assets/shaders\shadows_fragment.glsl", 
                contents: include_bytes!(r"assets/shaders\shadows_fragment.glsl"),
                },
File { 
                path: r"assets/shaders\shadows_geometry.glsl", 
                contents: include_bytes!(r"assets/shaders\shadows_geometry.glsl"),
                },
File { 
                path: r"assets/shaders\shadows_vertex.glsl", 
                contents: include_bytes!(r"assets/shaders\shadows_vertex.glsl"),
                },
File { 
                path: r"assets/shaders\vxgi_render.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi_render.frag"),
                },
    ],
    subdirs: &[
Dir {
    path: r"atmosphere",
    files: &[
File { 
                path: r"assets/shaders\atmosphere\compute_direct_irradiance.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_direct_irradiance.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\compute_functions.glsl", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_functions.glsl"),
                },
File { 
                path: r"assets/shaders\atmosphere\compute_indirect_irradiance.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_indirect_irradiance.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\compute_multiple_scattering.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_multiple_scattering.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\compute_scattering_density.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_scattering_density.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\compute_single_scattering.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_single_scattering.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\compute_transmittance.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\compute_transmittance.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\constants.glsl", 
                contents: include_bytes!(r"assets/shaders\atmosphere\constants.glsl"),
                },
File { 
                path: r"assets/shaders\atmosphere\render.frag", 
                contents: include_bytes!(r"assets/shaders\atmosphere\render.frag"),
                },
File { 
                path: r"assets/shaders\atmosphere\render.glsl", 
                contents: include_bytes!(r"assets/shaders\atmosphere\render.glsl"),
                },
File { 
                path: r"assets/shaders\atmosphere\structures.glsl", 
                contents: include_bytes!(r"assets/shaders\atmosphere\structures.glsl"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"clear",
    files: &[
File { 
                path: r"assets/shaders\clear\clear_rgba8_texture.vert", 
                contents: include_bytes!(r"assets/shaders\clear\clear_rgba8_texture.vert"),
                },
File { 
                path: r"assets/shaders\clear\clear_rgba8_texture_alpha_to_1.vert", 
                contents: include_bytes!(r"assets/shaders\clear\clear_rgba8_texture_alpha_to_1.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"clouds",
    files: &[
File { 
                path: r"assets/shaders\clouds\mix_clouds_atmosphere.frag", 
                contents: include_bytes!(r"assets/shaders\clouds\mix_clouds_atmosphere.frag"),
                },
File { 
                path: r"assets/shaders\clouds\render.frag", 
                contents: include_bytes!(r"assets/shaders\clouds\render.frag"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"copy",
    files: &[
File { 
                path: r"assets/shaders\copy\copy_texture_rgba.vert", 
                contents: include_bytes!(r"assets/shaders\copy\copy_texture_rgba.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"debug",
    files: &[
File { 
                path: r"assets/shaders\debug\2dtexture_draw.frag", 
                contents: include_bytes!(r"assets/shaders\debug\2dtexture_draw.frag"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture.frag", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture.frag"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture.geom", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture.geom"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture.vert", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture.vert"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture_buffer.geom", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture_buffer.geom"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture_buffers.frag", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture_buffers.frag"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture_buffers.vert", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture_buffers.vert"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture_set.frag", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture_set.frag"),
                },
File { 
                path: r"assets/shaders\debug\3dtexture_set.vert", 
                contents: include_bytes!(r"assets/shaders\debug\3dtexture_set.vert"),
                },
File { 
                path: r"assets/shaders\debug\compress_3d_count.vert", 
                contents: include_bytes!(r"assets/shaders\debug\compress_3d_count.vert"),
                },
File { 
                path: r"assets/shaders\debug\compress_3d_texture.vert", 
                contents: include_bytes!(r"assets/shaders\debug\compress_3d_texture.vert"),
                },
File { 
                path: r"assets/shaders\debug\geometry.frag", 
                contents: include_bytes!(r"assets/shaders\debug\geometry.frag"),
                },
File { 
                path: r"assets/shaders\debug\geometry.vert", 
                contents: include_bytes!(r"assets/shaders\debug\geometry.vert"),
                },
File { 
                path: r"assets/shaders\debug\show_3d_slice.frag", 
                contents: include_bytes!(r"assets/shaders\debug\show_3d_slice.frag"),
                },
File { 
                path: r"assets/shaders\debug\sun_light.frag", 
                contents: include_bytes!(r"assets/shaders\debug\sun_light.frag"),
                },
File { 
                path: r"assets/shaders\debug\sun_light.vert", 
                contents: include_bytes!(r"assets/shaders\debug\sun_light.vert"),
                },
File { 
                path: r"assets/shaders\debug\voxel_list_store_debug.frag", 
                contents: include_bytes!(r"assets/shaders\debug\voxel_list_store_debug.frag"),
                },
File { 
                path: r"assets/shaders\debug\wireframe.frag", 
                contents: include_bytes!(r"assets/shaders\debug\wireframe.frag"),
                },
File { 
                path: r"assets/shaders\debug\wireframe.vert", 
                contents: include_bytes!(r"assets/shaders\debug\wireframe.vert"),
                },
File { 
                path: r"assets/shaders\debug\write_3d.vert", 
                contents: include_bytes!(r"assets/shaders\debug\write_3d.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"deferred",
    files: &[
File { 
                path: r"assets/shaders\deferred\deferred_collect.frag", 
                contents: include_bytes!(r"assets/shaders\deferred\deferred_collect.frag"),
                },
File { 
                path: r"assets/shaders\deferred\deferred_collect.vert", 
                contents: include_bytes!(r"assets/shaders\deferred\deferred_collect.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"global_illumination",
    files: &[
File { 
                path: r"assets/shaders\global_illumination\brickpool_reset_alpha.vert", 
                contents: include_bytes!(r"assets/shaders\global_illumination\brickpool_reset_alpha.vert"),
                },
File { 
                path: r"assets/shaders\global_illumination\cone_trace.glsl", 
                contents: include_bytes!(r"assets/shaders\global_illumination\cone_trace.glsl"),
                },
File { 
                path: r"assets/shaders\global_illumination\cone_trace_f.frag", 
                contents: include_bytes!(r"assets/shaders\global_illumination\cone_trace_f.frag"),
                },
File { 
                path: r"assets/shaders\global_illumination\inject_sun_light.vert", 
                contents: include_bytes!(r"assets/shaders\global_illumination\inject_sun_light.vert"),
                },
File { 
                path: r"assets/shaders\global_illumination\spread_leaf_irradiance.vert", 
                contents: include_bytes!(r"assets/shaders\global_illumination\spread_leaf_irradiance.vert"),
                },
File { 
                path: r"assets/shaders\global_illumination\svogi_final_render.frag", 
                contents: include_bytes!(r"assets/shaders\global_illumination\svogi_final_render.frag"),
                },
File { 
                path: r"assets/shaders\global_illumination\svogi_final_render.vert", 
                contents: include_bytes!(r"assets/shaders\global_illumination\svogi_final_render.vert"),
                },
File { 
                path: r"assets/shaders\global_illumination\traverse_octree_lod.glsl", 
                contents: include_bytes!(r"assets/shaders\global_illumination\traverse_octree_lod.glsl"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"gui",
    files: &[
File { 
                path: r"assets/shaders\gui\gui.frag", 
                contents: include_bytes!(r"assets/shaders\gui\gui.frag"),
                },
File { 
                path: r"assets/shaders\gui\gui.vert", 
                contents: include_bytes!(r"assets/shaders\gui\gui.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"image_based_lighting",
    files: &[
File { 
                path: r"assets/shaders\image_based_lighting\diffuse_irradiance_convolution_fragment.glsl", 
                contents: include_bytes!(r"assets/shaders\image_based_lighting\diffuse_irradiance_convolution_fragment.glsl"),
                },
File { 
                path: r"assets/shaders\image_based_lighting\diffuse_irradiance_convolution_vertex.glsl", 
                contents: include_bytes!(r"assets/shaders\image_based_lighting\diffuse_irradiance_convolution_vertex.glsl"),
                },
File { 
                path: r"assets/shaders\image_based_lighting\integrate_brdf_fragment.glsl", 
                contents: include_bytes!(r"assets/shaders\image_based_lighting\integrate_brdf_fragment.glsl"),
                },
File { 
                path: r"assets/shaders\image_based_lighting\integrate_brdf_vertex.glsl", 
                contents: include_bytes!(r"assets/shaders\image_based_lighting\integrate_brdf_vertex.glsl"),
                },
File { 
                path: r"assets/shaders\image_based_lighting\prefilter_fragment.glsl", 
                contents: include_bytes!(r"assets/shaders\image_based_lighting\prefilter_fragment.glsl"),
                },
File { 
                path: r"assets/shaders\image_based_lighting\prefilter_vertex.glsl", 
                contents: include_bytes!(r"assets/shaders\image_based_lighting\prefilter_vertex.glsl"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"libs",
    files: &[
File { 
                path: r"assets/shaders\libs\camera.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\camera.glsl"),
                },
File { 
                path: r"assets/shaders\libs\consts.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\consts.glsl"),
                },
File { 
                path: r"assets/shaders\libs\cook_torrance_distribution.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\cook_torrance_distribution.glsl"),
                },
File { 
                path: r"assets/shaders\libs\cook_torrance_fresnel.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\cook_torrance_fresnel.glsl"),
                },
File { 
                path: r"assets/shaders\libs\cook_torrance_geometry.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\cook_torrance_geometry.glsl"),
                },
File { 
                path: r"assets/shaders\libs\filmic_tonemapping.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\filmic_tonemapping.glsl"),
                },
File { 
                path: r"assets/shaders\libs\mipmap.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\mipmap.glsl"),
                },
File { 
                path: r"assets/shaders\libs\parameters.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\parameters.glsl"),
                },
File { 
                path: r"assets/shaders\libs\pbr.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\pbr.glsl"),
                },
File { 
                path: r"assets/shaders\libs\traverse_octree.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\traverse_octree.glsl"),
                },
File { 
                path: r"assets/shaders\libs\utility.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\utility.glsl"),
                },
File { 
                path: r"assets/shaders\libs\version.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\version.glsl"),
                },
File { 
                path: r"assets/shaders\libs\vertices.glsl", 
                contents: include_bytes!(r"assets/shaders\libs\vertices.glsl"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"noises",
    files: &[
File { 
                path: r"assets/shaders\noises\curl_2d.vert", 
                contents: include_bytes!(r"assets/shaders\noises\curl_2d.vert"),
                },
File { 
                path: r"assets/shaders\noises\pw1_w1_w2_w4_3D.vert", 
                contents: include_bytes!(r"assets/shaders\noises\pw1_w1_w2_w4_3D.vert"),
                },
File { 
                path: r"assets/shaders\noises\simplex_3d.glsl", 
                contents: include_bytes!(r"assets/shaders\noises\simplex_3d.glsl"),
                },
File { 
                path: r"assets/shaders\noises\w1_w2_w4_3D.vert", 
                contents: include_bytes!(r"assets/shaders\noises\w1_w2_w4_3D.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"skybox",
    files: &[
File { 
                path: r"assets/shaders\skybox\equirectangular_to_cubemap_fragment.glsl", 
                contents: include_bytes!(r"assets/shaders\skybox\equirectangular_to_cubemap_fragment.glsl"),
                },
File { 
                path: r"assets/shaders\skybox\equirectangular_to_cubemap_vertex.glsl", 
                contents: include_bytes!(r"assets/shaders\skybox\equirectangular_to_cubemap_vertex.glsl"),
                },
File { 
                path: r"assets/shaders\skybox\skybox_fragment.glsl", 
                contents: include_bytes!(r"assets/shaders\skybox\skybox_fragment.glsl"),
                },
File { 
                path: r"assets/shaders\skybox\skybox_vertex.glsl", 
                contents: include_bytes!(r"assets/shaders\skybox\skybox_vertex.glsl"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"sun",
    files: &[
File { 
                path: r"assets/shaders\sun\shadowmap.frag", 
                contents: include_bytes!(r"assets/shaders\sun\shadowmap.frag"),
                },
File { 
                path: r"assets/shaders\sun\shadowmap.vert", 
                contents: include_bytes!(r"assets/shaders\sun\shadowmap.vert"),
                },
File { 
                path: r"assets/shaders\sun\sun.glsl", 
                contents: include_bytes!(r"assets/shaders\sun\sun.glsl"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"vxgi",
    files: &[
File { 
                path: r"assets/shaders\vxgi\gi_shared.glsl", 
                contents: include_bytes!(r"assets/shaders\vxgi\gi_shared.glsl"),
                },
File { 
                path: r"assets/shaders\vxgi\options.glsl", 
                contents: include_bytes!(r"assets/shaders\vxgi\options.glsl"),
                },
File { 
                path: r"assets/shaders\vxgi\README.md", 
                contents: include_bytes!(r"assets/shaders\vxgi\README.md"),
                },
File { 
                path: r"assets/shaders\vxgi\voxelize_shared.glsl", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxelize_shared.glsl"),
                },
    ],
    subdirs: &[
Dir {
    path: r"vxgi\debug",
    files: &[
File { 
                path: r"assets/shaders\vxgi\debug\cone.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\debug\cone.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\debug\cone.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\debug\cone.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\debug\cone.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\debug\cone.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\debug\trace_cone.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\debug\trace_cone.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"vxgi\svo",
    files: &[
File { 
                path: r"assets/shaders\vxgi\svo\anisotropic_gi.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\anisotropic_gi.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\anisotropic_gi.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\anisotropic_gi.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\inject_radiance.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\inject_radiance.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\inject_radiance_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\inject_radiance_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\isotropic_gi.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\isotropic_gi.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\isotropic_gi.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\isotropic_gi.vert"),
                },
    ],
    subdirs: &[
Dir {
    path: r"vxgi\svo\visualize",
    files: &[
File { 
                path: r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip_rgba32.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip_rgba32.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip_rgba32.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip_rgba32.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\aniso_brickpool_mip_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\brickpool.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\brickpool.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\brickpool.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\brickpool.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\brickpool.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\brickpool.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\brickpool_rgba32.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\brickpool_rgba32.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\brickpool_rgba32.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\brickpool_rgba32.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\brickpool_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\brickpool_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\fragment_list.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\fragment_list.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\fragment_list.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\fragment_list.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\nodepool.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\nodepool.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\nodepool.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\nodepool.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\visualize\nodepool.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\visualize\nodepool.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"vxgi\svo\voxelize",
    files: &[
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\node_allocate.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\node_allocate.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\node_flag.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\node_flag.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\node_neighbours.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\node_neighbours.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\shared.glsl", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\shared.glsl"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_count.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_count.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_count.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_count.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_count.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_count.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba32.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba32.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba32.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba32.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba8.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba8.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba8.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba8.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba8.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\voxelize_rgba8.vert"),
                },
    ],
    subdirs: &[
Dir {
    path: r"vxgi\svo\voxelize\brickpool",
    files: &[
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_base.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_base.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_base_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_base_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_mip.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_mip.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_mip_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\aniso_mipmap_mip_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\border_transfer.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\border_transfer.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\border_transfer_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\border_transfer_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\mipmap.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\mipmap.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\mipmap_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\mipmap_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\reset_alpha.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\reset_alpha.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\reset_alpha_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\reset_alpha_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\spread_leaf.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\spread_leaf.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\spread_leaf_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\spread_leaf_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\write_leaf.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\write_leaf.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\svo\voxelize\brickpool\write_leaf_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\svo\voxelize\brickpool\write_leaf_rgba32.vert"),
                },
    ],
    subdirs: &[
    ]
},
    ]
},
    ]
},
Dir {
    path: r"vxgi\voxel_texture",
    files: &[
File { 
                path: r"assets/shaders\vxgi\voxel_texture\anisotropic_gi.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\anisotropic_gi.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\anisotropic_gi.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\anisotropic_gi.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\inject_radiance_rgba32.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\inject_radiance_rgba32.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\inject_radiance_rgba8.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\inject_radiance_rgba8.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\isotropic_gi.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\isotropic_gi.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\isotropic_gi.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\isotropic_gi.vert"),
                },
    ],
    subdirs: &[
Dir {
    path: r"vxgi\voxel_texture\clear",
    files: &[
File { 
                path: r"assets/shaders\vxgi\voxel_texture\clear\clear_alpha_rgba32.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\clear\clear_alpha_rgba32.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\clear\clear_alpha_rgba8.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\clear\clear_alpha_rgba8.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\clear\clear_dynamic_rgba32.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\clear\clear_dynamic_rgba32.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\clear\clear_dynamic_rgba8.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\clear\clear_dynamic_rgba8.comp"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"vxgi\voxel_texture\mipmap",
    files: &[
File { 
                path: r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_mip_rgba32.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_mip_rgba32.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_mip_rgba8.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_mip_rgba8.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_rgba32.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_rgba32.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_rgba8.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\mipmap\anisotropic_mipmap_rgba8.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\mipmap\isotropic_mipmap_rgba32.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\mipmap\isotropic_mipmap_rgba32.comp"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\mipmap\isotropic_mipmap_rgba8.comp", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\mipmap\isotropic_mipmap_rgba8.comp"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"vxgi\voxel_texture\visualize",
    files: &[
File { 
                path: r"assets/shaders\vxgi\voxel_texture\visualize\anisotropic.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\visualize\anisotropic.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\visualize\anisotropic.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\visualize\anisotropic.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\visualize\anisotropic.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\visualize\anisotropic.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\visualize\isotropic.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\visualize\isotropic.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\visualize\isotropic.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\visualize\isotropic.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\visualize\isotropic.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\visualize\isotropic.vert"),
                },
    ],
    subdirs: &[
    ]
},
Dir {
    path: r"vxgi\voxel_texture\voxelize",
    files: &[
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba32.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba32.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba32.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba32.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba32.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba32.vert"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba8.frag", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba8.frag"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba8.geom", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba8.geom"),
                },
File { 
                path: r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba8.vert", 
                contents: include_bytes!(r"assets/shaders\vxgi\voxel_texture\voxelize\voxelize_rgba8.vert"),
                },
    ],
    subdirs: &[
    ]
},
    ]
},
    ]
},
    ]
};

/// A single static asset.
#[derive(Clone, Debug, Hash, PartialEq)]
pub struct File {
    pub path: &'static str,
    pub contents: &'static [u8],
}

impl File {
    /// Get the file's path.
    #[inline]
    pub fn path(&self) -> &::std::path::Path {
        self.path.as_ref()
    }

    /// The file's name (everything after the last slash).
    pub fn name(&self) -> &str {
        self.path().file_name().unwrap().to_str().unwrap()
    }

    /// Get a Reader over the file's contents.
    pub fn as_reader(&self) -> ::std::io::Cursor<&[u8]> {
        ::std::io::Cursor::new(&self.contents)
    }

    /// The total size of this file in bytes.
    pub fn size(&self) -> usize {
        self.contents.len()
    }
}

/// A directory embedded as a static asset.
#[derive(Clone, Debug, Hash, PartialEq)]
pub struct Dir {
    pub path: &'static str,
    pub files: &'static [File],
    pub subdirs: &'static [Dir],
}

impl Dir {
    /// Find a file which *exactly* matches the provided name.
    pub fn find(&'static self, name: &str) -> Option<&'static File> {
        for file in self.files {
            if file.name() == name {
                return Some(file);
            }
        }

        for dir in self.subdirs {
            if let Some(f) = dir.find(name) {
                return Some(f);
            }
        }

        None
    }

    /// Recursively walk the various sub-directories and files inside
    /// the bundled asset.
    ///
    /// # Examples
    ///
    /// ```rust,ignore
    /// for entry in ASSET.walk() {
    ///   match entry {
    ///     DirEntry::File(f) => println!("{} ({} bytes)",
    ///                                   f.path().display(),
    ///                                   f.contents.len()),
    ///     DirEntry::Dir(d) => println!("{} (files: {}, subdirs: {})",
    ///                                  d.path().display(),
    ///                                  d.files.len(),
    ///                                  d.subdirs.len()),
    ///   }
    /// }
    /// ```
    pub fn walk<'a>(&'a self) -> DirWalker<'a> {
        DirWalker::new(self)
    }

    /// Get the directory's name.
    pub fn name(&self) -> &str {
        self.path()
            .file_name()
            .map(|s| s.to_str().unwrap())
            .unwrap_or("")
    }

    /// The directory's full path relative to the root.
    pub fn path(&self) -> &::std::path::Path {
        self.path.as_ref()
    }

    /// Get the total size of this directory and its contents in bytes.
    pub fn size(&self) -> usize {
        let file_size = self.files.iter().map(|f| f.size()).sum();

        self.subdirs.iter().fold(file_size, |acc, d| acc + d.size())
    }
}

/// A directory walker.
///
/// `DirWalker` is an iterator which will recursively traverse
/// the embedded directory, allowing you to inspect each item.
/// It is largely modelled on the API used by the `walkdir`
/// crate.
///
/// You probably won't create one of these directly, instead
/// prefer to use the `Dir::walk()` method.
#[derive(Debug, PartialEq, Clone)]
pub struct DirWalker<'a> {
    root: &'a Dir,
    entries_to_visit: ::std::collections::VecDeque<DirEntry<'a>>,
}

impl<'a> DirWalker<'a> {
    fn new(root: &'a Dir) -> DirWalker<'a> {
        let mut walker = DirWalker {
            root: root,
            entries_to_visit: ::std::collections::VecDeque::new(),
        };
        walker.extend_contents(root);
        walker
    }

    fn extend_contents(&mut self, from: &Dir) {
        for file in from.files {
            self.entries_to_visit.push_back(DirEntry::File(file));
        }

        for dir in from.subdirs {
            self.entries_to_visit.push_back(DirEntry::Dir(dir));
        }
    }
}

impl<'a> Iterator for DirWalker<'a> {
    type Item = DirEntry<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let entry = self.entries_to_visit.pop_front();

        if let Some(DirEntry::Dir(d)) = entry {
            self.extend_contents(d);
            Some(DirEntry::Dir(d))
        } else {
            entry
        }
    }
}

/// A directory entry.
#[derive(Debug, PartialEq, Clone)]
pub enum DirEntry<'a> {
    Dir(&'a Dir),
    File(&'a File),
}

impl<'a> DirEntry<'a> {
    /// Get the entry's name.
    pub fn name(&self) -> &str {
        match *self {
            DirEntry::Dir(d) => d.name(),
            DirEntry::File(f) => f.name(),
        }
    }

    /// Get the entry's path relative to the root directory.
    pub fn path(&self) -> &::std::path::Path {
        match *self {
            DirEntry::Dir(d) => d.path(),
            DirEntry::File(f) => f.path(),
        }
    }
}
/// An iterator over all `DirEntries` which match the specified
/// pattern.
///
/// # Note
///
/// You probably don't want to use this directly. Instead, you'll
/// want the [`Dir::glob()`] method.
///
/// [`Dir::glob()`]: struct.Dir.html#method.glob
pub struct Globs<'a> {
    walker: DirWalker<'a>,
    pattern: ::glob::Pattern,
}

impl<'a> Iterator for Globs<'a> {
    type Item = DirEntry<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(entry) = self.walker.next() {
            if self.pattern.matches_path(entry.path()) {
                return Some(entry);
            }
        }

        None
    }
}

impl Dir {
    /// Find all `DirEntries` which match a glob pattern.
    ///
    /// # Note
    ///
    /// This may fail if you pass in an invalid glob pattern,
    /// consult the [glob docs] for more info on what a valid
    /// pattern is.
    ///
    /// # Examples
    ///
    /// ```rust,ignore
    /// use handlebars::Handlebars;
    /// let mut handlebars = Handlebars::new();
    ///
    /// for entry in ASSETS.glob("*.hbs")? {
    ///     if let DirEntry::File(f) = entry {
    ///         let template_string = String::from_utf8(f.contents.to_vec())?;
    ///         handlebars.register_template_string(f.name(),template_string)?;
    ///     }
    /// }
    /// ```
    ///
    /// [glob docs]: https://doc.rust-lang.org/glob/glob/struct.Pattern.html
    pub fn glob<'a>(&'a self, pattern: &str) -> Result<Globs<'a>, Box<::std::error::Error>> {
        let pattern = ::glob::Pattern::new(pattern)?;
        Ok(Globs {
            walker: self.walk(),
            pattern: pattern,
        })
    }
}
