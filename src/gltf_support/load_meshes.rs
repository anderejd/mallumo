use gltf_support::errors::*;
use gltf_support::mesh::*;
use gltf_support::mikktspace::*;
use gltf_support::primitive::*;
use gltf_support::vertex::*;

use cgmath;
use gltf;
use gltf::mesh::Indices;
use gltf::mesh::TexCoords;
use gltf::DynamicImage;
use image::ImageBuffer;

pub fn load_meshes(gltf: &gltf::Gltf) -> Result<Vec<Mesh>> {
    let mut meshes: Vec<Mesh> = Vec::new();

    for mesh in gltf.meshes() {
        let mut primitives: Vec<Primitive> = Vec::new();

        for primitive in mesh.primitives() {
            // Extract positions
            let positions: Vec<[f32; 3]> = match primitive.positions() {
                Some(positions) => positions,
                None => bail!("Primitive does not have positions"),
            }.collect();

            // Extract indices (we do not support non-indexed geometry yet)
            // TODO: create indices if there are none
            let indices: Vec<u32> = match primitive.indices() {
                Some(indices) => match indices {
                    Indices::U8(iter) => iter.map(|x| x as u32).collect(),
                    Indices::U16(iter) => iter.map(|x| x as u32).collect(),
                    Indices::U32(iter) => iter.collect(),
                },
                None => bail!("Primitive does not have indices"),
            };

            // Extract normals
            let normals: Vec<[f32; 3]> = match primitive.normals() {
                Some(normals) => normals.collect(),
                None => bail!("Primitive does not have normals"),
            };

            // Extract texture coordinates or generate them
            let tex_coords: Vec<[f32; 2]> = match primitive.tex_coords(0) {
                Some(coords) => match coords {
                    TexCoords::F32(iter) => iter.map(|x| x as [f32; 2]).collect(),
                    _ => bail!("Unsigned integer texture coordinates not supported"),
                },
                None => vec![[0.0f32; 2]; positions.len()],
            };

            // Create vertices
            let vertices: Vec<Vertex> =
                Vertex::soa_to_aos(positions.as_slice(), normals.as_slice(), tex_coords.as_slice(), None)
                    .chain_err(|| "Could not convert SoA to AoS")?;

            // Generate tangents and binormals
            let mut deindexed_vertices = deindex_vertices(&indices, &vertices);
            {
                generate_tangents_deindexed(&mut deindexed_vertices).chain_err(|| "Could not generate tangents")?;
            }
            let (indexed_indices, indexed_vertices) = index_vertices(&deindexed_vertices);

            // Create materials and textures
            let mut albedo = cgmath::Vector4::new(1.0, 1.0, 1.0, 1.0);
            let mut metallic = 1.0;
            let mut roughness = 1.0;
            let mut emission = cgmath::Vector4::new(0.0, 0.0, 0.0, 0.0);

            let mut albedo_texture = None;
            let mut metallic_roughness_texture = None;
            let mut occlusion_texture = None;
            let mut normal_texture = None;
            let mut emissive_texture = None;

            match primitive.material() {
                Some(material) => {
                    // normal texture
                    match material.normal_texture() {
                        Some(texture) => {
                            match texture.source().data() {
                                &DynamicImage::ImageRgb8(ref image) => {
                                    // maybe refactor?
                                    let w = image.width();
                                    let h = image.height();
                                    normal_texture =
                                        Some(ImageBuffer::from_raw(w, h, image.clone().into_raw()).unwrap())
                                }
                                &DynamicImage::ImageRgba8(ref image) => {
                                    let w = image.width();
                                    let h = image.height();

                                    let rgb = rgba_to_rgb(image.clone().into_raw().as_slice());

                                    normal_texture = Some(ImageBuffer::from_raw(w, h, rgb).unwrap())
                                }
                                _ => bail!("Normal texture is not RGB8"),
                            }
                        }
                        None => {}
                    }

                    // pbr
                    match material.pbr_metallic_roughness() {
                        Some(pbr) => {
                            // albedo
                            let raw_albedo_factor = pbr.base_color_factor();
                            albedo = cgmath::Vector4::new(
                                raw_albedo_factor[0],
                                raw_albedo_factor[1],
                                raw_albedo_factor[2],
                                raw_albedo_factor[3],
                            );

                            match pbr.base_color_texture() {
                                Some(texture) => {
                                    // maybe refactor?
                                    match texture.source().data() {
                                        &DynamicImage::ImageRgb8(ref image) => {
                                            let w = image.width();
                                            let h = image.height();
                                            let image_pixels = image.clone().into_raw();
                                            let mut raw_rgba = Vec::with_capacity(w as usize * h as usize * 4);
                                            for i in 0..image_pixels.len() / 3 {
                                                raw_rgba.push(image_pixels[i * 3]);
                                                raw_rgba.push(image_pixels[i * 3 + 1]);
                                                raw_rgba.push(image_pixels[i * 3 + 2]);
                                                raw_rgba.push(::std::u8::MAX);
                                            }
                                            albedo_texture = Some(ImageBuffer::from_raw(w, h, raw_rgba).unwrap())
                                        }
                                        &DynamicImage::ImageRgba8(ref image) => {
                                            let w = image.width();
                                            let h = image.height();
                                            albedo_texture =
                                                Some(ImageBuffer::from_raw(w, h, image.clone().into_raw()).unwrap())
                                        }
                                        _ => bail!("Albedo texture is not RGB or RGBA"),
                                    }
                                }
                                None => {}
                            }

                            // mr
                            metallic = pbr.metallic_factor();
                            roughness = pbr.roughness_factor();

                            match pbr.metallic_roughness_texture() {
                                Some(texture) => {
                                    let texture_source = texture.source();
                                    let image = texture_source.data();

                                    let (w, h) = image_size(&image);

                                    let raw_m = extract_channel(2, &image);
                                    let raw_r = extract_channel(1, &image);

                                    if raw_m == None {
                                        bail!("Metallic roughness texture must have at least 3 channels")
                                    }

                                    let raw_m = raw_m.unwrap();
                                    let raw_r = raw_r.unwrap();

                                    metallic_roughness_texture = Some(
                                        ImageBuffer::from_raw(
                                            w as u32,
                                            h as u32,
                                            m_r_to_mr(raw_m.as_slice(), raw_r.as_slice()),
                                        ).unwrap(),
                                    );
                                }
                                None => {}
                            }
                        }
                        None => {}
                    }

                    // occlusion
                    match material.occlusion_texture() {
                        Some(texture) => {
                            let texture_source = texture.source();
                            let image = texture_source.data();

                            let (w, h) = image_size(&image);

                            let raw_o = extract_channel(0, &image).unwrap();

                            occlusion_texture = Some(ImageBuffer::from_raw(w as u32, h as u32, raw_o).unwrap());
                        }
                        None => {}
                    }

                    // emission
                    let raw_emissive_factor = material.emissive_factor();
                    emission = cgmath::Vector4::new(
                        raw_emissive_factor[0],
                        raw_emissive_factor[1],
                        raw_emissive_factor[2],
                        1.0,
                    );

                    match material.emissive_texture() {
                        Some(texture) => match texture.source().data() {
                            &DynamicImage::ImageRgb8(ref image) => {
                                let w = image.width();
                                let h = image.height();
                                emissive_texture = Some(ImageBuffer::from_raw(w, h, image.clone().into_raw()).unwrap())
                            }
                            &DynamicImage::ImageRgba8(ref image) => {
                                let w = image.width();
                                let h = image.height();

                                let rgb = rgba_to_rgb(image.clone().into_raw().as_slice());

                                emissive_texture = Some(ImageBuffer::from_raw(w, h, rgb).unwrap())
                            }
                            _ => bail!("Emissive texture must be RGB"),
                        },
                        None => {}
                    }
                }
                None => {}
            }

            primitives.push(Primitive {
                vertices: indexed_vertices,
                indices: indexed_indices,

                albedo: albedo,
                metallic: metallic,
                roughness: roughness,
                emission: emission,

                albedo_texture: albedo_texture,
                metallic_roughness_texture: metallic_roughness_texture,
                occlusion_texture: occlusion_texture,
                normal_texture: normal_texture,
                emissive_texture: emissive_texture,
            });
        }

        meshes.push(Mesh { primitives: primitives });
    }

    Ok(meshes)
}

pub fn generate_tangents_deindexed(vertices: &mut Vec<Vertex>) -> Result<()> {
    unsafe {
        let mut mikkt_interface = SMikkTSpaceInterface {
            get_faces_count: mikkt_get_num_faces,
            get_vertices_count: mikkt_get_num_vertices_of_face,
            get_position: mikkt_get_position,
            get_normal: mikkt_get_normal,
            get_texture_coordinate: mikkt_get_texcoord,
            set_tspace_basic: mikkt_set_tspace_basic,
            set_tspace: mikkt_set_tspace,
        };

        let mikkt_context = SMikkTSpaceContext {
            interface: &mut mikkt_interface,
            data: vertices as *mut Vec<Vertex> as *mut ::std::os::raw::c_void,
        };

        let res: i32 = genTangSpaceDefault(&mikkt_context);

        if res != 1 {
            bail!("Could not calculate tangent");
        } else {
            Ok(())
        }
    }
}

fn m_r_to_mr(m: &[u8], r: &[u8]) -> Vec<u8> {
    let mut result = Vec::with_capacity(m.len() * 2);

    for i in 0..m.len() {
        result.push(m[i]);
        result.push(r[i]);
    }

    result
}

fn rgba_to_rgb(rgba: &[u8]) -> Vec<u8> {
    let mut result = Vec::with_capacity((rgba.len() * 3) / 4);

    for i in 0..rgba.len() {
        if i % 4 != 3 {
            result.push(rgba[i]);
        }
    }

    result
}

fn image_size(image: &DynamicImage) -> (usize, usize) {
    match image {
        &DynamicImage::ImageLuma8(ref image) => (image.width() as usize, image.height() as usize),
        &DynamicImage::ImageLumaA8(ref image) => (image.width() as usize, image.height() as usize),
        &DynamicImage::ImageRgb8(ref image) => (image.width() as usize, image.height() as usize),
        &DynamicImage::ImageRgba8(ref image) => (image.width() as usize, image.height() as usize),
    }
}

fn extract_channel(channel: usize, image: &DynamicImage) -> Option<Vec<u8>> {
    let (width, height) = image_size(&image);

    match image {
        image @ &DynamicImage::ImageLuma8(_) => {
            if channel == 0 {
                return Some(image.raw_pixels());
            }
            return None;
        }
        image @ &DynamicImage::ImageLumaA8(_) => {
            let mut result = Vec::with_capacity(width * height);

            let raw_pixels = image.raw_pixels();
            if channel < 2 {
                for i in 0..raw_pixels.len() / 2 {
                    result.push(raw_pixels[i * 2 + channel]);
                }
                return Some(result);
            } else {
                return None;
            }
        }
        image @ &DynamicImage::ImageRgb8(_) => {
            let mut result = Vec::with_capacity(width * height);

            let raw_pixels = image.raw_pixels();
            if channel < 3 {
                for i in 0..raw_pixels.len() / 3 {
                    result.push(raw_pixels[i * 3 + channel]);
                }
                return Some(result);
            } else {
                return None;
            }
        }
        image @ &DynamicImage::ImageRgba8(_) => {
            let mut result = Vec::with_capacity(width * height);

            let raw_pixels = image.raw_pixels();
            if channel < 4 {
                for i in 0..raw_pixels.len() / 4 {
                    result.push(raw_pixels[i * 4 + channel]);
                }
                return Some(result);
            } else {
                return None;
            }
        }
    }
}
