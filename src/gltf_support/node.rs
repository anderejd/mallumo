use cgmath::{Basis3, InnerSpace, Matrix3, Matrix4, Quaternion, Vector3};

pub fn array_to_matrix4(arr: [f32; 16]) -> Matrix4<f32> {
    Matrix4::new(
        arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10], arr[11], arr[12],
        arr[13], arr[14], arr[15],
    )
}

#[derive(Debug, Clone, Copy)]
pub struct Node {
    pub id: usize,
    pub mesh: Option<usize>,

    pub translation: Vector3<f32>,
    pub rotation: Quaternion<f32>,
    pub scale: Vector3<f32>,
}

impl Node {
    pub fn model_matrix(&self) -> Matrix4<f32> {
        let translation_matrix = Matrix4::from_translation(self.translation);
        let rotation: Matrix3<f32> = Basis3::from_quaternion(&self.rotation).into();
        let mut rotation_matrix: Matrix4<f32> = rotation.into();
        rotation_matrix[3][3] = 1.0;
        let scale_matrix = Matrix4::from_nonuniform_scale(self.scale[0], self.scale[1], self.scale[2]);

        translation_matrix * rotation_matrix * scale_matrix
    }
}

#[derive(Debug, Clone, Copy)]
pub struct NodeBuilder {
    pub id: usize,
    pub mesh: Option<usize>,

    pub translation: Vector3<f32>,
    pub rotation: Quaternion<f32>,
    pub scale: Vector3<f32>,
}

impl NodeBuilder {
    pub fn new(id: usize) -> NodeBuilder {
        NodeBuilder {
            id: id,
            mesh: None,
            translation: Vector3::new(0.0, 0.0, 0.0),
            rotation: Quaternion::new(0.0, 0.0, 0.0, 1.0),
            scale: Vector3::new(1.0, 1.0, 1.0),
        }
    }

    pub fn mesh(&mut self, mesh: usize) -> &mut NodeBuilder {
        self.mesh = Some(mesh);
        self
    }

    pub fn translation<T>(&mut self, translation: T) -> &mut NodeBuilder
    where
        T: Into<Vector3<f32>>,
    {
        self.translation = translation.into();
        self
    }

    pub fn rotation<T>(&mut self, rotation: T) -> &mut NodeBuilder
    where
        T: Into<Quaternion<f32>>,
    {
        self.rotation = rotation.into();
        self
    }

    pub fn scale<T>(&mut self, scale: T) -> &mut NodeBuilder
    where
        T: Into<Vector3<f32>>,
    {
        self.scale = scale.into();
        self
    }

    pub fn matrix<T>(&mut self, matrix: T) -> &mut NodeBuilder
    where
        T: Into<Matrix4<f32>>,
    {
        // a b c d
        // e f g h
        // i j k l
        // 0 0 0 1

        let matrix: Matrix4<f32> = matrix.into();

        // translation = <d, h, l>
        let translation = matrix.w;

        // sx = || <a, e, i> ||
        // sy = || <e, f, g> ||
        // sz = || <i, j, k> ||
        //
        // scale = <sx, sy, sz>
        let sx = matrix.x.magnitude();
        let sy = matrix.y.magnitude();
        let sz = matrix.z.magnitude();
        let scale = Vector3::new(sx, sy, sz);

        // divide rotation by scale to obtain only rotation matrix
        // a/sx b/sy c/sz | d
        // e/sx f/sy g/sz | h
        // i/sx j/sy k/sz | l
        // -------------------
        // 0    0    0    | 1
        let rotation = Matrix3::from_cols(
            matrix.x.truncate() / sx,
            matrix.y.truncate() / sy,
            matrix.z.truncate() / sz,
        );

        self.translation = translation.truncate();
        self.scale = scale;
        self.rotation = Quaternion::from(rotation);

        self
    }

    pub fn build(&self) -> Node {
        Node {
            id: self.id,
            mesh: self.mesh,
            translation: self.translation,
            rotation: self.rotation,
            scale: self.scale,
        }
    }
}
