use cgmath::prelude::One;
use cgmath::{Matrix4, Vector3, Vector4};

/// Object parameters.
///
/// This structure can also be used on GPU side.
#[repr(C)]
#[derive(Copy, Clone)]
pub struct PrimitiveParameters {
    pub model_matrix: Matrix4<f32>,

    pub albedo: Vector4<f32>,
    pub metallic_roughness_refraction: Vector4<f32>,
    pub emission: Vector4<f32>,

    pub has_albedo_texture: u32,
    pub has_metallic_roughness_texture: u32,
    pub has_occlusion_texture: u32,
    pub has_normal_texture: u32,
    pub has_emissive_texture: u32,

    pub has_diffuse_ibl: u32,
    pub has_specular_ibl: u32,

    pub casts_shadows: u32,
    pub receives_shadows: u32,

    padding: [u8; 108],
}

impl PrimitiveParameters {
    /// Creates new Primitive Parameters.
    pub fn new(
        model_matrix: Matrix4<f32>,
        albedo: Vector4<f32>,
        metallic: f32,
        roughness: f32,
        emission: Vector3<f32>,
        reffraction_index: f32,
        casts_shadows: bool,
        receives_shadows: bool,
        has_albedo_texture: bool,
        has_metallic_roughness_texture: bool,
        has_occlusion_texture: bool,
        has_normal_texture: bool,
        has_emissive_texture: bool,
        has_diffuse_ibl: bool,
        has_specular_ibl: bool,
    ) -> PrimitiveParameters {
        PrimitiveParameters {
            model_matrix: model_matrix,

            albedo: albedo,
            metallic_roughness_refraction: Vector4::new(metallic, roughness, reffraction_index, 0.0),
            emission: Vector4::new(emission[0], emission[1], emission[2], 0.0),

            has_albedo_texture: has_albedo_texture as u32,
            has_metallic_roughness_texture: has_metallic_roughness_texture as u32,
            has_occlusion_texture: has_occlusion_texture as u32,
            has_normal_texture: has_normal_texture as u32,
            has_emissive_texture: has_emissive_texture as u32,

            has_diffuse_ibl: has_diffuse_ibl as u32,
            has_specular_ibl: has_specular_ibl as u32,

            casts_shadows: casts_shadows as u32,
            receives_shadows: receives_shadows as u32,

            padding: [0u8; 108],
        }
    }
}

impl Default for PrimitiveParameters {
    fn default() -> PrimitiveParameters {
        PrimitiveParameters {
            model_matrix: Matrix4::one(),

            albedo: Vector4::new(1.0, 1.0, 1.0, 1.0),
            metallic_roughness_refraction: Vector4::new(0.0, 1.0, 0.0, 0.0),
            emission: Vector4::new(0.0, 0.0, 0.0, 0.0),

            has_albedo_texture: 0,
            has_metallic_roughness_texture: 0,
            has_occlusion_texture: 0,
            has_normal_texture: 0,
            has_emissive_texture: 0,

            has_diffuse_ibl: 0,
            has_specular_ibl: 0,

            casts_shadows: 1,
            receives_shadows: 1,

            padding: [0u8; 108],
        }
    }
}
