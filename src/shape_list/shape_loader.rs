use shape_list::errors::*;
use shape_list::primitive_parameters::*;
use shape_list::shape_list::*;

use gltf_support::*;
use mallumo_gls::*;

use std;
use std::collections::HashMap;
use std::f32;
use std::path::{Path, PathBuf};

use cgmath::prelude::One;
use cgmath::{Matrix4, Quaternion, Vector3, Vector4};

use gltf;
use image;
use image::*;
use ordermap::OrderMap;
use petgraph::*;
use tobj;

/// Loads .gltf file and returns joined shapes. The result may be passed into ShapeList functions.
pub fn load_gltf<T: AsRef<Path>>(
    filepath: &T,
) -> Result<(
    Vec<u32>,
    Vec<Vertex>,
    Vec<PrimitiveParameters>,
    Vec<Texture2D>,
    Vec<Shape>,
)> {
    let source = gltf::import::FromPath::new(filepath.as_ref());
    let import = gltf::Import::custom(source, Default::default());
    let scene_graph = match import.sync() {
        Ok(gltf) => SceneGraph::from_gltf(&gltf).chain_err(|| "Could not create scene graph")?,
        Err(err) => bail!("Invalid glTF ({:?})", err),
    };
    scene_graph
        .to_shape_list(0)
        .chain_err(|| "Could not create shape list out of scene graph")
}

/// Loads .obj file and returns joined shapes. The result may be passed into ShapeList functions.
pub fn load_obj<T: AsRef<Path>>(
    path: &T,
) -> Result<(
    Vec<u32>,
    Vec<Vertex>,
    Vec<PrimitiveParameters>,
    Vec<Texture2D>,
    Vec<Shape>,
)> {
    let (models, materials) = match tobj::load_obj(path.as_ref()) {
        Ok((models, materials)) => (models, materials),
        Err(e) => bail!("Loading of {:?} failed due to {:?}", path.as_ref(), e),
    };

    let mut joined_indices = Vec::new();
    let mut joined_vertices = Vec::new();
    let mut primitive_parameters = Vec::new();
    let mut shapes = Vec::new();

    let mut textures: OrderMap<PathBuf, Texture2D> = OrderMap::new();

    let mut min_pos = [f32::INFINITY; 3];
    let mut max_pos = [f32::NEG_INFINITY; 3];

    let mut positions: Vec<[f32; 3]> = Vec::new();
    let mut normals: Vec<[f32; 3]> = Vec::new();
    let mut texcoords: Vec<[f32; 2]> = Vec::new();

    for model in models {
        let mesh = model.mesh;

        if mesh.positions.len() == 0 {
            bail!("Object does not have positions");
        }

        positions.clear();
        normals.clear();
        texcoords.clear();

        for i in 0..(mesh.positions.len() / 3) {
            let position = [
                mesh.positions[i * 3],
                mesh.positions[i * 3 + 1],
                mesh.positions[i * 3 + 2],
            ];

            positions.push(position);

            for i in 0..3 {
                min_pos[i] = f32::min(min_pos[i], position[i]);
                max_pos[i] = f32::max(max_pos[i], position[i]);
            }
        }

        if mesh.normals.len() > 0 {
            for i in 0..(mesh.positions.len() / 3) {
                normals.push([mesh.normals[i * 3], mesh.normals[i * 3 + 1], mesh.normals[i * 3 + 2]]);
            }
        } else {
            for _ in 0..positions.len() {
                normals.push([0.0, 0.0, 1.0]);
            }
        }

        if mesh.texcoords.len() > 0 {
            for i in 0..(mesh.texcoords.len() / 2) {
                texcoords.push([mesh.texcoords[i * 2], mesh.texcoords[i * 2 + 1]]);
            }
        } else {
            for _ in 0..positions.len() {
                texcoords.push([0.0, 0.0]);
            }
        }

        let vertices = Vertex::soa_to_aos(positions.as_slice(), normals.as_slice(), texcoords.as_slice(), None)
            .chain_err(|| "Could not convert SoA to AoS")?;

        let mut deindexed_vertices = deindex_vertices(&mesh.indices, &vertices);

        generate_tangents_deindexed(&mut deindexed_vertices).chain_err(|| "Could not generate tangents")?;

        let (indices, vertices) = index_vertices(&deindexed_vertices);

        let mut albedo = Vector4::new(1.0, 1.0, 1.0, 1.0);
        let mut has_albedo = false;

        match mesh.material_id {
            Some(i) => {
                albedo = Vector4::new(
                    materials[i].ambient[0],
                    materials[i].ambient[1],
                    materials[i].ambient[2],
                    1.0,
                );
                let albedo_image_obj_raw_path = &materials[i].diffuse_texture;
                if !albedo_image_obj_raw_path.is_empty() {
                    let albedo_image_obj_path = Path::new(albedo_image_obj_raw_path);
                    let albedo_image_path = if albedo_image_obj_path.is_absolute() {
                        albedo_image_obj_path.to_path_buf()
                    } else {
                        match path.as_ref().parent() {
                            Some(parent) => parent.join(albedo_image_obj_path),
                            None => Path::new("/").join(albedo_image_obj_path),
                        }
                    };

                    if textures.contains_key(&albedo_image_path) {
                        has_albedo = true;
                    } else {
                        match image::open(&albedo_image_path) {
                            Ok(image) => {
                                let w;
                                let h;
                                let mut raw_rgba;
                                match image {
                                    DynamicImage::ImageRgb8(ref image) => {
                                        w = image.width();
                                        h = image.height();
                                        let image_pixels = image.clone().into_raw();
                                        raw_rgba = Vec::with_capacity(w as usize * h as usize * 4);
                                        for i in 0..image_pixels.len() / 3 {
                                            raw_rgba.push(image_pixels[i * 3]);
                                            raw_rgba.push(image_pixels[i * 3 + 1]);
                                            raw_rgba.push(image_pixels[i * 3 + 2]);
                                            raw_rgba.push(std::u8::MAX);
                                        }
                                    }
                                    DynamicImage::ImageRgba8(ref image) => {
                                        w = image.width();
                                        h = image.height();
                                        raw_rgba = image.clone().into_raw();
                                    }
                                    _ => bail!("Albedo texture is not RGB or RGBA"),
                                }
                                let gl_texture = Texture2D::new(
                                    Texture2DSize(w as usize, h as usize),
                                    TextureInternalFormat::RGBA8,
                                    TextureFormat::RGBA,
                                    TextureDataType::UnsignedByte,
                                    Texture2DParameters {
                                        min: TextureTexelFilter::Linear,
                                        mag: TextureTexelFilter::Linear,
                                        mipmap: TextureMipmapFilter::None,
                                        ..Default::default()
                                    },
                                    MipmapLevel::Max,
                                    &raw_rgba,
                                ).chain_err(|| "Could not create texture")?;
                                has_albedo = true;
                                textures.insert(albedo_image_path, gl_texture);
                            }
                            Err(_) => {}
                        };
                    }
                }
            }
            None => {}
        }

        let primitive_parameter = PrimitiveParameters::new(
            Matrix4::one(),
            albedo,
            0.0,
            1.0,
            Vector3::new(0.0, 0.0, 0.0),
            0.0,
            // Shadows are on by default
            true,
            true,
            has_albedo,
            false,
            false,
            false,
            false,
            false,
            false,
        );

        if has_albedo {
            let texture_index = textures.len() - 1;
            shapes.push(Shape {
                indices: (joined_indices.len() as usize, indices.len()),
                vertices: (joined_vertices.len() as usize, vertices.len()),

                albedo: Some(texture_index),
                metallic_roughness: None,
                occlusion: None,
                normal: None,
                emissive: None,
            });
        } else {
            shapes.push(Shape {
                indices: (joined_indices.len() as usize, indices.len()),
                vertices: (joined_vertices.len() as usize, vertices.len()),

                albedo: None,
                metallic_roughness: None,
                occlusion: None,
                normal: None,
                emissive: None,
            });
        }

        let aligned_indices_len = if indices.len() % 64 == 0 {
            indices.len()
        } else {
            ((indices.len() / 64) + 1) * 64
        };

        let aligned_vertices_len = if vertices.len() % 4 == 0 {
            vertices.len()
        } else {
            ((vertices.len() / 4) + 1) * 4
        };

        let mut aligned_indices = indices.clone();
        let mut aligned_vertices = vertices.clone();

        for _ in 0..aligned_indices_len - indices.len() {
            aligned_indices.push(0);
        }

        for _ in 0..aligned_vertices_len - vertices.len() {
            aligned_vertices.push(Vertex::new(
                [0.0f32, 0.0, 0.0],
                [0.0f32, 0.0, 0.0],
                [0.0f32, 0.0],
                [0.0f32, 0.0, 0.0, 0.0],
            ));
        }

        joined_indices.append(&mut aligned_indices);
        joined_vertices.append(&mut aligned_vertices);
        primitive_parameters.push(primitive_parameter);
    }

    let textures = textures.into_iter().map(|(_, val)| val as Texture2D).collect();

    Ok((joined_indices, joined_vertices, primitive_parameters, textures, shapes))
}

#[derive(Debug)]
struct SceneGraph {
    pub graph: Graph<Node, u32>,
    pub meshes: Vec<Mesh>,
    pub scenes: Vec<Vec<usize>>,
}

fn node_matrix_default() -> [f32; 16] {
    [
        1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
    ]
}

impl SceneGraph {
    // pub fn new() -> SceneGraph {
    //     SceneGraph {
    //         graph: Graph::<Node, u32>::new(),
    //         meshes: Vec::new(),
    //         scenes: Vec::new(),
    //     }
    // }

    fn from_gltf(gltf: &gltf::Gltf) -> Result<SceneGraph> {
        let mut g = Graph::<Node, u32>::new();

        let meshes = load_meshes(gltf).chain_err(|| "Could not load meshes from glTF")?;

        let mut node_mapping: HashMap<usize, graph::NodeIndex<u32>> = HashMap::new();

        // add nodes to graph
        for n in gltf.nodes() {
            let mut node = NodeBuilder::new(n.index());

            if let Some(mesh) = n.mesh() {
                node.mesh(mesh.index());
            }

            if n.matrix() != node_matrix_default() {
                node.matrix(array_to_matrix4(n.matrix()));
            } else {
                node.translation(n.translation());
                node.rotation(Quaternion::new(
                    n.rotation()[3],
                    n.rotation()[0],
                    n.rotation()[1],
                    n.rotation()[2],
                ));
                node.scale(n.scale());
            }

            let index = g.add_node(node.build());
            node_mapping.insert(node.id, index);
        }

        for from_node in gltf.nodes() {
            for to_node in from_node.children() {
                if let (Some(from_index), Some(to_index)) =
                    (node_mapping.get(&from_node.index()), node_mapping.get(&to_node.index()))
                {
                    g.add_edge(*from_index, *to_index, 0);
                }
            }
        }

        let mut scenes = Vec::new();
        for scene in gltf.scenes() {
            let mut nodes = Vec::new();
            for node in scene.nodes() {
                nodes.push(node.index());
            }
            scenes.push(nodes);
        }

        Ok(SceneGraph {
            graph: g,
            meshes: meshes,
            scenes: scenes,
        })
    }

    fn to_shape_list(
        &self,
        scene_id: usize,
    ) -> Result<(
        Vec<u32>,
        Vec<Vertex>,
        Vec<PrimitiveParameters>,
        Vec<Texture2D>,
        Vec<Shape>,
    )> {
        let mut shape_list = (Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new());

        for node_index in &self.scenes[scene_id] {
            for node_graph_index in self.graph.node_indices() {
                if self.graph[node_graph_index].id == *node_index {
                    self.add_node(node_graph_index, Vec::new(), &mut shape_list)
                        .chain_err(|| "Failed at adding node")?;

                    break;
                }
            }
        }

        Ok(shape_list)
    }

    /// Recursive function for adding nodes to graph
    fn add_node(
        &self,
        node_index: graph::NodeIndex,
        mut model_matrices: Vec<Matrix4<f32>>,
        shape_list: &mut (
            Vec<u32>,
            Vec<Vertex>,
            Vec<PrimitiveParameters>,
            Vec<Texture2D>,
            Vec<Shape>,
        ),
    ) -> Result<()> {
        model_matrices.push(self.graph[node_index].model_matrix());

        let mut node_model_matrix = Matrix4::one();
        for parent_matrix in model_matrices.iter() {
            node_model_matrix = node_model_matrix * parent_matrix;
        }

        if let Some(mesh_id) = self.graph[node_index].mesh {
            let mesh = &self.meshes[mesh_id];
            for ref primitive in mesh.primitives.iter() {
                // Transform images into OpenGL Textures
                let texture_ids = [
                    (
                        match primitive.albedo_texture {
                            Some(ref texture) => Some((texture.dimensions(), texture.clone().into_raw())),
                            None => None,
                        },
                        TextureInternalFormat::RGBA8,
                        TextureFormat::RGBA,
                    ),
                    (
                        match primitive.metallic_roughness_texture {
                            Some(ref texture) => Some((texture.dimensions(), texture.clone().into_raw())),
                            None => None,
                        },
                        TextureInternalFormat::RG8,
                        TextureFormat::RG,
                    ),
                    (
                        match primitive.occlusion_texture {
                            Some(ref texture) => Some((texture.dimensions(), texture.clone().into_raw())),
                            None => None,
                        },
                        TextureInternalFormat::R8,
                        TextureFormat::Red,
                    ),
                    (
                        match primitive.normal_texture {
                            Some(ref texture) => Some((texture.dimensions(), texture.clone().into_raw())),
                            None => None,
                        },
                        TextureInternalFormat::RGB8,
                        TextureFormat::RGB,
                    ),
                    (
                        match primitive.emissive_texture {
                            Some(ref texture) => Some((texture.dimensions(), texture.clone().into_raw())),
                            None => None,
                        },
                        TextureInternalFormat::RGB8,
                        TextureFormat::RGB,
                    ),
                ].iter()
                    .map(|primitive_texture| -> Result<Option<usize>> {
                        match (primitive_texture.0).clone() {
                            Some(texture) => {
                                let texture_dimensions = texture.0;
                                let mut gl_texture = Texture2D::new(
                                    Texture2DSize(texture_dimensions.0 as usize, texture_dimensions.1 as usize),
                                    primitive_texture.1,
                                    primitive_texture.2,
                                    TextureDataType::UnsignedByte,
                                    Texture2DParameters {
                                        min: TextureTexelFilter::Linear,
                                        mag: TextureTexelFilter::Linear,
                                        mipmap: TextureMipmapFilter::Linear,
                                        ..Default::default()
                                    },
                                    MipmapLevel::Max,
                                    &texture.1,
                                ).chain_err(|| "Could not create texture")?;
                                gl_texture.generate_mipmap();
                                shape_list.3.push(gl_texture);
                                Ok(Some(shape_list.3.len() - 1))
                            }
                            None => Ok(None),
                        }
                    })
                    .collect::<Vec<Result<Option<usize>>>>();

                if !texture_ids.iter().all(|id| id.is_ok()) {
                    bail!("Could not create texture");
                }

                let texture_ids = texture_ids
                    .into_iter()
                    .map(|bindless_id| bindless_id.unwrap())
                    .collect::<Vec<Option<usize>>>();

                let primitive_parameters = PrimitiveParameters::new(
                    node_model_matrix,
                    primitive.albedo,
                    primitive.metallic,
                    primitive.roughness,
                    primitive.emission.truncate(),
                    0.0,
                    // Shadows are on by default
                    true,
                    true,
                    texture_ids[0].is_some(),
                    texture_ids[1].is_some(),
                    texture_ids[2].is_some(),
                    texture_ids[3].is_some(),
                    texture_ids[4].is_some(),
                    // IBL is not set by default
                    false,
                    false,
                );

                shape_list.2.push(primitive_parameters);

                let aligned_indices_len = if primitive.indices.len() % 64 == 0 {
                    primitive.indices.len()
                } else {
                    ((primitive.indices.len() / 64) + 1) * 64
                };

                let aligned_vertices_len = if primitive.vertices.len() % 4 == 0 {
                    primitive.vertices.len()
                } else {
                    ((primitive.vertices.len() / 4) + 1) * 4
                };

                shape_list.4.push(Shape {
                    indices: (shape_list.0.len() as usize, primitive.indices.len()),
                    vertices: (shape_list.1.len() as usize, primitive.vertices.len()),

                    albedo: texture_ids[0],
                    metallic_roughness: texture_ids[1],
                    occlusion: texture_ids[2],
                    normal: texture_ids[3],
                    emissive: texture_ids[4],
                });

                let mut aligned_indices = primitive.indices.clone();
                let mut aligned_vertices = primitive.vertices.clone();

                for _ in 0..aligned_indices_len - primitive.indices.len() {
                    aligned_indices.push(0);
                }

                for _ in 0..aligned_vertices_len - primitive.vertices.len() {
                    aligned_vertices.push(Vertex::new(
                        [0.0f32, 0.0, 0.0],
                        [0.0f32, 0.0, 0.0],
                        [0.0f32, 0.0],
                        [0.0f32, 0.0, 0.0, 0.0],
                    ));
                }

                shape_list.0.append(&mut aligned_indices);
                shape_list.1.append(&mut aligned_vertices);
            }
        }

        for child in self.graph.neighbors(node_index) {
            self.add_node(child, model_matrices.clone(), shape_list)?;
        }

        Ok(())
    }
}
