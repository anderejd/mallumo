use image;
use image::*;
use mallumo_gls::*;
use shape_list::errors::*;

use std;
use std::path::Path;

pub fn load_texture_2d<T: AsRef<Path>, M: Into<MipmapLevel>>(filepath: &T, m: M) -> Result<Texture2D> {
    let mipmap = m.into();
    let image_path = if filepath.as_ref().is_absolute() {
        filepath.as_ref().to_path_buf()
    } else {
        match filepath.as_ref().parent() {
            Some(parent) => parent.join(filepath),
            None => Path::new("/").join(filepath),
        }
    };
    let mut texture = match image::open(image_path) {
        Ok(image) => {
            let w;
            let h;
            let mut raw_rgba;
            match image {
                DynamicImage::ImageRgb8(ref image) => {
                    w = image.width();
                    h = image.height();
                    let image_pixels = image.clone().into_raw();
                    raw_rgba = Vec::with_capacity(w as usize * h as usize * 4);
                    for i in 0..image_pixels.len() / 3 {
                        raw_rgba.push(image_pixels[i * 3]);
                        raw_rgba.push(image_pixels[i * 3 + 1]);
                        raw_rgba.push(image_pixels[i * 3 + 2]);
                        raw_rgba.push(std::u8::MAX);
                    }
                }
                DynamicImage::ImageRgba8(ref image) => {
                    w = image.width();
                    h = image.height();
                    raw_rgba = image.clone().into_raw();
                }
                _ => bail!("Albedo texture is not RGB or RGBA"),
            }
            Texture2D::new(
                Texture2DSize(w as usize, h as usize),
                TextureInternalFormat::RGBA8,
                TextureFormat::RGBA,
                TextureDataType::UnsignedByte,
                Texture2DParameters {
                    min: TextureTexelFilter::Linear,
                    mag: TextureTexelFilter::Linear,
                    mipmap: TextureMipmapFilter::None,
                    wrap_s: TextureWrapMode::Repeat,
                    wrap_t: TextureWrapMode::Repeat,
                },
                mipmap,
                &raw_rgba,
            ).chain_err(|| "Could not create texture")?
        }
        Err(e) => bail!("Could not load image {:?}", e),
    };

    texture.generate_mipmap();

    Ok(texture)
}
