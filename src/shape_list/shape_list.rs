use gltf_support::*;
use mallumo_gls::*;
use shape_list::errors::*;
use shape_list::primitive_parameters::*;
use shape_list::shape_loader::*;

use std::borrow::Borrow;
use std::f32;
use std::path::Path;

use cgmath::{Matrix4, Point3, Vector3, Vector4};

/// Describes one single shape.
///
/// Textures are indices into some collection storing the textures.
#[derive(Copy, Clone, Debug)]
pub struct Shape {
    pub indices: (usize, usize),
    pub vertices: (usize, usize),

    pub albedo: Option<usize>,
    pub metallic_roughness: Option<usize>,
    pub occlusion: Option<usize>,
    pub normal: Option<usize>,
    pub emissive: Option<usize>,
}

// May be thought of as a flat scene graph.
// TODO implement scene graph
/// Describes multiple shapes. This structure contains data sufficient for drawing.
pub struct ShapeList {
    pub shapes: Vec<Shape>,

    pub indices: Vec<u32>,
    pub vertices: Vec<Vertex>,

    pub primitive_parameters: Vec<PrimitiveParameters>,

    pub textures: Vec<Texture2D>,
    pub ibl_diffuse_texture: Option<TextureCubemap>,
    pub ibl_specular_texture: Option<TextureCubemap>,

    indices_length: usize,
    vertices_length: usize,
    primitive_parameters_length: usize,

    pub indices_buffer: MutableBuffer,
    pub vertices_buffer: MutableBuffer,
    pub primitive_parameters_buffer: MutableBuffer,
}

/// This structure is used as unitization parameters. The box is axis aligned.
#[derive(Copy, Clone, Debug)]
pub struct Unitization {
    pub box_min: Point3<f32>,
    pub box_max: Point3<f32>,
    pub unitize_if_fits: bool,
}

impl ShapeList {
    /// Create Shape List from single shape.
    pub fn from_shape(
        indices: &[u32],
        vertices: &[Vertex],
        primitive_parameters: PrimitiveParameters,
        albedo: Option<Texture2D>,
        metallic_roughness: Option<Texture2D>,
        occlusion: Option<Texture2D>,
        normal: Option<Texture2D>,
        emissive: Option<Texture2D>,
        unitization: Option<Unitization>,
    ) -> Result<ShapeList> {
        let mut shape = Shape {
            indices: (0, indices.len()),
            vertices: (0, vertices.len()),

            albedo: None,
            metallic_roughness: None,
            occlusion: None,
            normal: None,
            emissive: None,
        };

        let mut textures = Vec::new();

        if let Some(albedo) = albedo {
            shape.albedo = Some(textures.len());
            textures.push(albedo);
        }

        if let Some(texture) = metallic_roughness {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        if let Some(texture) = occlusion {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        if let Some(texture) = normal {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        if let Some(texture) = emissive {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        let mut shapes = Vec::new();
        shapes.push(shape);

        match unitization {
            Some(unitization) => unitize_model(
                indices,
                vertices,
                &mut [primitive_parameters],
                &shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            ),
            None => {}
        }

        let indices_buffer = MutableBuffer::new(indices).chain_err(|| "Could not create indices buffer")?;
        let vertices_buffer = MutableBuffer::new(vertices).chain_err(|| "Could not create vertices buffer")?;
        let primitive_parameters_buffer =
            MutableBuffer::new(&[primitive_parameters]).chain_err(|| "Could not create primitive parameters buffer")?;

        Ok(ShapeList {
            shapes: shapes,

            indices: indices.to_vec(),
            vertices: vertices.to_vec(),
            primitive_parameters: vec![primitive_parameters],

            textures: textures,
            ibl_diffuse_texture: None,
            ibl_specular_texture: None,

            indices_length: indices.len(),
            vertices_length: vertices.len(),
            primitive_parameters_length: 1,

            indices_buffer: indices_buffer,
            vertices_buffer: vertices_buffer,
            primitive_parameters_buffer: primitive_parameters_buffer,
        })
    }

    // TODO tuple -> Shape
    /// Create Shape List from multiple shapes.
    pub fn from_shapes(
        shapes: Vec<(
            &[u32],
            &[Vertex],
            PrimitiveParameters,
            Option<Texture2D>,
            Option<Texture2D>,
            Option<Texture2D>,
            Option<Texture2D>,
            Option<Texture2D>,
        )>,
        unitization: Option<Unitization>,
    ) -> Result<ShapeList> {
        if shapes.len() == 0 {
            bail!("At least one shape must be provided");
        }

        let mut joined_indices: Vec<u32> = Vec::with_capacity(shapes.iter().fold(0, |sum, val| sum + val.0.len()));
        let mut joined_vertices: Vec<Vertex> = Vec::with_capacity(shapes.iter().fold(0, |sum, val| sum + val.1.len()));
        let mut joined_primitive_parameters: Vec<PrimitiveParameters> = Vec::with_capacity(shapes.len());
        let mut joined_textures = Vec::new();

        let mut shape_list_shapes = Vec::new();
        let mut texture_count = 0;

        for shape in shapes {
            let indices = shape.0;
            let vertices = shape.1;

            let mut shape_list_shape = Shape {
                indices: (joined_indices.len() as usize, shape.0.len()),
                vertices: (joined_vertices.len() as usize, shape.1.len()),

                albedo: None,
                metallic_roughness: None,
                occlusion: None,
                normal: None,
                emissive: None,
            };

            if let Some(albedo) = shape.3 {
                joined_textures.push(albedo);
                texture_count += 1;
                shape_list_shape.albedo = Some(texture_count);
            }

            if let Some(metallic_roughness) = shape.4 {
                joined_textures.push(metallic_roughness);
                texture_count += 1;
                shape_list_shape.metallic_roughness = Some(texture_count);
            }

            if let Some(occlusion) = shape.5 {
                joined_textures.push(occlusion);
                texture_count += 1;
                shape_list_shape.occlusion = Some(texture_count);
            }

            if let Some(normal) = shape.6 {
                joined_textures.push(normal);
                texture_count += 1;
                shape_list_shape.normal = Some(texture_count);
            }

            if let Some(emissive) = shape.7 {
                joined_textures.push(emissive);
                texture_count += 1;
                shape_list_shape.emissive = Some(texture_count);
            }

            let aligned_indices_len = if indices.len() % 64 == 0 {
                indices.len()
            } else {
                ((indices.len() / 64) + 1) * 64
            };

            let aligned_vertices_len = if vertices.len() % 4 == 0 {
                vertices.len()
            } else {
                ((vertices.len() / 4) + 1) * 4
            };

            let mut aligned_indices: Vec<u32> = indices.to_vec();
            let mut aligned_vertices: Vec<Vertex> = vertices.to_vec();

            for _ in 0..aligned_indices_len - indices.len() {
                aligned_indices.push(0);
            }

            for _ in 0..aligned_vertices_len - vertices.len() {
                aligned_vertices.push(Vertex::new(
                    [0.0f32, 0.0, 0.0],
                    [0.0f32, 0.0, 0.0],
                    [0.0f32, 0.0],
                    [0.0f32, 0.0, 0.0, 0.0],
                ));
            }

            joined_indices.append(&mut aligned_indices);
            joined_vertices.append(&mut aligned_vertices);
            joined_primitive_parameters.push(shape.2);
            shape_list_shapes.push(shape_list_shape);
        }

        match unitization {
            Some(unitization) => unitize_model(
                joined_indices.as_slice(),
                joined_vertices.as_slice(),
                joined_primitive_parameters.as_mut_slice(),
                &shape_list_shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            ),
            None => {}
        }

        let indices_buffer =
            MutableBuffer::new(joined_indices.as_slice()).chain_err(|| "Could not create indices buffer")?;
        let vertices_buffer =
            MutableBuffer::new(joined_vertices.as_slice()).chain_err(|| "Could not create vertices buffer")?;
        let primitive_parameters_buffer = MutableBuffer::new(joined_primitive_parameters.as_slice())
            .chain_err(|| "Could not create primitive parameters buffer")?;

        Ok(ShapeList {
            shapes: shape_list_shapes,

            indices_length: joined_indices.len(),
            vertices_length: joined_vertices.len(),
            primitive_parameters_length: joined_primitive_parameters.len(),

            indices: joined_indices,
            vertices: joined_vertices,
            primitive_parameters: joined_primitive_parameters,

            textures: joined_textures,

            ibl_diffuse_texture: None,
            ibl_specular_texture: None,

            indices_buffer: indices_buffer,
            vertices_buffer: vertices_buffer,
            primitive_parameters_buffer: primitive_parameters_buffer,
        })
    }

    /// Creates Shape List from joined shapes: indices and vertices are in single array.
    pub fn from_joined_shapes(
        indices: &[u32],
        vertices: &[Vertex],
        primitive_parameters: &[PrimitiveParameters],
        textures: Vec<Texture2D>,
        shapes: Vec<Shape>,
        unitization: Option<Unitization>,
    ) -> Result<ShapeList> {
        let mut primitive_parameters = primitive_parameters.to_vec();
        let primitive_parameters_len = primitive_parameters.len();

        match unitization {
            Some(unitization) => unitize_model(
                indices,
                vertices,
                primitive_parameters.as_mut_slice(),
                &shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            ),
            None => {}
        };

        let indices_buffer = MutableBuffer::new(indices).chain_err(|| "Could not create indices buffer")?;
        let vertices_buffer = MutableBuffer::new(vertices).chain_err(|| "Could not create vertices buffer")?;
        let primitive_parameters_buffer = MutableBuffer::new(primitive_parameters.as_slice())
            .chain_err(|| "Could not create primitive parameters buffer")?;

        Ok(ShapeList {
            shapes: shapes.clone(),

            indices: indices.to_vec(),
            vertices: vertices.to_vec(),
            primitive_parameters: primitive_parameters,

            textures: textures,
            ibl_diffuse_texture: None,
            ibl_specular_texture: None,

            indices_length: indices.len(),
            vertices_length: vertices.len(),
            primitive_parameters_length: primitive_parameters_len,

            indices_buffer: indices_buffer,
            vertices_buffer: vertices_buffer,
            primitive_parameters_buffer: primitive_parameters_buffer,
        })
    }

    /// Creates Shape List from file.
    ///
    /// Currently only .obj and .gltf files are supported.
    pub fn from_file<T: AsRef<Path>>(filepath: &T, unitization: Option<Unitization>) -> Result<ShapeList> {
        let has_supported_extensions =
            supported_file_format(filepath).chain_err(|| "Could not determine if file is supported")?;
        if !has_supported_extensions {
            bail!("File format is not supported");
        }
        let extension = filepath.as_ref().extension().unwrap().to_str().unwrap();
        let shape_list = match extension {
            "obj" => load_obj(filepath).chain_err(|| "Could not load obj scene")?,
            "gltf" => load_gltf(filepath).chain_err(|| "Could not load gltf scene")?,
            _ => unreachable!(),
        };

        ShapeList::from_joined_shapes(
            shape_list.0.as_slice(),
            shape_list.1.as_slice(),
            shape_list.2.as_slice(),
            shape_list.3,
            shape_list.4,
            unitization,
        )
    }

    /// Create Shape List from multiple files.
    ///
    /// Currently only .obj and .gltf files are supported.
    pub fn from_files<T: AsRef<Path>>(filepaths: &[T], unitization: Option<Unitization>) -> Result<ShapeList> {
        if filepaths.len() == 0 {
            bail!("At least one path must be provided");
        }

        let mut has_supported_extensions = true;
        for filepath in filepaths.iter() {
            let ok = supported_file_format(filepath).chain_err(|| "Could not determine if file is supported")?;
            if !ok {
                has_supported_extensions = false;
                break;
            }
        }

        if !has_supported_extensions {
            bail!("File format is not supported");
        }

        let mut shapes = Vec::with_capacity(filepaths.len());

        for filepath in filepaths {
            // cannot fail as it did not fail during extension testing
            let extension = filepath.as_ref().extension().unwrap().to_str().unwrap();
            let shape = match extension {
                "obj" => load_obj(filepath).chain_err(|| "Could not load obj shape")?,
                "gltf" => load_gltf(filepath).chain_err(|| "Could not load gltf scene")?,
                _ => unreachable!(),
            };
            shapes.push(shape);
        }

        let shape_list = join_joined_shapes(shapes);

        ShapeList::from_joined_shapes(
            shape_list.0.as_slice(),
            shape_list.1.as_slice(),
            shape_list.2.as_slice(),
            shape_list.3,
            shape_list.4,
            unitization,
        )
    }

    /// Updates Shape List's buffer, necessary to use after any changes(if changes are supposed to be seen on GPU side).
    pub fn update_buffers(&mut self) -> Result<()> {
        if self.indices.len() == self.indices_length {
            self.indices_buffer
                .set_data(self.indices.as_slice())
                .chain_err(|| "Could not update indices buffer")?;
        } else {
            let new_indices_buffer =
                MutableBuffer::new(self.indices.as_slice()).chain_err(|| "Could not create new indices buffer")?;
            self.indices_buffer = new_indices_buffer;
            self.indices_length = self.indices.len();
        }

        if self.vertices.len() == self.vertices_length {
            self.vertices_buffer
                .set_data(self.vertices.as_slice())
                .chain_err(|| "Could not update vertices buffer")?;
        } else {
            let new_vertices_buffer =
                MutableBuffer::new(self.vertices.as_slice()).chain_err(|| "Could not create new vertices buffer")?;
            self.vertices_buffer = new_vertices_buffer;
            self.vertices_length = self.vertices.len();
        }

        if self.primitive_parameters.len() == self.primitive_parameters_length {
            self.primitive_parameters_buffer
                .set_data(self.primitive_parameters.as_slice())
                .chain_err(|| "Could not update primitive_parameters buffer")?;
        } else {
            let new_primitive_parameters_buffer = MutableBuffer::new(self.primitive_parameters.as_slice())
                .chain_err(|| "Could not create new primitive parameters buffer")?;
            self.primitive_parameters_buffer = new_primitive_parameters_buffer;
            self.primitive_parameters_length = self.primitive_parameters.len();
        }

        Ok(())
    }

    /// Adds shape into Shape List.
    pub fn add_shape(
        &mut self,
        indices: &[u32],
        vertices: &[Vertex],
        primitive_parameters: PrimitiveParameters,
        albedo: Option<Texture2D>,
        metallic_roughness: Option<Texture2D>,
        occlusion: Option<Texture2D>,
        normal: Option<Texture2D>,
        emissive: Option<Texture2D>,
        unitization: Option<Unitization>,
    ) -> Result<()> {
        let mut shape = Shape {
            indices: (self.indices.len(), indices.len()),
            vertices: (self.vertices.len(), vertices.len()),

            albedo: None,
            metallic_roughness: None,
            occlusion: None,
            normal: None,
            emissive: None,
        };

        let mut textures = Vec::new();

        if let Some(albedo) = albedo {
            shape.albedo = Some(textures.len());
            textures.push(albedo);
        }

        if let Some(texture) = metallic_roughness {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        if let Some(texture) = occlusion {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        if let Some(texture) = normal {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        if let Some(texture) = emissive {
            shape.albedo = Some(textures.len());
            textures.push(texture);
        }

        let mut shapes = Vec::new();
        shapes.push(shape);

        match unitization {
            Some(unitization) => unitize_model(
                indices,
                vertices,
                &mut [primitive_parameters],
                &shapes,
                unitization.box_min,
                unitization.box_max,
                unitization.unitize_if_fits,
            ),
            None => {}
        }

        self.indices.extend(indices.iter());
        self.vertices.extend(vertices.iter());
        self.primitive_parameters.push(primitive_parameters);
        self.textures.append(&mut textures);

        self.update_buffers().chain_err(|| "Could not add shape")
    }

    /// Gets all texture references excluding ibl textures.
    pub fn get_texture_references(&self) -> Vec<&Texture2D> {
        self.textures.iter().map(|t| (*t).borrow()).collect()
    }

    /// QoL function used to index into texture array.
    pub fn texture<I: Into<Option<usize>>>(&self, i: I) -> Option<&Texture2D> {
        let index = i.into();
        if index.is_none() {
            None
        } else {
            Some(&self.textures[index.unwrap()])
        }
    }
}

fn join_joined_shapes(
    joined_shapes: Vec<(
        Vec<u32>,
        Vec<Vertex>,
        Vec<PrimitiveParameters>,
        Vec<Texture2D>,
        Vec<Shape>,
    )>,
) -> (
    Vec<u32>,
    Vec<Vertex>,
    Vec<PrimitiveParameters>,
    Vec<Texture2D>,
    Vec<Shape>,
) {
    let mut indices: Vec<u32> = Vec::with_capacity(joined_shapes.iter().fold(0, |sum, val| sum + val.0.len()));
    let mut vertices: Vec<Vertex> = Vec::with_capacity(joined_shapes.iter().fold(0, |sum, val| sum + val.1.len()));
    let mut primitive_parameters: Vec<PrimitiveParameters> =
        Vec::with_capacity(joined_shapes.iter().fold(0, |sum, val| sum + val.2.len()));
    let mut textures: Vec<Texture2D> = Vec::with_capacity(joined_shapes.iter().fold(0, |sum, val| sum + val.4.len()));

    let mut shapes: Vec<Shape> = Vec::new();

    for mut joined_shape in joined_shapes {
        let mut shapes_joined_shapes = joined_shape.4.clone();

        for shape in &mut shapes_joined_shapes {
            shape.indices.0 += indices.len();
            shape.vertices.0 += vertices.len();
        }

        shapes.append(&mut shapes_joined_shapes.clone());
        indices.append(&mut joined_shape.0);
        vertices.append(&mut joined_shape.1);
        primitive_parameters.append(&mut joined_shape.2);
        textures.append(&mut joined_shape.3);
    }

    (indices, vertices, primitive_parameters, textures, shapes)
}

pub fn unitize_model(
    indices: &[u32],
    vertices: &[Vertex],
    primitive_parameters: &mut [PrimitiveParameters],
    shapes: &[Shape],
    box_min: Point3<f32>,
    box_max: Point3<f32>,
    unitize_if_fits: bool,
) {
    let mut min_pos = [f32::INFINITY; 3];
    let mut max_pos = [f32::NEG_INFINITY; 3];

    for (i, shape) in shapes.iter().enumerate() {
        for j in shape.indices.0..shape.indices.0 + shape.indices.1 {
            let index = indices[j] as usize + shape.vertices.0;
            let model_matrix = primitive_parameters[i].model_matrix;
            let raw_pos = Vector4::new(
                vertices[index].position[0],
                vertices[index].position[1],
                vertices[index].position[2],
                1.0,
            );
            let current_pos = model_matrix * raw_pos;
            for k in 0..3 {
                min_pos[k] = f32::min(min_pos[k], current_pos[k]);
                max_pos[k] = f32::max(max_pos[k], current_pos[k]);
            }
        }
    }

    let mut fits = true;
    for i in 0..3 {
        if min_pos[i] < box_min[i] || max_pos[i] > box_max[i] {
            fits = false;
        }
    }

    if fits && unitize_if_fits {
        return;
    }

    let max_box_side_length = (box_max[0] - box_min[0]).max((box_max[1] - box_min[1]).max(box_max[2] - box_min[2]));
    let max_model_side_length = (max_pos[0] - min_pos[0]).max((max_pos[1] - min_pos[1]).max(max_pos[2] - min_pos[2]));

    let scale = max_box_side_length / max_model_side_length;
    let matrix =
        // translate so center of model is center of box
        Matrix4::from_translation(
            Vector3::new(
                (max_pos[0] - min_pos[0]) * scale / -2.0 + (box_min[0] + box_max[0]) / 2.0,
                (max_pos[1] - min_pos[1]) * scale / -2.0 + (box_min[0] + box_max[0]) / 2.0,
                (max_pos[2] - min_pos[2]) * scale / -2.0 + (box_min[0] + box_max[0]) / 2.0
            )
          )
        // scale
        * Matrix4::from_scale(scale)
        // translate to (0, 0, 0)
        * Matrix4::from_translation(Vector3::new(-min_pos[0], -min_pos[1], -min_pos[2]));

    for primitive_parameter in primitive_parameters {
        primitive_parameter.model_matrix = matrix * primitive_parameter.model_matrix;
    }
}

fn supported_file_format<T: AsRef<Path>>(filepath: T) -> Result<bool> {
    let supported_extensions = ["obj", "gltf"];
    for extension in supported_extensions.iter() {
        let filepath_extension = match filepath.as_ref().extension() {
            Some(ext) => match ext.to_str() {
                Some(e) => e,
                None => bail!("Could not convert extracted extension to string"),
            },
            None => bail!("Could not get extension from filepath"),
        };
        if &filepath_extension == extension {
            return Ok(true);
        }
    }

    Ok(false)
}
