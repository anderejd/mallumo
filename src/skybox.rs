use super::errors::*;
use super::*;

use std::ffi::OsStr;
use std::path::Path;

/// Simple skybox texture holding its own internal state used for rendering.
pub struct Skybox {
    pub skybox: TextureCubemap,

    pipeline: Box<Pipeline>,

    cube_indices: ImmutableBuffer,
    cube_vertices: ImmutableBuffer,
}

impl Skybox {
    /// Creates Skybox from file.
    ///
    /// # Errors
    /// Currently only .hdr files are supported.
    pub fn from_file<P: AsRef<Path>>(mut renderer: &mut Renderer, filepath: &P, size: usize) -> Result<Skybox> {
        if filepath.as_ref().extension() != Some(OsStr::new("hdr")) {
            bail!("Currently only .hdr files are supported");
        };

        let skybox = equirectangular_to_cubemap(&mut renderer, &Path::new(filepath.as_ref()), size, 1)
            .chain_err(|| "Could not convert equirectangular map")?;

        let vertex_shader = Shader::new(
            ShaderType::Vertex,
            &[include_str!("../assets/shaders/skybox/skybox_vertex.glsl")],
        ).chain_err(|| "Failed to compile skybox render vertex shader")?;
        let fragment_shader = Shader::new(
            ShaderType::Fragment,
            &[include_str!("../assets/shaders/skybox/skybox_fragment.glsl")],
        ).chain_err(|| "Failed to compile skybox render fragment shader")?;

        let pipeline = PipelineBuilder::new()
            .vertex_shader(&vertex_shader)
            .fragment_shader(&fragment_shader)
            .build()
            .chain_err(|| "Unable to build skybox pipeline")?;

        let indices_buffer =
            ImmutableBuffer::new(&INVERTED_CUBE_INDICES).chain_err(|| "Unable to create skybox indices buffer")?;
        let vertices_buffer =
            ImmutableBuffer::new(&CUBE_VERTICES).chain_err(|| "Unable to create skybox vertices buffer")?;

        Ok(Skybox {
            skybox: skybox,

            pipeline: Box::new(pipeline),

            cube_indices: indices_buffer,
            cube_vertices: vertices_buffer,
        })
    }

    /// Creates Skybox from cubemap.
    pub fn from_cubemap(skybox: TextureCubemap) -> Result<Skybox> {
        let vertex_shader = Shader::new(
            ShaderType::Vertex,
            &[include_str!("../assets/shaders/skybox/skybox_vertex.glsl")],
        ).chain_err(|| "Failed to compile vertex shader")?;
        let fragment_shader = Shader::new(
            ShaderType::Fragment,
            &[include_str!("../assets/shaders/skybox/skybox_fragment.glsl")],
        ).chain_err(|| "Failed to compile fragment shader")?;

        let pipeline = PipelineBuilder::new()
            .vertex_shader(&vertex_shader)
            .fragment_shader(&fragment_shader)
            .build()
            .chain_err(|| "Unable to build skybox pipeline")?;

        let indices_buffer =
            ImmutableBuffer::new(&CUBE_INDICES).chain_err(|| "Unable to create skybox indices buffer")?;
        let vertices_buffer =
            ImmutableBuffer::new(&CUBE_VERTICES).chain_err(|| "Unable to create skybox vertices buffer")?;

        Ok(Skybox {
            skybox: skybox,

            pipeline: Box::new(pipeline),

            cube_indices: indices_buffer,
            cube_vertices: vertices_buffer,
        })
    }

    /// Renders skybox.
    pub fn render<T: Camera>(&self, renderer: &mut Renderer, camera: &T) -> Result<()> {
        let draw_command = DrawCommand::arrays(self.pipeline.as_ref(), 0, CUBE_INDICES.len())
            .uniform(camera.get_buffer(), 0)
            .storage_read(&self.cube_indices, 0)
            .storage_read(&self.cube_vertices, 1)
            .texture_cubemap(&self.skybox, 0);

        renderer.draw(&draw_command).chain_err(|| "Could not draw skybox")?;

        Ok(())
    }
}
