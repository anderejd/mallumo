use super::errors::*;
use super::*;

pub trait Camera {
    /// Updates Camera's buffer, necessary to use after any changes(if changes are supposed to be seen on GPU side).
    ///
    /// '''
    /// camera.look_at(Point3::new(0.0, 0.0, 0.0)); // even though internal state is changed
    /// camera.update_buffer();                     // it is required to send the new data to GPU
    fn update_buffer(&mut self) -> Result<()>;

    /// Returns reference to Camera's buffer, mostly used for binding.
    fn get_buffer(&self) -> &Buffer;

    /// Returns projection matrix.
    fn get_projection(&self) -> cgmath::Matrix4<f32>;

    /// Returns view matrix.
    fn get_view(&self) -> cgmath::Matrix4<f32>;

    /// Return projection view matrix.
    fn get_projection_view(&self) -> cgmath::Matrix4<f32>;

    /// Returns Camera's position.
    fn get_position(&self) -> cgmath::Point3<f32>;

    /// Returns screen ratio used by Camera.
    fn get_ratio(&self) -> f32;
}
