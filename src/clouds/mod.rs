mod errors {
    error_chain! {
        errors {
        }
    }
}

mod noise_generation;
pub use self::noise_generation::*;
