use super::super::*;
use super::errors::*;

/// Creates RGBA 3D noise texture.
///
/// R: perlin combined with worley noise
/// G: worley, the same as in R
/// B: worley, G's frequency doubled
/// A: worley, B's frequency doubled
/// all channels are [0.0, 1.0]
pub fn create_pw1_w1_w2_w4_3d_noise(renderer: &mut Renderer, size: usize) -> Result<Texture3D> {
    let noise_texture = Texture3D::new_empty(
        Texture3DSize(size, size, size),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Float,
        Texture3DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::Repeat,
            wrap_t: TextureWrapMode::Repeat,
            wrap_r: TextureWrapMode::Repeat,
        },
        1,
    ).chain_err(|| "Could not create noise texture")?;

    let vertex = Shader::new(
        ShaderType::Vertex,
        &[
            include_str!("../../assets/shaders/libs/version.glsl"),
            include_str!("../../assets/shaders/noises/pw1_w1_w2_w4_3D.vert"),
        ],
    ).chain_err(|| "Failed to compile noise vertex shader")?;

    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex)
        .build()
        .chain_err(|| "Unable to build noise pipeline")?;

    // borrow checker
    {
        let draw_command = DrawCommand::arrays(&pipeline, 0, size * size * size)
            .image(&noise_texture, 0, 0, ImageInternalFormat::RGBA32F)
            .barriers(MemoryBarriers::All);

        renderer.draw(&draw_command).chain_err(|| "Could not render noise")?;
    }

    Ok(noise_texture)
}

/// Creates RGB 3D noise texture. (4th channel is unused)
///
/// R: worley noise
/// G: worley, R's frequency doubled
/// B: worley, G's frequency doubled
/// all channels are [0.0, 1.0]
pub fn create_w1_w2_w4_3d_noise(renderer: &mut Renderer, size: usize) -> Result<Texture3D> {
    let noise_texture = Texture3D::new_empty(
        Texture3DSize(size, size, size),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Float,
        Texture3DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::Repeat,
            wrap_t: TextureWrapMode::Repeat,
            wrap_r: TextureWrapMode::Repeat,
        },
        1,
    ).chain_err(|| "Could not create noise texture")?;

    let vertex = Shader::new(
        ShaderType::Vertex,
        &[
            include_str!("../../assets/shaders/libs/version.glsl"),
            include_str!("../../assets/shaders/noises/w1_w2_w4_3D.vert"),
        ],
    ).chain_err(|| "Failed to compile noise vertex shader")?;

    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex)
        .build()
        .chain_err(|| "Unable to build noise pipeline")?;

    // borrow checker
    {
        let draw_command = DrawCommand::arrays(&pipeline, 0, size * size * size)
            .image(&noise_texture, 0, 0, ImageInternalFormat::RGBA32F)
            .barriers(MemoryBarriers::All);

        renderer.draw(&draw_command).chain_err(|| "Could not render noise")?;
    }

    Ok(noise_texture)
}

/// Creates RGB 2D noise texture. (4th channel is unused)
///
/// R: curl noise
/// G: curl noise
/// B: curl noise
/// all channels are [-1.0, 1.0]
pub fn create_curl_2d_noise(renderer: &mut Renderer, size: usize) -> Result<Texture2D> {
    let noise_texture = Texture2D::new_empty(
        Texture2DSize(size, size),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Float,
        Texture2DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::Repeat,
            wrap_t: TextureWrapMode::Repeat,
        },
        1,
    ).chain_err(|| "Could not create noise texture")?;

    let vertex = Shader::new(
        ShaderType::Vertex,
        &[
            include_str!("../../assets/shaders/libs/version.glsl"),
            include_str!("../../assets/shaders/noises/simplex_3d.glsl"),
            include_str!("../../assets/shaders/noises/curl_2D.vert"),
        ],
    ).chain_err(|| "Failed to compile noise vertex shader")?;

    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex)
        .build()
        .chain_err(|| "Unable to build noise pipeline")?;

    {
        let draw_command = DrawCommand::arrays(&pipeline, 0, size * size)
            .image(&noise_texture, 0, 0, ImageInternalFormat::RGBA32F)
            .barriers(MemoryBarriers::All);

        renderer.draw(&draw_command).chain_err(|| "Could not render noise")?;
    }

    Ok(noise_texture)
}
