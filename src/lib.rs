#![recursion_limit = "1024"]

#[macro_use]
extern crate error_chain;
extern crate glob;
#[macro_use]
extern crate lazy_static;
extern crate owning_ref;
extern crate rand;
extern crate regex;
#[macro_use]
extern crate structopt;
extern crate term;

extern crate gltf;
extern crate image;
extern crate ordermap;
extern crate petgraph;
extern crate tobj;

extern crate mallumo_gls;

// Reexport conrod
pub extern crate conrod as conrod_ext;
pub mod conrod {
    pub use conrod_ext::*;
}

extern crate glutin as glutin_ext;
pub mod glutin {
    pub use glutin_ext::*;
}

extern crate cgmath as cgmath_ext;
pub mod cgmath {
    pub use cgmath_ext::*;
}

mod errors {
    error_chain!{}
}

pub use mallumo_gls::*;

mod gltf_support;
pub use gltf_support::*;

mod basic_shapes;
pub use basic_shapes::*;

mod shape_list;
pub use shape_list::*;

mod input;
pub use input::*;

mod camera;
pub use camera::*;

mod sphere_camera;
pub use sphere_camera::*;

mod free_camera;
pub use free_camera::*;

mod lights;
pub use lights::*;

mod equirectangular;
pub use equirectangular::*;

mod ibl;
pub use ibl::*;

mod skybox;
pub use skybox::*;

mod scene_renderer;
pub use scene_renderer::*;

mod deferred;
pub use deferred::*;

// mod sparse_voxel_octree;
// pub use sparse_voxel_octree::*;

mod gui;
pub use gui::*;

mod app;
pub use app::*;

mod sun;
pub use sun::*;

mod clouds;
pub use clouds::*;

mod build_brdf;
pub use build_brdf::*;

mod shader_loader;
pub use shader_loader::*;

mod atmosphere;
pub use atmosphere::*;

mod vxgi;
pub use vxgi::*;

mod reprojection;
pub use reprojection::*;
