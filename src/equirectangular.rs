use super::errors::*;
use super::*;

use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use cgmath::{Deg, Matrix4, Point3, Vector3};

// TODO: pipeline should be lazy_staticed.
/// Converts equirectangular image to its cubemap representation.
pub fn equirectangular_to_cubemap(
    renderer: &mut Renderer,
    path: &Path,
    size: usize,
    levels: usize,
) -> Result<TextureCubemap> {
    let vertex_shader = Shader::new(
        ShaderType::Vertex,
        &[include_str!(
            "../assets/shaders/skybox/equirectangular_to_cubemap_vertex.glsl"
        )],
    ).chain_err(|| "Failed to compile vertex shader")?;
    let fragment_shader = Shader::new(
        ShaderType::Fragment,
        &[include_str!(
            "../assets/shaders/skybox/equirectangular_to_cubemap_fragment.glsl"
        )],
    ).chain_err(|| "Failed to compile fragment shader")?;
    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex_shader)
        .fragment_shader(&fragment_shader)
        .build()
        .chain_err(|| "Unable to build pipeline")?;

    // Create Cube
    let cube_vertices_buffer = ImmutableBuffer::new(&CUBE_VERTICES).chain_err(|| "Unable to create vertices buffer")?;
    let cube_indices_buffer = ImmutableBuffer::new(&CUBE_INDICES).chain_err(|| "Unable to create indices buffer")?;

    // Load HDR equirectangular texture
    let hdr_file = File::open(path).chain_err(|| "Could not open file")?;
    let hdr_file = BufReader::new(hdr_file);
    let hdr_decoder = image::hdr::HDRDecoder::new(hdr_file).chain_err(|| "Could not read file as HDR")?;
    let hdr_metadata = hdr_decoder.metadata();

    let hdr_pixels = hdr_decoder.read_image_hdr().chain_err(|| "Unable to read HDR image")?;

    let mut hdr_pixels_raw = Vec::new();
    for pixel in hdr_pixels {
        hdr_pixels_raw.push(pixel.data[0]);
        hdr_pixels_raw.push(pixel.data[1]);
        hdr_pixels_raw.push(pixel.data[2]);
        hdr_pixels_raw.push(1.0);
    }

    let equirectangular_texture = Texture2D::new(
        Texture2DSize(hdr_metadata.width as usize, hdr_metadata.height as usize),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Float,
        Texture2DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            ..Default::default()
        },
        1,
        &hdr_pixels_raw,
    ).chain_err(|| "Could not create equirectangular texture")?;

    // Create buffers
    let perspective = cgmath::perspective(Deg(90.0), 1.0 as f32, 0.1 as f32, 10.0 as f32);
    let center = Point3::new(0.0f32, 0.0, 0.0);
    let views = [
        Matrix4::look_at(center, Point3::new(1.0, 0.0, 0.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(-1.0, 0.0, 0.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(0.0, 1.0, 0.0), Vector3::new(0.0, 0.0, 1.0)),
        Matrix4::look_at(center, Point3::new(0.0, -1.0, 0.0), Vector3::new(0.0, 0.0, -1.0)),
        Matrix4::look_at(center, Point3::new(0.0, 0.0, 1.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(0.0, 0.0, -1.0), Vector3::new(0.0, -1.0, 0.0)),
    ];

    let mut camera_buffer = MutableBuffer::new_empty(128).chain_err(|| "Unable to create camera buffer")?;
    camera_buffer
        .set_sub_data(perspective.as_ref() as &[f32; 16], 0)
        .chain_err(|| "Could not update camera buffer")?;

    let mut cubemap = TextureCubemap::new_empty(
        TextureCubemapSize(size, size),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Float,
        TextureCubemapParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
            seamless: TextureSeamless::True,
            mipmap: TextureMipmapFilter::Linear,
        },
        levels,
    ).chain_err(|| "Could not create environment cubemap texture")?;

    // Create framebuffer
    let mut framebuffer = GeneralFramebuffer::new();
    framebuffer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: size,
        height: size,
    });
    framebuffer.set_clear_color(ClearColor {
        r: 0.5,
        g: 0.5,
        b: 0.5,
        a: 1.0,
    });
    framebuffer.set_disable(EnableOption::DepthTest);

    for i in 0..6 {
        // Update face where to render
        camera_buffer
            .set_sub_data(views[i].as_ref() as &[f32; 16], 64)
            .chain_err(|| "Coult not change view")?;

        let attachments = DrawTextureTarget {
            color0: DrawTextureAttachOption::AttachTextureLayer {
                texture: &mut cubemap,
                level: 0,
                layer: i,
            },
            ..Default::default()
        };

        let draw_command = DrawCommand::arrays(&pipeline, 0, CUBE_INDICES.len())
            .framebuffer(&framebuffer)
            .attachments(&attachments)
            .uniform(&camera_buffer, 0)
            .storage_read(&cube_indices_buffer, 0)
            .storage_read(&cube_vertices_buffer, 1)
            .texture_2d(&equirectangular_texture, 0);

        renderer
            .draw(&draw_command)
            .chain_err(|| "Could not convert to cubemap")?;
    }

    cubemap.generate_mipmap();

    Ok(cubemap)
}
