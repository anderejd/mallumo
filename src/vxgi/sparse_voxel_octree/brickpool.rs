use super::*;

use mallumo_gls::*;
use shader_loader::*;

use Camera;

/// Compressed 3D texture of Sparse Voxel Octree.
pub struct Brickpool {
    pub albedos: Texture3D,
    pub irradiances: Texture3D,

    options: VXGIOptions,
    voxel_format: VoxelTextureFormat,
}

impl Brickpool {
    /// Creates new Brickpool from Voxel Fragment List.
    pub fn new(
        renderer: &mut Renderer,
        voxel_fragment_list: &VoxelFragmentList,
        nodepool: &mut Nodepool,
        options: &VXGIOptions,
        options_buffer: &MutableBuffer,
    ) -> Result<Brickpool> {
        let voxel_format = match options.hdr() {
            true => VoxelTextureFormat::RGBA32,
            false => VoxelTextureFormat::RGBA8,
        };

        let nodes_num = nodepool.count;

        let brickpool_size = (3.0 * f32::ceil(f32::powf(nodes_num as f32, 1.0f32 / 3.0f32))) as usize;

        let size_x = brickpool_size;
        let size_y = brickpool_size;
        let size_z = brickpool_size;

        // println!("Brickpool size: {} ^ 3", brickpool_size);
        // println!(
        //     "Brickpool memory: {} MB",
        //     (4 * 2 * brickpool_size * brickpool_size * brickpool_size) / 1024 / 1024
        // );

        let parameters = Texture3DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
        };

        let mut zero_data: Vec<f32> = Vec::new();
        for _ in 0..size_x * size_y * size_z {
            zero_data.push(0.0);
            zero_data.push(0.0);
            zero_data.push(0.0);
            zero_data.push(0.0);
        }

        let albedos = Texture3D::new(
            Texture3DSize(size_x, size_y, size_z),
            voxel_format.into(),
            TextureFormat::RGBA,
            TextureDataType::Float,
            parameters,
            1,
            zero_data.as_slice(),
        ).chain_err(|| "Could not create albedos texture")?;

        let irradiances = Texture3D::new(
            Texture3DSize(size_x, size_y, size_z),
            voxel_format.into(),
            TextureFormat::RGBA,
            TextureDataType::Float,
            parameters,
            1,
            zero_data.as_slice(),
        ).chain_err(|| "Could not create irradiances texture")?;

        let mut lock_texture = match options.hdr() {
            false => None,
            true => Some(Texture3D::new_empty(
                Texture3DSize(size_x, size_y, size_z),
                TextureInternalFormat::R32UI,
                TextureFormat::RedInteger,
                TextureDataType::UnsignedInt,
                parameters,
                1,
            ).chain_err(|| "")?),
        };

        if options.hdr() {
            lock_texture.as_mut().unwrap().clear().chain_err(|| "")?;
        }

        let mut count_texture = match options.hdr() {
            false => None,
            true => Some(Texture3D::new_empty(
                Texture3DSize(size_x, size_y, size_z),
                TextureInternalFormat::R32UI,
                TextureFormat::RedInteger,
                TextureDataType::UnsignedInt,
                parameters,
                1,
            ).chain_err(|| "")?),
        };

        if options.hdr() {
            count_texture.as_mut().unwrap().clear().chain_err(|| "")?;
        }

        // Brickpool write leaf
        {
            let write_leaf_pipeline_name = match voxel_format {
                VoxelTextureFormat::RGBA32 => "vxgi/svo/voxelize/brickpool/write_leaf_rgba32",
                VoxelTextureFormat::RGBA8 => "vxgi/svo/voxelize/brickpool/write_leaf",
            };
            let write_leaf_command = &*fetch_program(write_leaf_pipeline_name);

            let write_leaf_format = match voxel_format {
                VoxelTextureFormat::RGBA32 => ImageInternalFormat::RGBA32F,
                VoxelTextureFormat::RGBA8 => ImageInternalFormat::R32UI,
            };

            let mut write_leaf_command = DrawCommand::arrays(
                write_leaf_command,
                0,
                voxel_fragment_list.count,
            ).mode(DrawMode::Points)
                // Options
                .uniform(options_buffer, 0)
                // Voxel Fragment List
                .storage(&voxel_fragment_list.voxel_positions_buffer, 0)
                .storage(&voxel_fragment_list.voxel_albedos_buffer, 1)
                // Nodepool
                .storage(&nodepool.next_buffer, 2)
                // Brickpool
                .image(&albedos, 0, 0, write_leaf_format)
                .barriers(MemoryBarriers::All);

            if options.hdr() {
                write_leaf_command = write_leaf_command
                    .image_3d(&lock_texture.as_ref().unwrap(), 1, 0, ImageInternalFormat::R32UI)
                    .image_3d(&count_texture.as_ref().unwrap(), 2, 0, ImageInternalFormat::R32UI);
            }

            renderer
                .draw(&write_leaf_command)
                .chain_err(|| "Could not write leafs")?;
        }

        let mut brickpool = Brickpool {
            albedos: albedos,
            irradiances: irradiances,

            options: *options,
            voxel_format: voxel_format,
        };

        // Set alpha to 1.0 to voxel at max level
        {
            let reset_alpha_pipeline_name = match voxel_format {
                VoxelTextureFormat::RGBA32 => "vxgi/svo/voxelize/brickpool/reset_alpha_rgba32",
                VoxelTextureFormat::RGBA8 => "vxgi/svo/voxelize/brickpool/reset_alpha",
            };
            let reset_alpha_pipeline = &*fetch_program(reset_alpha_pipeline_name);

            let reset_alpha_command = DrawCommand::arrays(reset_alpha_pipeline, 0, 27 * nodes_num)
                .mode(DrawMode::Points)
                .image_3d(&brickpool.albedos, 0, 0, voxel_format.into())
                .barriers(MemoryBarriers::All);

            renderer
                .draw(&reset_alpha_command)
                .chain_err(|| "Could not reset alpha in brickpool")?;
        }

        // Brickpool spread leaf
        {
            brickpool
                .spread_leaf(
                    renderer,
                    nodepool,
                    options.levels() as usize - 1usize,
                    BrickpoolType::Albedo,
                )
                .chain_err(|| "Could not spread leaf albedo")?;
        }

        {
            brickpool
                .transfer_neighbours(
                    renderer,
                    nodepool,
                    options.levels() as usize - 1usize,
                    BrickpoolType::Albedo,
                )
                .chain_err(|| "Could not spread leaf albedo")?;
        }

        // Brickpool mipmap
        {
            brickpool
                .mipmap(renderer, nodepool, BrickpoolType::Albedo)
                .chain_err(|| "Could not mipmap albedo")?;
        }

        Ok(brickpool)
    }

    pub fn spread_leaf(
        &mut self,
        renderer: &mut Renderer,
        nodepool: &Nodepool,
        level: usize,
        ty: BrickpoolType,
    ) -> Result<()> {
        let pipeline_name = match self.voxel_format {
            VoxelTextureFormat::RGBA32 => "vxgi/svo/voxelize/brickpool/spread_leaf_rgba32",
            VoxelTextureFormat::RGBA8 => "vxgi/svo/voxelize/brickpool/spread_leaf",
        };
        let pipeline = &*fetch_program(pipeline_name);

        let texture = match ty {
            BrickpoolType::Albedo => &self.albedos,
            BrickpoolType::Irradiance => &self.irradiances,
        };

        let command = DrawCommand::arrays(pipeline, 0, nodepool.count)
            .mode(DrawMode::Points)
            .uniform_1ui(level as u32, 0)
            .storage(&nodepool.position_buffer, 0)
            .image(texture, 0, 0, ImageInternalFormat::RGBA8)
            .barriers(MemoryBarriers::All);

        renderer.draw(&command).chain_err(|| "Could not spread leafs")?;

        Ok(())
    }

    pub fn transfer_neighbours(
        &mut self,
        renderer: &mut Renderer,
        nodepool: &Nodepool,
        level: usize,
        ty: BrickpoolType,
    ) -> Result<()> {
        let neighbours = [
            &nodepool.neighbours_x_buffer,
            &nodepool.neighbours_y_buffer,
            &nodepool.neighbours_z_buffer,
        ];

        let brickpool = match ty {
            BrickpoolType::Albedo => &self.albedos,
            BrickpoolType::Irradiance => &self.irradiances,
        };

        for axis in 0..3 {
            let pipeline_name = match self.voxel_format {
                VoxelTextureFormat::RGBA32 => "vxgi/svo/voxelize/brickpool/border_transfer_rgba32",
                VoxelTextureFormat::RGBA8 => "vxgi/svo/voxelize/brickpool/border_transfer",
            };
            let pipeline = &*fetch_program(pipeline_name);

            let command = DrawCommand::arrays(pipeline, 0, nodepool.count)
                .mode(DrawMode::Points)
                // Globals
                .uniform_1ui(level as u32, 0)
                .uniform_1ui(axis as u32, 1)
                // Brickpool
                .image_3d(brickpool, 0, 0, ImageInternalFormat::RGBA8)
                // Nodepool
                .storage(&nodepool.next_buffer, 0)
                .storage(&nodepool.position_buffer, 1)
                .storage(neighbours[axis], 2)
                .barriers(MemoryBarriers::All);

            renderer.draw(&command).chain_err(|| "Could not transfer borders")?;
        }

        Ok(())
    }

    pub fn mipmap(&mut self, renderer: &mut Renderer, nodepool: &Nodepool, ty: BrickpoolType) -> Result<()> {
        for level in (0..self.options.levels()).rev() {
            if level > 0 {
                self.transfer_neighbours(renderer, nodepool, level as usize, ty)
                    .chain_err(|| "Could not spread leaf during mipmap")?;
            }

            let mipmap_pipeline_name = match self.voxel_format {
                VoxelTextureFormat::RGBA32 => "vxgi/svo/voxelize/brickpool/mipmap_rgba32",
                VoxelTextureFormat::RGBA8 => "vxgi/svo/voxelize/brickpool/mipmap",
            };
            let mipmap_pipeline = &*fetch_program(mipmap_pipeline_name);

            let mut mipmap_command = DrawCommand::arrays(mipmap_pipeline, 0, nodepool.count)
                .mode(DrawMode::Points)
                .uniform_1ui(level - 1 as u32, 0)
                .image_3d(&self.irradiances, 0, 0, self.voxel_format.into())
                .storage(&nodepool.next_buffer, 5)
                .storage(&nodepool.position_buffer, 6)
                .barriers(MemoryBarriers::All);

            renderer.draw(&mipmap_command).chain_err(|| "Could not mipmap")?;
        }

        Ok(())
    }

    pub fn render_debug(
        &self,
        renderer: &mut Renderer,
        camera: &Camera,
        nodepool: &Nodepool,
        brickpool_level: usize,
        brickpool_type: BrickpoolType,
    ) -> Result<()> {
        renderer.set_enable(EnableOption::DepthTest);
        renderer.set_enable(EnableOption::CullFace);

        let brickpool = match brickpool_type {
            BrickpoolType::Albedo => &self.albedos,
            BrickpoolType::Irradiance => &self.irradiances,
        };

        let pipeline_name = match self.voxel_format {
            VoxelTextureFormat::RGBA32 => "vxgi/svo/visualize/brickpool_rgba32",
            VoxelTextureFormat::RGBA8 => "vxgi/svo/visualize/brickpool",
        };
        let pipeline = &*fetch_program(pipeline_name);

        // Number of nodes * 27 (number of voxels in brick)
        let draw_command = DrawCommand::arrays(pipeline, 0, nodepool.count * 27)
            .mode(DrawMode::Points)
            .uniform_1ui(brickpool_level as u32, 0)
            .uniform(camera.get_buffer(), 0)
            .storage_read(&nodepool.position_buffer, 0)
            .image(brickpool, 0, 0, self.voxel_format.into());

        renderer
            .draw(&draw_command)
            .chain_err(|| "Could not render debug view")?;

        Ok(())
    }
}
