use super::*;

mod voxel_fragment_list;
pub use self::voxel_fragment_list::*;

mod nodepool;
pub use self::nodepool::*;

mod brickpool;
pub use self::brickpool::*;

mod aniso_brickpool;
pub use self::aniso_brickpool::*;

/// Describes what kind of data Brickpool Holds.
///
/// All Brickpools are currently RGBA8.
#[derive(Clone, Copy, Debug)]
pub enum BrickpoolType {
    Albedo,
    Irradiance,
}
