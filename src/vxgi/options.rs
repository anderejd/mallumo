use super::*;
use cgmath::prelude::*;
use cgmath::{Matrix4, Rad, Vector4};

use rand::prelude::*;

/// Table taken from "The packing of circles on a hemisphere" paper (http://iopscience.iop.org/article/10.1088/0957-0233/10/11/307/meta)
/// Each row represents arrangement of cones in 4 concentric circles around hemisphere
#[cfg_attr(rustfmt, rustfmt_skip)]
const CONES_TABLE: [[f32; 17]; 31] = [
    [ 2.0,  0.7854,  0.7854,  0.0,       0.0,     0.0,     0.0,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.7854],
    [ 3.0,  0.7138,  0.7138,  0.0,       0.0,     0.0,     0.0,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.7138],
    [ 4.0,  0.6161,  0.6161,  0.0,       0.0,     0.0,     0.0,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.6161],
    [ 5.0,  0.5323,  0.5323,  0.0,       0.0,     0.0,     0.0,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.5323],
    [ 5.0,  0.5236,  0.5236,  0.0,       1.0,  1.5708,  0.5236,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.5236],
    [ 6.0,  0.4643,  0.4643,  0.0,       1.0,  1.5708,  0.6423,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.4643],
    [ 7.0,  0.4102,  0.4102,  0.0,       1.0,  1.5708,  0.7505,     0.0,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.4102],
    [ 7.0,  0.3927,  0.3927,  0.0,       2.0,  1.1781,  0.3927,  0.2251,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3927],
    [ 7.0,  0.3770,  0.3770,  0.0,       3.0,  1.1310,  0.3770,  0.1501,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3770],
    [ 8.0,  0.3648,  0.3648,  0.0,       3.0,  1.1188,  0.3892,  0.1309,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3648],
    [ 8.0,  0.3526,  0.3526,  0.0,       4.0,  1.0594,  0.3543,  0.3927,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3526],
    [ 9.0,  0.3299,  0.3299,  0.0,       4.0,  1.0315,  0.3718,  0.0873,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3299],
    [ 9.0,  0.3299,  0.3299,  0.0,       5.0,  0.9896,  0.3299,  0.0698,       0.0,     0.0,     0.0,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3299],
    [ 9.0,  0.3142,  0.3142,  0.0,       5.0,  0.9425,  0.3142,  0.0698,       1.0,  1.5708,  0.3142,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3142],
    [ 9.0,  0.3072,  0.3072,  0.0,       6.0,  0.9215,  0.3072,  0.1745,       1.0,  1.5708,  0.3421,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3072],
    [10.0,  0.3002,  0.3002,  0.0,       6.0,  0.9111,  0.3107,  0.1047,       1.0,  1.5708,  0.3491,     0.0,       0.0,      0.0,    0.0,  0.0,  0.3002],
    [10.0,  0.2862,  0.2862,  0.0,       7.0,  0.8604,  0.2880,  0.0454,       1.0,  1.5708,  0.4224,     0.0,       0.0,      0.0,    0.0,  0.0,  0.2862],
    [11.0,  0.2740,  0.2740,  0.0,       7.0,  0.8430,  0.2950,  0.0401,       1.0,  1.5708,  0.4328,     0.0,       0.0,      0.0,    0.0,  0.0,  0.2740],
    [11.0,  0.2688,  0.2688,  0.0,       8.0,  0.8063,  0.2688,  0.0367,       1.0,  1.5708,  0.4957,     0.0,       0.0,      0.0,    0.0,  0.0,  0.2688],
    [11.0,  0.2618,  0.2618,  0.0,       8.0,  0.7854,  0.2618,  0.0367,       2.0,  1.3090,  0.2618,  0.3927,       0.0,      0.0,    0.0,  0.0,  0.2618],
    [11.0,  0.2548,  0.2548,  0.0,       8.0,  0.7645,  0.2548,  0.0367,       3.0,  1.2758,  0.2566,  0.1309,       0.0,      0.0,    0.0,  0.0,  0.2548],
    [12.0,  0.2531,  0.2531,  0.0,       8.0,  0.7627,  0.2566,  0.1309,       3.0,  1.2758,  0.2566,  0.1309,       0.0,      0.0,    0.0,  0.0,  0.2531],
    [12.0,  0.2513,  0.2513,  0.0,       9.0,  0.7557,  0.2531,  0.0873,       3.0,  1.2689,  0.2601,  0.3491,       0.0,      0.0,    0.0,  0.0,  0.2513],
    [12.0,  0.2443,  0.2443,  0.0,       9.0,  0.7330,  0.2443,  0.0873,       4.0,  1.2217,  0.2443,  0.0873,       0.0,      0.0,    0.0,  0.0,  0.2443],
    [12.0,  0.2374,  0.2374,  0.0,      10.0,  0.7121,  0.2374,  0.0524,       4.0,  1.2025,  0.2531,  0.1571,       0.0,      0.0,    0.0,  0.0,  0.2374],
    [13.0,  0.2356,  0.2356,  0.0,      10.0,  0.7069,  0.2356,  0.0244,       4.0,  1.2025,  0.2601,  0.1571,       0.0,      0.0,    0.0,  0.0,  0.2356],
    [13.0,  0.2339,  0.2339,  0.0,      10.0,  0.7016,  0.2339,  0.0244,       5.0,  1.1694,  0.2339,  0.3142,       0.0,      0.0,    0.0,  0.0,  0.2339],
    [13.0,  0.2234,  0.2234,  0.0,      10.0,  0.6702,  0.2234,  0.0244,       5.0,  1.1188,  0.2251,  0.3142,       1.0,  1.5708,  0.2269,  0.0,  0.2234],
    [13.0,  0.2234,  0.2234,  0.0,      11.0,  0.6702,  0.2234,  0.0227,       5.0,  1.1188,  0.2251,  0.0576,       1.0,  1.5708,  0.2269,  0.0,  0.2234],
    [13.0,  0.2234,  0.2234,  0.0,      11.0,  0.6702,  0.2234,  0.0227,       6.0,  1.1170,  0.2234,  0.0471,       1.0,  1.5708,  0.2304,  0.0,  0.2234],
    [14.0,  0.2182,  0.2182,  0.0,      11.0,  0.6615,  0.2251,  0.0209,       6.0,  1.1100,  0.2251,  0.0471,       1.0,  1.5708,  0.2339,  0.0,  0.2182],
];

/// Method used to calculate distribution of cones
///
/// GLSL code:
///
/// ```glsl
/// const uint VoxelStructureVolumeTexture = 0;
/// const uint VoxelStructureSparseVoxelOctree = 1;
/// ```
///
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u32)]
pub enum VoxelStructure {
    VolumeTexture = 0u32,
    SparseVoxelOctree = 1u32,
}

impl From<usize> for VoxelStructure {
    fn from(voxel_structure: usize) -> VoxelStructure {
        match voxel_structure {
            0 => VoxelStructure::VolumeTexture,
            _ => VoxelStructure::SparseVoxelOctree,
        }
    }
}

impl From<u32> for VoxelStructure {
    fn from(voxel_structure: u32) -> VoxelStructure {
        match voxel_structure {
            0 => VoxelStructure::VolumeTexture,
            _ => VoxelStructure::SparseVoxelOctree,
        }
    }
}

/// Method used to calculate distribution of cones
///
/// GLSL code:
///
/// ```glsl
/// const uint ConeDistributionConcentricRegular = 0;
/// const uint ConeDistributionConcentricIrregular = 1;
/// const uint ConeDistributionRandom = 2;
/// ```
///
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u32)]
pub enum ConeDistribution {
    /// Concentric non-intersecting distribution of cones
    /// with regular apertures.
    ConcentricRegular = 0u32,

    /// Concentric non-intersecting distribution of cones
    /// with irregular apertures.
    ConcentricIrregular = 1u32,

    /// Random distribution
    Random = 2u32,
}

impl From<usize> for ConeDistribution {
    fn from(cone_distribution: usize) -> ConeDistribution {
        match cone_distribution {
            1 => ConeDistribution::ConcentricIrregular,
            2 => ConeDistribution::Random,
            _ => ConeDistribution::ConcentricRegular,
        }
    }
}

impl From<u32> for ConeDistribution {
    fn from(cone_distribution: u32) -> ConeDistribution {
        match cone_distribution {
            1 => ConeDistribution::ConcentricIrregular,
            2 => ConeDistribution::Random,
            _ => ConeDistribution::ConcentricRegular,
        }
    }
}

/// Mode of calculation of shadows
///
/// GLSL code:
///
/// ```glsl
/// const uint ShadowsModeNone = 0;
/// const uint ShadowsModeShadowmap = 1;
/// const uint ShadowsModeConeTraced = 2;
/// ```
///
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u32)]
pub enum ShadowsMode {
    /// No shadows are calculated
    None = 0u32,

    /// Sharp shadows are calculated using classic shadow map
    Shadowmap = 1u32,

    /// Soft shadows are calculated using cone tracing
    ConeTraced = 2u32,
}

impl From<usize> for ShadowsMode {
    fn from(cone_distribution: usize) -> ShadowsMode {
        match cone_distribution {
            1 => ShadowsMode::Shadowmap,
            2 => ShadowsMode::ConeTraced,
            _ => ShadowsMode::None,
        }
    }
}

impl From<u32> for ShadowsMode {
    fn from(cone_distribution: u32) -> ShadowsMode {
        match cone_distribution {
            1 => ShadowsMode::Shadowmap,
            2 => ShadowsMode::ConeTraced,
            _ => ShadowsMode::None,
        }
    }
}

/// Options for Voxel Based Global Illumination
///
/// GLSL code:
///
/// ```glsl
/// struct VoxelGiOptions {
///  vec4 cones[32],
///  uint cones_num,
///  uint cones_distribution,
///  uint levels,
///  uint dimension,
///  float calculate_direct,
///  float calculate_indirect_diffuse,
///  float calculate_indirect_specular,
///  float calculate_ambient_occlusion,
///  uint shadows_mode,
///  bool anisotropic,
///  bool hdr,
///  uint structure,
/// }
///
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct VXGIOptions {
    ///
    cones: [[f32; 4]; 32],
    cones_weights: [f32; 32],

    /// Number of cones to use for diffuse and ambient occlussion tracing.
    /// Valid values are in range [1; 32]
    cones_num: u32,

    /// Distribution of the cones. See `ConeDistribution`
    cones_distribution: ConeDistribution,

    /// How many mipmap levels should the 3D texture/octree have.
    levels: u32,

    /// Dimension of 3D texture or octree representing the 3d texture.
    /// Should be calculated based on `levels` parameter as: 2^levels.
    dimension: u32,

    /// Toggle (represented as u32 for OpenGL) representing whether
    /// direct lighting should be added to final computed pixel.
    pub calculate_direct: u32,

    /// Toggle (represented as u32 for OpenGL) representing whether
    /// cone traced indirect diffuse lighting should be added to final
    /// computed pixel.
    pub calculate_indirect_diffuse: u32,

    /// Toggle (represented as u32 for OpenGL) representing whether
    /// cone traced indirect specular lighting should be added to final
    /// computed pixel.
    pub calculate_indirect_specular: u32,

    /// Toggle (represented as u32 for OpenGL) representing whether
    /// cone traced ambient occlussion should be added to final computed pixel.
    pub calculate_ambient_occlusion: u32,

    /// Toggle (represented as u32 for OpenGL) representing how(if)
    /// shadows are added to final computed pixel.
    pub shadows_mode: ShadowsMode,

    /// Toggle (represented as u32 for OpenGL) representing whether
    /// to use isotropic(false) or anisotropic(true) voxels.
    /// Anisotropic voxels are mipmapped in 6 directions and during
    /// cone tracing 3 closest faces are averaged.
    pub anisotropic: u32,

    /// Toggle (represented as u32 for OpenGL) representing whether
    /// to use RGBA8(false) or RGBA32F(true) format for albedo and emission.
    /// Note that only RGBA32F has alpha averaged because RGBA8 mode uses
    /// Alpha channel for storing number of fragments.
    pub hdr: u32,

    ///
    pub voxel_structure: VoxelStructure,
}

impl VXGIOptions {
    pub fn new() -> VXGIOptions {
        let mut options = VXGIOptions {
            cones: [[0.0f32; 4]; 32],
            cones_weights: [0.0f32; 32],
            cones_num: 1,
            cones_distribution: ConeDistribution::ConcentricRegular,
            levels: 8,
            dimension: 256,
            calculate_direct: 1,
            calculate_indirect_diffuse: 1,
            calculate_indirect_specular: 0,
            calculate_ambient_occlusion: 0,
            shadows_mode: ShadowsMode::Shadowmap,
            anisotropic: 0,
            hdr: 0,
            voxel_structure: VoxelStructure::VolumeTexture,
        };

        options.set_cones(1, ConeDistribution::ConcentricRegular);

        options
    }

    pub fn from_arguments(arguments: &VXGIArguments) -> VXGIOptions {
        let mut options = VXGIOptions {
            cones: [[0.0f32; 4]; 32],
            cones_weights: [0.0f32; 32],
            cones_num: 1,
            cones_distribution: ConeDistribution::ConcentricRegular,
            levels: 8,
            dimension: 256,
            calculate_direct: !arguments.direct_off as u32,
            calculate_indirect_diffuse: !arguments.indirect_diffuse_off as u32,
            calculate_indirect_specular: arguments.indirect_specular as u32,
            calculate_ambient_occlusion: arguments.ambient_occlusion as u32,
            shadows_mode: arguments.shadows_mode.into(),
            anisotropic: arguments.anisotropic as u32,
            hdr: arguments.hdr as u32,
            voxel_structure: arguments.voxel_structure.into(),
        };

        options.set_levels(arguments.levels);
        options.set_cones(arguments.cones_num, arguments.cones_distribution.into());

        options
    }

    pub fn dimension(&self) -> u32 {
        self.dimension
    }

    pub fn cones(&self) -> [[f32; 4]; 32] {
        self.cones
    }

    pub fn cones_num(&self) -> u32 {
        self.cones_num
    }

    pub fn levels(&self) -> u32 {
        self.levels
    }

    pub fn set_levels(&mut self, levels: u32) {
        self.levels = levels;
        self.dimension = 2u32.pow(levels);
    }

    pub fn cones_distribution(&self) -> ConeDistribution {
        self.cones_distribution
    }

    pub fn set_cones(&mut self, cones_num: u32, distribution: ConeDistribution) {
        self.cones_num = cones_num;
        self.cones_distribution = distribution;

        if cones_num == 0 || cones_num > 32 {
            panic!("Wrong number of cones!");
        }

        if cones_num == 1 {
            self.cones[0] = [0.0, 1.0, 0.0, 0.5236];
            self.cones_weights[0] = 1.0;
            return;
        }

        if distribution == ConeDistribution::ConcentricRegular || distribution == ConeDistribution::ConcentricIrregular
        {
            let table_row = cones_num as usize - 2usize;
            let mut cone_n = 0;

            let mut total_coverage = 0.0f32;

            for circle in 0..4 {
                let circle_cone_count = CONES_TABLE[table_row][circle * 4] as usize;

                if circle_cone_count > 0 {
                    let elevation = CONES_TABLE[table_row][circle * 4 + 1];
                    let viewing_angle = match distribution {
                        ConeDistribution::ConcentricIrregular => CONES_TABLE[table_row][circle * 4 + 2],
                        _ => CONES_TABLE[table_row][16],
                    };
                    let angular_shift = CONES_TABLE[table_row][circle * 4 + 3];
                    let angular_shift_step = (2.0f32 * ::std::f32::consts::PI) / circle_cone_count as f32;

                    for n in 0..circle_cone_count {
                        let cone_angular_shift = angular_shift_step * (n as f32) + angular_shift;

                        let base = Vector4::new(0.0, 0.0, 1.0, 1.0);

                        let rotation_x = Matrix4::from_angle_x(Rad(-elevation));
                        let rotation_y = Matrix4::from_angle_y(Rad(cone_angular_shift));

                        let cone = rotation_y * rotation_x * base;
                        let cone = cone.truncate().normalize();

                        // cone weight calculation
                        let beta = ::std::f32::consts::PI / 2.0 - viewing_angle;
                        let r = beta.sin();
                        let a = beta.sin();
                        let h = 1.0 - a;
                        let coverage = ::std::f32::consts::PI * (r * r + h * h) / 2.0;
                        total_coverage += coverage;

                        self.cones[cone_n] = [cone[0], cone[1], cone[2], viewing_angle];
                        self.cones_weights[cone_n] = coverage;
                        cone_n += 1;
                    }
                }
            }

            // Redistribute rest of the area in cone weights, so that we get sum of 1
            let remaining_coverage = 1.0f32 - total_coverage;

            for cone_weight in &mut self.cones_weights {
                *cone_weight += remaining_coverage / (self.cones_num as f32);
            }
        } else {
            let mut rng = thread_rng();
            for cone_n in 0..cones_num as usize {
                self.cones[cone_n] = [rng.gen::<f32>(), rng.gen::<f32>(), rng.gen::<f32>(), 0.3072];
                self.cones_weights[cone_n] = 1.0 / cones_num as f32;
            }
        }
    }

    pub fn anisotropic(&self) -> bool {
        if self.anisotropic == 1 {
            true
        } else {
            false
        }
    }

    pub fn set_isotropic(&mut self) {
        self.anisotropic = 0;
    }

    pub fn set_anisotropic(&mut self) {
        self.anisotropic = 1;
    }

    pub fn hdr(&self) -> bool {
        if self.hdr == 1 {
            true
        } else {
            false
        }
    }

    pub fn set_hdr(&mut self) {
        self.hdr = 1;
    }

    pub fn set_ldr(&mut self) {
        self.hdr = 0;
    }

    pub fn calculate_direct(&self) -> bool {
        if self.calculate_direct == 1 {
            true
        } else {
            false
        }
    }

    pub fn set_calculate_direct(&mut self, calculate_direct: bool) {
        self.calculate_direct = calculate_direct as u32;
    }

    pub fn calculate_indirect_diffuse(&self) -> bool {
        if self.calculate_indirect_diffuse == 1 {
            true
        } else {
            false
        }
    }

    pub fn set_calculate_indirect_diffuse(&mut self, calculate_indirect_diffuse: bool) {
        self.calculate_indirect_diffuse = calculate_indirect_diffuse as u32;
    }

    pub fn calculate_indirect_specular(&self) -> bool {
        if self.calculate_indirect_specular == 1 {
            true
        } else {
            false
        }
    }

    pub fn set_calculate_indirect_specular(&mut self, calculate_indirect_specular: bool) {
        self.calculate_indirect_specular = calculate_indirect_specular as u32;
    }

    pub fn calculate_ambient_occlusion(&self) -> bool {
        if self.calculate_ambient_occlusion == 1 {
            true
        } else {
            false
        }
    }

    pub fn set_calculate_ambient_occlusion(&mut self, calculate_ambient_occlusion: bool) {
        self.calculate_ambient_occlusion = calculate_ambient_occlusion as u32;
    }
}
