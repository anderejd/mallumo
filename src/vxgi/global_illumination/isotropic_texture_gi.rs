use super::*;

use gltf_support::*;
use mallumo_gls::*;
use shader_loader::*;
use shape_list::*;
use sun::*;

pub struct IsotropicTextureGI {
    framebuffer: GeneralFramebuffer,

    pub options: VXGIOptions,
    pub options_buffer: MutableBuffer,

    pub albedo: IsotropicVoxelTexture,
    pub normal: Texture3D,
    pub emission: IsotropicVoxelTexture,
    pub radiance: IsotropicVoxelTexture,

    /// Static flag texture indicating whether the voxel at position
    /// belongs to static geometry. (R8UI)
    pub static_flag_texture: Texture3D,

    /// Lock texture for averaging of RGBA32F values. (R8UI)
    pub lock_texture: Option<Texture3D>,

    /// Count texture for averaging of RGBA32F values. (R16UI)
    pub count_texture: Option<Texture3D>,

    /// Debug cone path
    pub cone_path: MutableBuffer,
}

impl IsotropicTextureGI {
    pub fn new(options: VXGIOptions) -> Result<IsotropicTextureGI> {
        let format = match options.hdr() {
            false => VoxelTextureFormat::RGBA8,
            true => VoxelTextureFormat::RGBA32,
        };

        let levels = options.levels() as usize;
        let dimension = options.dimension() as usize;

        let mut framebufer = GeneralFramebuffer::new();
        framebufer.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: dimension,
            height: dimension,
        });
        framebufer.set_parameter(FramebufferParameter::DefaultWidth, dimension as u32);
        framebufer.set_parameter(FramebufferParameter::DefaultHeight, dimension as u32);
        framebufer.set_disable(EnableOption::DepthTest);
        framebufer.set_disable(EnableOption::CullFace);
        framebufer.set_clip_control_depth(ClipControlDepth::NegativeOneToOne);

        let options_buffer = MutableBuffer::new(&[options]).chain_err(|| "Could not create options buffer")?;

        let albedo = IsotropicVoxelTexture::new(levels, format).chain_err(|| "")?;
        let emission = IsotropicVoxelTexture::new(levels, format).chain_err(|| "")?;
        let radiance = IsotropicVoxelTexture::new(levels, format).chain_err(|| "")?;

        let texture_size = Texture3DSize(dimension, dimension, dimension);

        let parameters = Texture3DParameters {
            min: TextureTexelFilter::Nearest,
            mag: TextureTexelFilter::Nearest,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
        };

        let normal = Texture3D::new_empty(
            texture_size,
            format.into(),
            TextureFormat::RGBA,
            TextureDataType::Float,
            parameters,
            1,
        ).chain_err(|| "")?;

        let static_flag_texture = Texture3D::new_empty(
            texture_size,
            TextureInternalFormat::R8UI,
            TextureFormat::RedInteger,
            TextureDataType::UnsignedByte,
            parameters,
            1,
        ).chain_err(|| "")?;

        let mut lock_texture = match options.hdr() {
            false => None,
            true => Some(Texture3D::new_empty(
                texture_size,
                TextureInternalFormat::R32UI,
                TextureFormat::RedInteger,
                TextureDataType::UnsignedInt,
                parameters,
                1,
            ).chain_err(|| "")?),
        };

        if options.hdr() {
            lock_texture.as_mut().unwrap().clear().chain_err(|| "")?;
        }

        let count_texture = match options.hdr() {
            false => None,
            true => Some(Texture3D::new_empty(
                texture_size,
                TextureInternalFormat::R32UI,
                TextureFormat::RedInteger,
                TextureDataType::UnsignedInt,
                parameters,
                1,
            ).chain_err(|| "")?),
        };

        let mut cone_path = MutableBuffer::new_empty(4112).chain_err(|| "")?;
        cone_path.clear_data().chain_err(|| "")?;
        let isotropic_texture_gi = IsotropicTextureGI {
            framebuffer: framebufer,

            options: options,
            options_buffer: options_buffer,

            albedo: albedo,
            normal: normal,
            emission: emission,
            radiance: radiance,

            static_flag_texture: static_flag_texture,

            lock_texture: lock_texture,
            count_texture: count_texture,

            cone_path: cone_path,
        };

        Ok(isotropic_texture_gi)
    }
}

impl VXGIModule for IsotropicTextureGI {
    /// Voxelizes given scene.
    fn voxelize(&mut self, renderer: &mut Renderer, shape_lists: &[&ShapeList], static_geometry: bool) -> Result<()> {
        let format = match self.options.hdr {
            0 => VoxelTextureFormat::RGBA8,
            _ => VoxelTextureFormat::RGBA32,
        };

        if static_geometry {
            self.albedo
                .clear_mipmap(0)
                .chain_err(|| "Could not clear albedo voxel texture")?;

            self.normal
                .clear_mipmap(0)
                .chain_err(|| "Could not clear normal voxel texture")?;

            self.emission
                .clear_mipmap(0)
                .chain_err(|| "Could not clear albedo voxel texture")?;

            self.static_flag_texture
                .clear()
                .chain_err(|| "Could not clear static flag voxel texture")?;

            if self.options.hdr() {
                self.count_texture.as_mut().unwrap().clear().chain_err(|| "")?;
            }
        } else {
            let clear_pipeline_name = match self.options.hdr() {
                true => "vxgi/voxel_texture/clear/clear_dynamic_rgba32",
                false => "vxgi/voxel_texture/clear/clear_dynamic_rgba8",
            };
            let clear_pipeline = &*fetch_compute_program(clear_pipeline_name);

            let size = ((self.options.dimension() + 7) / 8) as usize;
            let mut clear_command = DrawCommand::compute(clear_pipeline, size, size, size)
                .uniform(&self.options_buffer, 0)
                .image_3d(&self.static_flag_texture, 0, 0, ImageInternalFormat::R8UI)
                .image_3d(self.albedo.level(0), 1, 0, format.into())
                .image_3d(&self.normal, 2, 0, format.into())
                .image_3d(self.emission.level(0), 3, 0, format.into())
                .barriers(MemoryBarriers::All);

            if self.options.hdr() {
                clear_command =
                    clear_command.image_3d(self.count_texture.as_ref().unwrap(), 4, 0, ImageInternalFormat::R32UI);
            }

            renderer.draw(&clear_command).chain_err(|| "Could not clear dynamic")?;
        }

        let voxelize_pipeline_name = match self.options.hdr() {
            true => "vxgi/voxel_texture/voxelize/voxelize_rgba32",
            false => "vxgi/voxel_texture/voxelize/voxelize_rgba8",
        };
        let voxelize_pipeline = &*fetch_program(voxelize_pipeline_name);

        for shape_list in shape_lists.iter() {
            for (i, shape) in shape_list.shapes.iter().enumerate() {
                let indices = shape.indices;
                let vertices = shape.vertices;

                let draw_command = DrawCommand::arrays(voxelize_pipeline, 0, shape.indices.1)
                    .framebuffer(&self.framebuffer)
                    .uniform(&self.options_buffer, 0)
                    .uniform_1ui(static_geometry as u32, 0)
                    .storage_range_read::<u32>(&shape_list.indices_buffer, 1, indices.0, indices.1)
                    .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 2, vertices.0, vertices.1)
                    .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 3, i, 1)
                    .texture_2d(shape_list.texture(shape.albedo), 0)
                    .texture_2d(shape_list.texture(shape.metallic_roughness), 2)
                    .texture_2d(shape_list.texture(shape.occlusion), 3)
                    .texture_2d(shape_list.texture(shape.emissive), 4)
                    .barriers(MemoryBarriers::All);

                let draw_command = match self.options.hdr() {
                    false => draw_command
                        .image_3d(self.albedo.level(0), 0, 0, ImageInternalFormat::R32UI)
                        .image_3d(&self.normal, 1, 0, ImageInternalFormat::R32UI)
                        .image_3d(self.emission.level(0), 2, 0, ImageInternalFormat::R32UI)
                        .image_3d(&self.static_flag_texture, 3, 0, ImageInternalFormat::R8UI),
                    true => draw_command
                        .image_3d(self.albedo.level(0), 0, 0, ImageInternalFormat::RGBA32F)
                        .image_3d(&self.normal, 1, 0, ImageInternalFormat::RGBA32F)
                        .image_3d(self.emission.level(0), 2, 0, ImageInternalFormat::RGBA32F)
                        .image_3d(&self.static_flag_texture, 3, 0, ImageInternalFormat::R8UI)
                        .image_3d(&self.lock_texture.as_ref().unwrap(), 4, 0, ImageInternalFormat::R32UI)
                        .image_3d(&self.count_texture.as_ref().unwrap(), 5, 0, ImageInternalFormat::R32UI),
                };

                renderer
                    .draw(&draw_command)
                    .chain_err(|| "Could not voxelize shape list")?;
            }
        }

        if !self.options.hdr() {
            let clear_alpha_pipeline_name = match self.options.hdr() {
                true => "vxgi/voxel_texture/clear/clear_alpha_rgba32",
                false => "vxgi/voxel_texture/clear/clear_alpha_rgba8",
            };
            let clear_alpha_pipeline = &*fetch_compute_program(clear_alpha_pipeline_name);

            let size = ((self.options.dimension() + 7) / 8) as usize;
            let clear_alpha_command = DrawCommand::compute(clear_alpha_pipeline, size, size, size)
                .uniform(&self.options_buffer, 0)
                .uniform_1ui(static_geometry as u32, 0)
                .image_3d(self.albedo.level(0), 0, 0, ImageInternalFormat::RGBA8)
                .image_3d(&self.static_flag_texture, 1, 0, ImageInternalFormat::R8UI)
                .barriers(MemoryBarriers::All);

            renderer
                .draw(&clear_alpha_command)
                .chain_err(|| "Could not clear alpha")?;
        }

        self.albedo
            .generate_mipmap(renderer)
            .chain_err(|| "Could not mipmap albedo texture")?;

        self.emission
            .generate_mipmap(renderer)
            .chain_err(|| "Could not mipmap albedo texture")?;

        Ok(())
    }

    fn inject_radiance(&mut self, renderer: &mut Renderer, sun: &Sun) -> Result<()> {
        let format = match self.options.hdr {
            0 => VoxelTextureFormat::RGBA8,
            _ => VoxelTextureFormat::RGBA32,
        };

        let pipeline_name = match self.options.hdr() {
            true => "vxgi/voxel_texture/inject_radiance_rgba32",
            false => "vxgi/voxel_texture/inject_radiance_rgba8",
        };

        {
            let pipeline = &*fetch_compute_program(pipeline_name);

            self.radiance.clear().chain_err(|| "Could not clear radiance")?;

            let workgroup_size = ((self.options.dimension() + 7) / 8) as usize;
            let command = DrawCommand::compute(pipeline, workgroup_size, workgroup_size, workgroup_size)
                .uniform(&self.options_buffer, 0)
                .uniform(sun.get_buffer(), 1)
                .texture_3d(self.albedo.level(0), 0)
                .texture_3d(&self.normal, 1)
                .texture_3d(self.emission.level(0), 2)
                .texture_2d(sun.get_shadowmap(), 3)
                .image_3d(&self.radiance.level(0), 0, 0, format.into())
                .barriers(MemoryBarriers::All);

            renderer.draw(&command).chain_err(|| "Could not inject radiance")?;
        }

        self.radiance
            .generate_mipmap(renderer)
            .chain_err(|| "Could not mipmap radiance texture")?;

        Ok(())
    }

    fn render_voxels(
        &self,
        renderer: &mut Renderer,
        camera: &Camera,
        level: usize,
        ty: VoxelTextureType,
    ) -> Result<()> {
        let visualize_pipeline = &*fetch_program("vxgi/voxel_texture/visualize/isotropic");

        let dimension = self.options.dimension() / 2u32.pow(level as u32);
        let voxels = dimension * dimension * dimension;

        let visualize_command = DrawCommand::arrays(visualize_pipeline, 0, voxels as usize)
            .mode(DrawMode::Points)
            .uniform_1ui(dimension, 0)
            .uniform(&self.options_buffer, 0)
            .uniform(camera.get_buffer(), 1);

        let visualize_command = match ty {
            VoxelTextureType::Albedo => visualize_command.texture_3d(self.albedo.level(level), 0),
            VoxelTextureType::Normal => visualize_command.texture_3d(&self.normal, 0),
            VoxelTextureType::Radiance => visualize_command.texture_3d(self.radiance.level(level), 0),
            _ => visualize_command,
        };

        renderer
            .draw(&visualize_command)
            .chain_err(|| "Could not visualize voxel structure")?;

        Ok(())
    }

    fn render(
        &mut self,
        renderer: &mut Renderer,
        deferred_collector: &DefferedCollector,
        camera: &Camera,
        sun: &Sun,
    ) -> Result<()> {
        {
            let pipeline = &*fetch_program("vxgi/voxel_texture/isotropic_gi");

            renderer.set_enable(EnableOption::CullFace);
            renderer.set_cull_face(Face::Back);

            let mut render_command = DrawCommand::arrays(pipeline, 0, 3)
                // Global uniforms
                .uniform(&self.options_buffer, 0)
                .uniform(camera.get_buffer(), 1)
                .uniform(sun.get_buffer(), 2)
                .storage(&self.cone_path, 0)
                .texture_2d(sun.get_shadowmap(), 5)
                // Deferred G-buffer
                .texture_2d(&deferred_collector.position, 0)
                .texture_2d(&deferred_collector.albedo, 1)
                .texture_2d(&deferred_collector.orm, 2)
                .texture_2d(&deferred_collector.normal, 3)
                .texture_2d(&deferred_collector.emission, 4);

            for level in 0..self.options.levels() as usize + 1usize {
                render_command = render_command
                    .texture_3d(self.radiance.level(level), 6usize + level)
                    .texture_3d(self.emission.level(level), 16usize + level);
            }

            renderer.draw(&render_command).chain_err(|| "Could not render GI")?;
        }

        Ok(())
    }

    fn update_options(&mut self, options: VXGIOptions) -> Result<()> {
        self.options = options;
        self.options_buffer
            .set_data::<VXGIOptions>(&[options])
            .chain_err(|| "Could not update options buffer")?;

        Ok(())
    }
}
