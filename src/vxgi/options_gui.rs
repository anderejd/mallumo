use super::*;

use conrod::*;
use glutin;
use gui::*;
use std;

const HEADER_LABEL_STYLE: widget::primitive::text::Style = widget::primitive::text::Style {
    font_size: Some(16),
    color: Some(color::WHITE),
    maybe_wrap: None,
    line_spacing: None,
    justify: None,
    font_id: None,
};

const LABEL_STYLE: widget::primitive::text::Style = widget::primitive::text::Style {
    font_size: Some(14),
    color: Some(color::WHITE),
    maybe_wrap: None,
    line_spacing: None,
    justify: None,
    font_id: None,
};

widget_ids! {
    struct Ids {
        panel,

        // Sun Panel
        canvas_sun,
        canvas_sun_header,
        canvas_sun_title,

        angle_x_title,
        angle_x,
        angle_y_title,
        angle_y,

        // Global Illumination Panel
        canvas_gi,
        canvas_gi_header,
        canvas_gi_title,

        voxel_structure_title,
        voxel_structure,

        levels_title,
        levels,
        anisotropic_title,
        anisotropic,
        hdr_title,
        hdr,

        cones_num_title,
        cones_num,

        cones_distribution_title,
        cones_distribution,

        calculate_direct_title,
        calculate_direct,
        calculate_indirect_diffuse_title,
        calculate_indirect_diffuse,
        calculate_indirect_specular_title,
        calculate_indirect_specular,
        calculate_ambient_occlusion_title,
        calculate_ambient_occlusion,

        shadows_mode_title,
        shadows_mode,

        // Post process Panel
    }
}

pub struct SunVariables {
    pub angle_x: f32,
    pub angle_y: f32,
    pub update: bool,
}

pub struct VXGIOptionsUI {
    ui: Ui,
    ids: self::Ids,
    image_map: image::Map<Texture2D>,
    pub sun_variables: self::SunVariables,
    pub gi_options: VXGIOptions,
    pub update_gi_structure: bool,
    pub update_gi_options: bool,
}

impl VXGIOptionsUI {
    pub fn new(width: usize, height: usize, options: VXGIOptions) -> VXGIOptionsUI {
        let mut ui = UiBuilder::new([width as f64, height as f64]).build();

        let ids = self::Ids::new(ui.widget_id_generator());

        // Add fonts to ui
        ui.fonts
            .insert_from_file("assets/fonts/NotoSans/NotoSans-Regular.ttf")
            .unwrap();

        // The image map describing widget->image mappings
        let image_map = image::Map::<Texture2D>::new();

        VXGIOptionsUI {
            ui: ui,
            ids: ids,
            image_map: image_map,
            sun_variables: SunVariables {
                angle_x: std::f32::consts::PI / 4.0f32,
                angle_y: 1.0766133,
                update: false,
            },
            gi_options: options,
            update_gi_options: false,
            update_gi_structure: false,
        }
    }

    pub fn get_ui<'a>(&'a self) -> &'a Ui {
        &self.ui
    }

    pub fn get_image_map<'a>(&'a self) -> &'a image::Map<Texture2D> {
        &self.image_map
    }

    pub fn process_event(&mut self, event: glutin::Event, window: &glutin::GlWindow) {
        let input = match convert_event(event, &window) {
            None => return,
            Some(input) => input,
        };

        self.ui.handle_event(input);
    }

    pub fn update_ui(&mut self) {
        let ui = &mut self.ui.set_widgets();

        let panel_padding = 10.0;

        widget::Canvas::new()
                .flow_down(&[
                    (self.ids.canvas_sun_header, widget::Canvas::new()
                        .length(32.0)
                        .color(Color::Rgba(0.6, 0.6, 0.6, 1.0))
                        .border(0.0)
                    ),
                    (
                        self.ids.canvas_sun,
                        widget::Canvas::new()
                            .length(110.0)
                            .border(0.0)
                            .pad(panel_padding)
                            .color(Color::Rgba(0.0, 0.0, 0.0, 0.0)),
                    ),
                    (self.ids.canvas_gi_header, widget::Canvas::new()
                        .length(32.0)
                        .color(Color::Rgba(0.6, 0.6, 0.6, 1.0))
                        .border(0.0)
                    ),
                    (
                        self.ids.canvas_gi,
                        widget::Canvas::new()
                            .border(0.0)
                            .pad(panel_padding)
                            .color(Color::Rgba(0.0, 0.0, 0.0, 0.0)),
                    ),
                ])
                .w(250.0)
                //.scroll_kids_vertically()
                .top_left_of(ui.window)
                .color(Color::Rgba(0.4, 0.4, 0.4, 1.0))
                .set(self.ids.panel, ui);

        widget::Text::new("SUN")
            .with_style(HEADER_LABEL_STYLE)
            .middle_of(self.ids.canvas_sun_header)
            .set(self.ids.canvas_sun_title, ui);

        widget::Text::new("Rotation")
            .with_style(LABEL_STYLE)
            .top_left_of(self.ids.canvas_sun)
            .set(self.ids.angle_x_title, ui);

        if let Some(angle_x) = widget::Slider::new(self.sun_variables.angle_x, 0.0, std::f32::consts::PI)
            .kid_area_w_of(self.ids.canvas_sun)
            .h(10.0)
            .down_from(self.ids.angle_x_title, 10.0)
            .set(self.ids.angle_x, ui)
        {
            if self.sun_variables.angle_x != angle_x {
                self.sun_variables.update = true;
            }
            self.sun_variables.angle_x = angle_x;
        };

        widget::Text::new("Elevation")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.angle_x, 10.0)
            .set(self.ids.angle_y_title, ui);

        if let Some(angle_y) = widget::Slider::new(self.sun_variables.angle_y, 0.0, std::f32::consts::PI)
            .kid_area_w_of(self.ids.canvas_sun)
            .h(10.0)
            .down_from(self.ids.angle_y_title, 10.0)
            .set(self.ids.angle_y, ui)
        {
            if self.sun_variables.angle_y != angle_y {
                self.sun_variables.update = true;
            }
            self.sun_variables.angle_y = angle_y;
        };

        widget::Text::new("GLOBAL ILLUMINATION")
            .with_style(HEADER_LABEL_STYLE)
            .middle_of(self.ids.canvas_gi_header)
            .set(self.ids.canvas_gi_title, ui);

        widget::Text::new("Voxel Structure")
            .with_style(LABEL_STYLE)
            .top_left_of(self.ids.canvas_gi)
            .set(self.ids.voxel_structure_title, ui);

        if let Some(voxel_structure) = widget::drop_down_list::DropDownList::new(
            &["Texture", "SVO"],
            Some(self.gi_options.voxel_structure as u32 as usize),
        ).w(100.0)
            .h(32.0)
            .label_y(position::Relative::Align(position::Align::Middle))
            .scrollbar_next_to()
            .down_from(self.ids.voxel_structure_title, 10.0)
            .set(self.ids.voxel_structure, ui)
        {
            if self.gi_options.voxel_structure as usize != voxel_structure {
                self.gi_options.voxel_structure = voxel_structure.into();
                self.update_gi_structure = true;
            }
        }

        widget::Text::new("Voxel levels")
            .with_style(LABEL_STYLE)
            .right_from(self.ids.voxel_structure_title, 10.0)
            .set(self.ids.levels_title, ui);

        if let Some(level) = widget::drop_down_list::DropDownList::new(
            &["1", "2", "3", "4", "5", "6", "7", "8", "9"],
            Some(self.gi_options.levels() as usize - 1),
        ).h(32.0)
            .label_y(position::Relative::Align(position::Align::Middle))
            .down_from(self.ids.levels_title, 10.0)
            .set(self.ids.levels, ui)
        {
            if self.gi_options.levels() != level as u32 + 1 {
                self.gi_options.set_levels(level as u32 + 1);
                self.update_gi_structure = true;
            }
        }

        widget::Text::new("Anisotropy")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.voxel_structure, 10.0)
            .set(self.ids.anisotropic_title, ui);

        let current_anistropy_value = if self.gi_options.anisotropic == 1 { true } else { false };
        let mut anisotropy = widget::toggle::Toggle::new(current_anistropy_value)
            .w(32.0)
            .h(32.0)
            .down_from(self.ids.anisotropic_title, 10.0)
            .set(self.ids.anisotropic, ui);

        if let Some(anisotropy) = anisotropy.next() {
            self.gi_options.anisotropic = if anisotropy { 1 } else { 0 };
            self.update_gi_structure = true;
        }

        widget::Text::new("HDR voxels")
            .with_style(LABEL_STYLE)
            .right_from(self.ids.anisotropic_title, 10.0)
            .set(self.ids.hdr_title, ui);

        let mut hdr = widget::toggle::Toggle::new(self.gi_options.hdr())
            .w(32.0)
            .h(32.0)
            .down_from(self.ids.hdr_title, 10.0)
            .set(self.ids.hdr, ui);

        if let Some(hdr) = hdr.next() {
            self.gi_options.hdr = hdr as u32;
            self.update_gi_structure = true;
        }

        widget::Text::new("Number of cones")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.anisotropic, 10.0)
            .set(self.ids.cones_num_title, ui);

        if let Some(cones_num) = widget::drop_down_list::DropDownList::new(
            &[
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
                "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32",
            ],
            Some(self.gi_options.cones_num() as usize - 1),
        ).w(200.0)
            .h(32.0)
            .label_y(position::Relative::Align(position::Align::Middle))
            .max_visible_items(8)
            .scrollbar_next_to()
            .down_from(self.ids.cones_num_title, 10.0)
            .set(self.ids.cones_num, ui)
        {
            if self.gi_options.cones_num() != cones_num as u32 + 1 {
                self.gi_options
                    .set_cones(cones_num as u32 + 1, ConeDistribution::ConcentricRegular);
                self.update_gi_options = true;
            }
        }

        widget::Text::new("Cones distribution")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.cones_num, 10.0)
            .set(self.ids.cones_distribution_title, ui);

        if let Some(cones_distribution) = widget::drop_down_list::DropDownList::new(
            &["Concentric Regular", "Concentric Irregular", "Random"],
            Some(self.gi_options.cones_distribution() as u32 as usize),
        ).w(200.0)
            .h(32.0)
            .label_y(position::Relative::Align(position::Align::Middle))
            .scrollbar_next_to()
            .down_from(self.ids.cones_distribution_title, 10.0)
            .set(self.ids.cones_distribution, ui)
        {
            if self.gi_options.cones_distribution() as u32 != cones_distribution as u32 {
                let cones_num = self.gi_options.cones_num();
                self.gi_options
                    .set_cones(cones_num, ConeDistribution::from(cones_distribution));
                self.update_gi_options = true;
            }
        }

        widget::Text::new("Direct")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.cones_distribution, 10.0)
            .set(self.ids.calculate_direct_title, ui);

        let mut direct = widget::toggle::Toggle::new(self.gi_options.calculate_direct())
            .w(32.0)
            .h(32.0)
            .down_from(self.ids.calculate_direct_title, 10.0)
            .set(self.ids.calculate_direct, ui);

        if let Some(direct) = direct.next() {
            self.gi_options.calculate_direct = direct as u32;
            self.update_gi_options = true;
        }

        widget::Text::new("Indirect diffuse")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.calculate_direct, 10.0)
            .set(self.ids.calculate_indirect_diffuse_title, ui);

        let mut indirect_diffuse = widget::toggle::Toggle::new(self.gi_options.calculate_indirect_diffuse())
            .w(32.0)
            .h(32.0)
            .down_from(self.ids.calculate_indirect_diffuse_title, 10.0)
            .set(self.ids.calculate_indirect_diffuse, ui);

        if let Some(indirect_diffuse) = indirect_diffuse.next() {
            self.gi_options.calculate_indirect_diffuse = indirect_diffuse as u32;
            self.update_gi_options = true;
        }

        widget::Text::new("Indirect specular")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.calculate_indirect_diffuse, 10.0)
            .set(self.ids.calculate_indirect_specular_title, ui);

        let mut indirect_specular = widget::toggle::Toggle::new(self.gi_options.calculate_indirect_specular())
            .w(32.0)
            .h(32.0)
            .down_from(self.ids.calculate_indirect_specular_title, 10.0)
            .set(self.ids.calculate_indirect_specular, ui);

        if let Some(indirect_specular) = indirect_specular.next() {
            self.gi_options.calculate_indirect_specular = indirect_specular as u32;
            self.update_gi_options = true;
        }

        widget::Text::new("Ambient occlusion")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.calculate_indirect_specular, 10.0)
            .set(self.ids.calculate_ambient_occlusion_title, ui);

        let mut ambient_occlusion = widget::toggle::Toggle::new(self.gi_options.calculate_ambient_occlusion())
            .w(32.0)
            .h(32.0)
            .down_from(self.ids.calculate_ambient_occlusion_title, 10.0)
            .set(self.ids.calculate_ambient_occlusion, ui);

        if let Some(ambient_occlusion) = ambient_occlusion.next() {
            self.gi_options.calculate_ambient_occlusion = ambient_occlusion as u32;
            self.update_gi_options = true;
        }

        widget::Text::new("Shadows mode")
            .with_style(LABEL_STYLE)
            .down_from(self.ids.calculate_ambient_occlusion, 10.0)
            .set(self.ids.shadows_mode_title, ui);

        if let Some(shadows_mode) = widget::drop_down_list::DropDownList::new(
            &["None", "Shadowmap", "Cone Traced"],
            Some(self.gi_options.shadows_mode as u32 as usize),
        ).w(200.0)
            .h(32.0)
            .label_y(position::Relative::Align(position::Align::Middle))
            .scrollbar_next_to()
            .down_from(self.ids.shadows_mode_title, 10.0)
            .set(self.ids.shadows_mode, ui)
        {
            if self.gi_options.shadows_mode as u32 != shadows_mode as u32 {
                self.gi_options.shadows_mode = ShadowsMode::from(shadows_mode as u32);
                self.update_gi_options = true;
            }
        }
    }
}
