/// Options for Voxel Based Global Illumination
#[derive(StructOpt, Debug, Clone)]
#[structopt(name = "vxgi")]
pub struct VXGIArguments {
    /// Number of cones to use for diffuse and ambient occlussion tracing.
    /// Valid values are in range [1; 32]
    #[structopt(long = "cones_num", default_value = "9")]
    pub cones_num: u32,

    /// Distribution of the cones
    /// 1 - concentric regular
    /// 2 - concentric irregular
    /// 3 - random
    #[structopt(long = "cones_distribution", default_value = "0")]
    pub cones_distribution: u32,

    /// How many mipmap levels should the 3D texture/octree have.
    #[structopt(long = "levels", default_value = "8")]
    pub levels: u32,

    /// Remove direct lighting from final image
    #[structopt(long = "direct_off")]
    pub direct_off: bool,

    /// Remove indirect diffuse from final image
    #[structopt(long = "indirect_diffuse_off")]
    pub indirect_diffuse_off: bool,

    /// Remove indirect specular from final image
    #[structopt(long = "indirect_specular")]
    pub indirect_specular: bool,

    /// Remove ambient occlusion from final image
    #[structopt(long = "ambient_occlusion")]
    pub ambient_occlusion: bool,

    /// Shadows mode:
    /// 1 - no shadows
    /// 2 - direct shadows
    /// 3 - cone traced shadows
    #[structopt(long = "shadows_mode", default_value = "1")]
    pub shadows_mode: u32,

    /// Toggle anisotropic voxels
    #[structopt(long = "anisotropic")]
    pub anisotropic: bool,

    /// Toggle HDR support
    #[structopt(long = "hdr")]
    pub hdr: bool,

    /// Voxel structure:
    /// 0 - 3D Texture
    /// 1 - Sparse Voxel Octree
    #[structopt(long = "voxel_structure", default_value = "0")]
    pub voxel_structure: u32,

    /// Scene files
    #[structopt(name = "FILE")]
    pub files: Vec<String>,
}
