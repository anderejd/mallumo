use camera::*;
use errors::*;
use gltf_support::*;
use ibl::*;
use lights::*;
use mallumo_gls::*;
use shader_loader::*;
use shape_list::*;
use sun::*;

/// Structure used to render scene supporting different modes for rendering.
///
/// Scene Renderer contains its own pipelines and other structures used to render given scene.
pub struct SceneRenderer {
    brdf_lut: Texture2D,

    globals_buffer: MutableBuffer,
}

impl SceneRenderer {
    /// Creates new Scene Renderer.
    ///
    /// This function needs to have access to Renderer because Scene Renderer creates its own LUT textures/buffers.
    pub fn new(mut renderer: &mut Renderer) -> Result<SceneRenderer> {
        let brdf_lut = integrate_brdf(&mut renderer, 512).chain_err(|| "Could not integrate BRDF")?;

        let globals_buffer =
            MutableBuffer::new(&[0u32, 0u32, 0u32, 0u32]).chain_err(|| "Could not create scene globals buffer")?;

        Ok(SceneRenderer {
            brdf_lut: brdf_lut,

            globals_buffer: globals_buffer,
        })
    }

    /// Creates new Scene Renderer with given BRDF model.
    ///
    /// This function needs to have access to Renderer because Scene Renderer creates its own LUT textures/buffers.
    // pub fn with_custom_brdf(mut renderer: &mut Renderer, brdf: &str) -> Result<SceneRenderer> {
    //     let vertex_shader = Shader::new(
    //         ShaderType::Vertex,
    //         &[
    //             include_str!("../assets/shaders/libs/version.glsl"),
    //             include_str!("../assets/shaders/libs/consts.glsl"),
    //             include_str!("../assets/shaders/libs/vertices.glsl"),
    //             include_str!("../assets/shaders/libs/parameters.glsl"),
    //             include_str!("../assets/shaders/main.vert"),
    //         ],
    //     ).chain_err(|| "Failed to compile vertex shader")?;
    //     let fragment_shader = Shader::new(
    //         ShaderType::Fragment,
    //         &[
    //             include_str!("../assets/shaders/libs/version.glsl"),
    //             include_str!("../assets/shaders/libs/consts.glsl"),
    //             include_str!("../assets/shaders/libs/vertices.glsl"),
    //             include_str!("../assets/shaders/libs/parameters.glsl"),
    //             brdf,
    //             include_str!("../assets/shaders/main.frag"),
    //         ],
    //     ).chain_err(|| "Failed to compile fragment shader")?;

    //     let pipeline = PipelineBuilder::new()
    //         .vertex_shader(&vertex_shader)
    //         .fragment_shader(&fragment_shader)
    //         .build()
    //         .chain_err(|| "Unable to build pipeline")?;

    //     let brdf_lut = integrate_brdf(&mut renderer, 512).chain_err(|| "Could not integrate BRDF")?;

    //     let globals_buffer =
    //         MutableBuffer::new(&[0u32, 0u32, 0u32, 0u32]).chain_err(|| "Could not create scene globals buffer")?;

    //     Ok(SceneRenderer {
    //         brdf_lut: brdf_lut,

    //         globals_buffer: globals_buffer,
    //     })
    // }

    /// Renders scene with IBL mode.
    pub fn render<T: Camera>(
        &mut self,
        renderer: &mut Renderer,
        shape_lists: &[&ShapeList],
        camera: &T,
        point_lights: Option<&LightGroup>,
        sun: Option<&Sun>,
    ) -> Result<()> {
        let pipeline = &*fetch_program("main");

        for shape_list in shape_lists {
            for (i, shape) in shape_list.shapes.iter().enumerate() {
                let indices = shape.indices;
                let vertices = shape.vertices;

                let shadowmaps;
                let shadowmaps_slice;

                let mut draw_command = DrawCommand::arrays(pipeline, 0, shape.indices.1);

                if let Some(sun) = sun {
                    self.globals_buffer
                        .set_sub_data(&[1u32], 0)
                        .chain_err(|| "Could not update scene globals buffer")?;

                    draw_command = draw_command
                        .uniform(sun.get_buffer(), 2)
                        .texture_2d(sun.get_shadowmap(), 16);
                } else {
                    self.globals_buffer
                        .set_sub_data(&[0u32], 0)
                        .chain_err(|| "Could not update scene globals buffer")?;
                }

                if let Some(point_lights) = point_lights {
                    self.globals_buffer
                        .set_sub_data(&[1u32], 4)
                        .chain_err(|| "Could not update scene globals buffer")?;

                    shadowmaps = point_lights.get_shadowmaps();
                    shadowmaps_slice = shadowmaps.as_slice();
                    draw_command = draw_command
                        .storage_read(point_lights.get_buffer(), 3)
                        .texture_cubemap_array(shadowmaps_slice, 8);
                } else {
                    self.globals_buffer
                        .set_sub_data(&[0u32], 4)
                        .chain_err(|| "Could not update scene globals buffer")?;
                }

                if shape_list.ibl_diffuse_texture.is_some() && shape_list.ibl_specular_texture.is_some() {
                    draw_command = draw_command
                        .texture_2d(Some(&self.brdf_lut), 5)
                        .texture_cubemap(shape_list.ibl_diffuse_texture.as_ref().unwrap(), 6)
                        .texture_cubemap(shape_list.ibl_specular_texture.as_ref().unwrap(), 7);
                }

                let draw_command = draw_command
                    .uniform(&self.globals_buffer, 0)
                    .uniform(camera.get_buffer(), 1)
                    .storage_range_read::<u32>(&shape_list.indices_buffer, 0, indices.0, indices.1)
                    .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 1, vertices.0, vertices.1)
                    .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 2, i, 1)
                    .texture_2d(shape_list.texture(shape.albedo), 0)
                    .texture_2d(shape_list.texture(shape.metallic_roughness), 1)
                    .texture_2d(shape_list.texture(shape.occlusion), 2)
                    .texture_2d(shape_list.texture(shape.normal), 3)
                    .texture_2d(shape_list.texture(shape.emissive), 4);

                renderer.draw(&draw_command).chain_err(|| "Could not render scene")?;
            }
        }

        Ok(())
    }
}
