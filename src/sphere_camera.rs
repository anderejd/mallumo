use super::errors::*;
use super::*;

use std::cell::Cell;

use cgmath;

/// Describes position with sphere coordinates.
#[derive(Copy, Clone, Debug)]
pub struct SpherePosition {
    pub radius: f32,
    pub y_angle: cgmath::Rad<f32>,
    pub height_angle: cgmath::Rad<f32>,
}

/// Simple camera which is controlled with mouse.
#[derive(Debug)]
pub struct SphereCamera {
    position: SpherePosition,
    look_at: cgmath::Point3<f32>,
    up: cgmath::Vector3<f32>,
    fov: cgmath::Rad<f32>,
    width: usize,
    height: usize,
    near: f32,
    far: f32,
    camera_buffer: MutableBuffer,

    change_p_matrix: Cell<bool>,
    change_v_matrix: Cell<bool>,
    change_position: Cell<bool>,
}

fn sphere_to_cartesian(sphere_position: SpherePosition) -> cgmath::Point3<f32> {
    use cgmath::prelude::*;
    use cgmath::Rad;

    let r = sphere_position.radius;
    let ya = sphere_position.y_angle;
    let h = sphere_position.height_angle;

    let x = r * Rad::sin(h) * Rad::sin(ya);
    let y = r * Rad::cos(h);
    let z = r * Rad::sin(h) * Rad::cos(ya);

    cgmath::Point3::new(x, y, z)
}

impl Camera for SphereCamera {
    fn update_buffer(&mut self) -> Result<()> {
        let change_p_matrix = self.change_p_matrix.get();
        let change_v_matrix = self.change_v_matrix.get();
        let change_pv_matrix = self.change_p_matrix.get() || self.change_v_matrix.get();
        let change_position = self.change_position.get();

        if change_p_matrix {
            let p_matrix = self.get_projection();
            self.camera_buffer
                .set_sub_data(p_matrix.as_ref() as &[f32; 16], 0)
                .chain_err(|| "Unable to update projection matrix")?;
            self.change_p_matrix.set(false);
        }

        if change_v_matrix {
            let v_matrix = self.get_view();
            self.camera_buffer
                .set_sub_data(v_matrix.as_ref() as &[f32; 16], 64)
                .chain_err(|| "Unable to update view matrix")?;
            self.change_v_matrix.set(false);
        }

        if change_pv_matrix {
            let pv_matrix = self.get_projection_view();
            self.camera_buffer
                .set_sub_data(pv_matrix.as_ref() as &[f32; 16], 128)
                .chain_err(|| "Unable to update projection view matrix")?;
            self.change_p_matrix.set(false);
            self.change_v_matrix.set(false);
        }

        if change_position {
            let position = self.get_position();
            self.camera_buffer
                .set_sub_data(position.as_ref() as &[f32; 3], 192)
                .chain_err(|| "Unable to update camera positionition")?;
            self.change_position.set(false);
        }

        Ok(())
    }

    fn get_buffer(&self) -> &Buffer {
        &self.camera_buffer
    }

    fn get_projection(&self) -> cgmath::Matrix4<f32> {
        let f: f32 = 1.0f32 / (self.fov.0 as f32 / 2.0f32).tan();
        cgmath::Matrix4::new(
            f / self.get_ratio(),
            0.0,
            0.0,
            0.0,
            0.0,
            f,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            -1.0,
            0.0,
            0.0,
            self.near,
            0.0,
        )
    }

    fn get_view(&self) -> cgmath::Matrix4<f32> {
        cgmath::Matrix4::look_at(self.get_position(), self.look_at, self.up)
    }

    fn get_projection_view(&self) -> cgmath::Matrix4<f32> {
        self.get_projection() * self.get_view()
    }

    fn get_position(&self) -> cgmath::Point3<f32> {
        sphere_to_cartesian(self.position)
    }

    fn get_ratio(&self) -> f32 {
        self.width as f32 / self.height as f32
    }
}

impl SphereCamera {
    // TODO: optimize setting data(not all camera transformation change all data)
    /// Creates new Sphere Camera.
    pub fn new(
        position: SpherePosition,
        look_at: cgmath::Point3<f32>,
        up: cgmath::Vector3<f32>,
        fov: cgmath::Rad<f32>,
        width: usize,
        height: usize,
        near: f32,
        far: f32,
    ) -> Result<SphereCamera> {
        use std::cmp::max;

        let clamped_width = max(1, width);
        let clamped_height = max(1, height);

        let cartesian_position = sphere_to_cartesian(position);
        let p_matrix = cgmath::perspective(fov, clamped_width as f32 / clamped_height as f32, near, far);
        let v_matrix = cgmath::Matrix4::look_at(cartesian_position, look_at, up);

        let mut camera_buffer =
            MutableBuffer::new_empty(std::mem::size_of::<f32>() * 52).chain_err(|| "Unable to create camera buffer")?;

        camera_buffer
            .set_sub_data(p_matrix.as_ref() as &[f32; 16], 0)
            .chain_err(|| "Unable to update projection matrix")?;
        camera_buffer
            .set_sub_data(v_matrix.as_ref() as &[f32; 16], 64)
            .chain_err(|| "Unable to update view matrix")?;
        camera_buffer
            .set_sub_data((p_matrix * v_matrix).as_ref() as &[f32; 16], 128)
            .chain_err(|| "Unable to update projection view matrix")?;
        camera_buffer
            .set_sub_data(cartesian_position.as_ref() as &[f32; 3], 192)
            .chain_err(|| "Unable to update camera positionition")?;

        Ok(SphereCamera {
            position: position,
            look_at: look_at,
            up: up,
            fov: fov,
            width: clamped_width,
            height: clamped_height,
            near: near,
            far: far,
            camera_buffer: camera_buffer,

            change_p_matrix: Cell::new(false),
            change_v_matrix: Cell::new(false),
            change_position: Cell::new(false),
        })
    }

    /// Moves radius.
    ///
    /// If desired this function may saturate so that radius is not negative.
    pub fn move_radius(&mut self, n: f32, saturate: bool) -> &mut Self {
        self.change_v_matrix.set(true);
        self.change_position.set(true);
        if saturate && self.position.radius + n < 0.00001 {
            self.position.radius = 0.00001;
        } else {
            self.position.radius += n;
        }
        self
    }

    /// Rotates camera on Y axis.
    pub fn rot_y(&mut self, angle: cgmath::Rad<f32>) -> &mut Self {
        use cgmath::Angle;
        self.change_v_matrix.set(true);
        self.change_position.set(true);
        self.position.y_angle = (self.position.y_angle + angle).normalize();
        self
    }

    /// Rotates height, in a way representing movement in Y direction.
    ///
    /// If desired this function may saturate, this is advised because the image flips if it rotates over poles.
    pub fn rot_height(&mut self, angle: cgmath::Rad<f32>, saturate: bool) -> &mut Self {
        use cgmath::{Deg, Rad};
        self.change_v_matrix.set(true);
        self.change_position.set(true);
        if saturate {
            if self.position.height_angle + angle < Rad(0.00001) {
                self.position.height_angle = Rad(0.00001);
            } else if self.position.height_angle + angle > Deg(179.99999).into() {
                self.position.height_angle = Deg(179.99999).into();
            } else {
                self.position.height_angle += angle;
            }
        } else {
            self.position.height_angle += angle;
        }

        self
    }

    /// Processes window events disregarding delta time.
    pub fn process_event(&mut self, event: &glutin::Event, input: &Input, _: f32) -> Result<()> {
        let window_event: glutin::WindowEvent;
        match *event {
            glutin::Event::WindowEvent { ref event, .. } => {
                window_event = event.clone();
            }
            _ => {
                return Ok(());
            }
        }

        match window_event {
            glutin::WindowEvent::Resized(width, height) => if width != 0 && height != 0 {
                self.set_viewport(width as usize, height as usize);
                self.update_buffer().chain_err(|| "Could not update camera buffer")?;
            },
            glutin::WindowEvent::MouseWheel { delta, .. } => match delta {
                glutin::MouseScrollDelta::LineDelta(_, rows) => {
                    self.move_radius(-rows / 10.0, true);
                    self.update_buffer().chain_err(|| "Could not update camera buffer")?;
                }
                glutin::MouseScrollDelta::PixelDelta(horizontal, vertical) => {
                    println!("horizontal: {:?} vertical: {:?}", horizontal, vertical);
                }
            },
            glutin::WindowEvent::CursorMoved { .. } => if input.mouse.left_button == ButtonState::Pressed {
                let (dx, dy) = (
                    input.mouse.position.0 - input.mouse.previous_position.0,
                    input.mouse.position.1 - input.mouse.previous_position.1,
                );
                let (dx, dy) = (dx as f32 / self.width as f32, dy as f32 / self.height as f32);
                self.rot_y(cgmath::Rad(-dx * 8.0));
                self.rot_height(cgmath::Rad(-dy * 8.0), true);
                self.update_buffer().chain_err(|| "Could not update camera buffer")?;
            },
            _ => (),
        }
        Ok(())
    }

    pub fn get_sphere_position(&self) -> SpherePosition {
        self.position
    }

    pub fn set_sphere_position(&mut self, position: SpherePosition) -> &mut Self {
        self.change_p_matrix.set(true);
        self.change_position.set(true);
        self.position = position;
        self
    }

    pub fn get_look_at(&self) -> cgmath::Point3<f32> {
        self.look_at
    }

    pub fn set_look_at(&mut self, look_at: cgmath::Point3<f32>) -> &mut Self {
        self.change_v_matrix.set(true);
        self.look_at = look_at;
        self
    }

    pub fn get_up(&self) -> cgmath::Vector3<f32> {
        self.up
    }

    pub fn set_up(&mut self, up: cgmath::Vector3<f32>) -> &mut Self {
        self.change_v_matrix.set(true);
        self.up = up;
        self
    }

    pub fn get_fov(&self) -> cgmath::Rad<f32> {
        self.fov
    }

    pub fn set_fov(&mut self, fov: cgmath::Rad<f32>) -> &mut Self {
        self.change_p_matrix.set(true);
        self.fov = fov;
        self
    }

    pub fn get_viewport(&self) -> (usize, usize) {
        (self.width, self.height)
    }

    pub fn set_viewport(&mut self, width: usize, height: usize) -> &mut Self {
        self.change_p_matrix.set(true);
        self.width = width;
        self.height = height;
        self
    }

    pub fn get_width(&self) -> usize {
        self.width
    }

    pub fn set_width(&mut self, width: usize) -> &mut Self {
        self.change_p_matrix.set(true);
        self.width = width;
        self
    }

    pub fn get_height(&self) -> usize {
        self.height
    }

    pub fn set_height(&mut self, height: usize) -> &mut Self {
        self.change_p_matrix.set(true);
        self.height = height;
        self
    }

    pub fn get_near(&self) -> f32 {
        self.near
    }

    pub fn set_near(&mut self, near: f32) -> &mut Self {
        self.change_p_matrix.set(true);
        self.near = near;
        self
    }

    pub fn get_far(&self) -> f32 {
        self.far
    }

    pub fn set_far(&mut self, far: f32) -> &mut Self {
        self.change_p_matrix.set(true);
        self.far = far;
        self
    }
}
