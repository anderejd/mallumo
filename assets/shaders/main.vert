#version 450 core

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/vertices.glsl
#include libs/parameters.glsl

layout(std140, binding = 0) uniform Globals {
    uint has_sun;
    uint has_point_lights;
} globals;

layout(std140, binding = 1) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout(std140, binding = 2) uniform Sun {
  mat4 space_matrix;
  vec4 color;
  vec4 position;
} sun;

out vertexOut {
  mat3 TBN;
  vec3 world_position; 
  vec2 texture_coordinate;
  vec3 normal;
  vec4 sun_space_position;
} vertex_out;

void main(void)
{
  int index = indices[gl_VertexID];

  vec3 position = vertices[index].position.xyz;
  vec3 normal = vertices[index].normal.xyz;
  vec2 texture_coordinate = vertices[index].texture_coordinate.xy;
  vec3 tangent = vertices[index].tangent.xyz;
  float sign = vertices[index].tangent.w;
  vec3 bitangent = cross(normal, tangent) * sign;

  mat4 model_matrix = primitive_parameters.model_matrix;
  mat3 normal_matrix = transpose(inverse(mat3(model_matrix)));

  vec3 T = normalize(normal_matrix * tangent);
  vec3 B = normalize(normal_matrix * bitangent);
  vec3 N = normalize(normal_matrix * normal);  
  mat3 TBN = mat3(T, B, N);

  vertex_out.TBN = TBN;
  vertex_out.world_position = vec3(model_matrix * vec4(position, 1.0));
  vertex_out.texture_coordinate = texture_coordinate;
  vertex_out.normal = normalize(normal_matrix * normal);

  if (globals.has_sun == 1) {
    vertex_out.sun_space_position = sun.space_matrix * vec4(vertex_out.world_position, 1.0);
  }

  gl_Position = camera.projection_view_matrix * vec4(vertex_out.world_position, 1.0f);
}