#version 450 core

layout(std140, binding = 0) uniform Globals
{
    mat4 shadow_projections[6];
    vec4 pos_far;
} globals;

layout(std430, binding = 2) buffer PrimitiveParameters {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness_refraction;  // 16B
  vec4 emission;                       // 16B

  // Material Textures
  uint has_albedo_texture;             // 4B
  uint has_metallic_roughness_texture; // 4B
  uint has_occlusion_texture;          // 4B
  uint has_normal_texture;             // 4B
  uint has_emissive_texture;           // 4B

  // Image based lighting
  uint has_diffuse_ibl;                // 4B
  uint has_specular_ibl;               // 4B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  // Padding
  uint padding[27];                   // 148B padded to 256
} primitive_parameters;

in geometryOut {
  vec3 frag_pos;
  flat uint casts_shadows;
} geometry_out;

void main()
{
    if (primitive_parameters.casts_shadows == 0) {
        discard;
    }

    float light_distance = length(geometry_out.frag_pos - globals.pos_far.xyz);
    
    light_distance = light_distance / globals.pos_far.w;
    
    gl_FragDepth = light_distance;
}  