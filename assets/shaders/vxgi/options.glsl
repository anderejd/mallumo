#define VoxelTextureType uint
const VoxelTextureType VoxelTextureTypeAlbedo = 0;
const VoxelTextureType VoxelTextureTypeNormal = 1;
const VoxelTextureType VoxelTextureTypeEmission = 2;
const VoxelTextureType VoxelTextureTypeMetallicRoughness = 3;
const VoxelTextureType VoxelTextureTypeRadiance = 4;

#define ShadowsMode uint
const ShadowsMode ShadowsModeNone = 0;
const ShadowsMode ShadowsModeShadowmap = 1;
const ShadowsMode ShadowsModeConeTraced = 2;

#define VoxelStructure uint
const VoxelStructure VoxelStructureVolumeTexture = 0;
const VoxelStructure VoxelStructureSparseVoxelOctree = 1;

#define ConeDistribution uint
const ConeDistribution ConeDistributionConcentricRegular = 0;
const ConeDistribution ConeDistributionConcentricIrregular = 1;
const ConeDistribution ConeDistributionRandom = 2;

layout(std140, binding = VXGI_OPTIONS_BINDING) uniform VXGIOptions {
  vec4 cones[32];
  // actually float[32] but written here as vec4[8] to have packed array in std140
  vec4 cones_weights[8]; 
  uint cones_num;
  ConeDistribution cones_distribution;

  uint levels;
  uint dimension;

  uint calculate_direct;
  uint calculate_indirect_diffuse;
  uint calculate_indirect_specular;
  uint calculate_ambient_occlusion;

  ShadowsMode shadows_mode;

  bool anisotropic;
  bool hdr;

  VoxelStructure structure;
} vxgi_options;
