#version 450 core

#define VXGI_OPTIONS_BINDING 0

#include vxgi/options.glsl

layout(binding = 0) buffer ConePath {
  vec4 cone_path[256];
  uint cone_count;
};

out vec3 position;
out float mip_level;

void main() {
    if(gl_VertexID < cone_count){
        position = cone_path[gl_VertexID].xyz;
        mip_level = floor(cone_path[gl_VertexID].w);
        gl_Position = vec4(cone_path[gl_VertexID].xyz, 1.0);
    } else {
        gl_Position = vec4(0.0);
    }
}