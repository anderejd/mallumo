#version 450 core

// CONSTS
// X-, X+, Y-, Y+, Z-, Z+
const vec4 cube_vertices[8] = vec4[8](
    vec4(-1.0, -1.0, -1.0, 1.0), // 0: back bottom left
    vec4(-1.0,  1.0, -1.0, 1.0), // 1: back top    left
    vec4( 1.0, -1.0, -1.0, 1.0), // 2: back bottom right
    vec4( 1.0,  1.0, -1.0, 1.0), // 3: back top    right

    vec4(-1.0, -1.0, 1.0, 1.0),  // 4: front bottom left
    vec4(-1.0,  1.0, 1.0, 1.0),  // 5: front top    left
    vec4( 1.0, -1.0, 1.0, 1.0),  // 6: front bottom right
    vec4( 1.0,  1.0, 1.0, 1.0)   // 7: front top    right
);

const int cube_indices[24]  = int[24](
    1,0,5,4, // left   X-
    7,6,3,2, // right  X+

    4,0,6,2, // bottom Y-
    3,1,7,5, // top    Y+

    0,1,2,3, // front  Z-
    7,5,6,4  // back   Z+
);

// GLOBALS
#define CAMERA_BINDING 0
#include libs/camera.glsl

layout (points) in;
layout (triangle_strip, max_vertices = 24) out;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

layout(std430, binding = 0) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

layout(rgba32f, binding = 0) uniform image3D brickpool[6];

// INPUTS
layout(location = 0) in float side_length[];
layout(location = 1) in ivec3 voxel_address[];

// OUTPUTS
layout(location = 0) out flat vec4 voxel_color;

void main()
{
    for (int index = 0; index < 24; index++)
    {
        voxel_color = imageLoad(brickpool[index / 4], voxel_address[0]).rgba;

        // Vertex Position
        vec3 vertex = cube_vertices[cube_indices[index]].xyz;
        vec3 world_position = vec3(gl_in[0].gl_Position) + vertex * (side_length[0] * (1.0/6.0));
        gl_Position = camera.projection_view_matrix * vec4(world_position, 1.0);
        EmitVertex();

        if ((index + 1) % 4 == 0) {
            EndPrimitive();
        }
    }
}
