#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

layout(std430, binding = 0) buffer NodePointsSizes
{
  vec4 nodepoint_positions[];
};

out float side_length;

void main() {
  int level = int(nodepoint_positions[gl_VertexID].w);

  if (level != 8) {
    side_length = 0.0;
    gl_Position = vec4(0.0, 0.0, 0.0, 0.0);
  } else {
    side_length = 2.0 / pow(2.0, level);
    gl_Position = vec4(nodepoint_positions[gl_VertexID].xyz * 2.0 - 1.0, 0.0);
  }
}