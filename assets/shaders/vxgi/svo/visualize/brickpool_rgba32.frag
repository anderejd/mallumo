#version 450

#define CAMERA_BINDING 0

#include libs/camera.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 0) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

in flat ivec3 brick_address_geometry;

layout(rgba32f, binding = 0) uniform coherent volatile image3D brickpool;

layout (location = 0) out vec4 color;

void main()
{
    vec4 value = imageLoad(brickpool, brick_address_geometry);

    if (value.a == 0.0) {
        discard;
    }

    color = value.rgba;
}