#version 450 core

#define CAMERA_BINDING 0

#include libs/camera.glsl
#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 0) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

const ivec3 brick_offsets[27] = {
  ivec3(-1, -1, -1),
  ivec3(-1, -1, 0),
  ivec3(-1, -1, 1),
  ivec3(-1, 0, -1),
  ivec3(-1, 0, 0),
  ivec3(-1, 0, 1),
  ivec3(-1, 1, -1),
  ivec3(-1, 1, 0),
  ivec3(-1, 1, 1),
  ivec3(0, -1, -1),
  ivec3(0, -1, 0),
  ivec3(0, -1, 1),
  ivec3(0, 0, -1),
  ivec3(0, 0, 0),
  ivec3(0, 0, 1),
  ivec3(0, 1, -1),
  ivec3(0, 1, 0),
  ivec3(0, 1, 1),
  ivec3(1, -1, -1),
  ivec3(1, -1, 0),
  ivec3(1, -1, 1),
  ivec3(1, 0, -1),
  ivec3(1, 0, 0),
  ivec3(1, 0, 1),
  ivec3(1, 1, -1),
  ivec3(1, 1, 0),
  ivec3(1, 1, 1)
};

layout(rgba32f, binding = 0) uniform coherent volatile image3D brickpool;

out float side_length;
out ivec3 brick_address_vertex;

void main() {
    int  node_address = gl_VertexID / 27;
    int  node_level = int(nodepoint_positions[node_address].w);
    vec3 node_position = nodepoint_positions[node_address].xyz;

    if (node_level != level) {
      side_length = 0.0;
      gl_Position = vec4(0.0, 0.0, 0.0, 0.0);
    } else {
      uint offset_index = gl_VertexID % 27;

      side_length = 2.0 / pow(2.0, level);

      const uint brickpool_size = imageSize(brickpool).x / 3;
      ivec3 brick_start = ivec3(
          3 * (node_address % brickpool_size),
          3 * ((node_address / brickpool_size) % brickpool_size),
          3 * ((node_address / brickpool_size / brickpool_size) % brickpool_size)
      );

      brick_address_vertex = brick_start + brick_offsets[offset_index] + ivec3(1, 1, 1);

      gl_Position = vec4(node_position * 2.0 - vec3(1.0, 1.0, 1.0) + brick_offsets[offset_index] * (side_length * (1.0/3.0)), 0.0);
    }
}