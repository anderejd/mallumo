#version 450

layout(location = 0) out vec4 fragColor;
layout(pixel_center_integer) in vec4 gl_FragCoord;

#define VXGI_OPTIONS_BINDING 0
#define INDICES_BINDING 1
#define VERTICES_BINDING 2
#define PRIMITIVE_PARAMETERS_BINDING 3

#include libs/vertices.glsl
#include libs/parameters.glsl
#include vxgi/options.glsl
#include vxgi/voxelize_shared.glsl

// Geometry inputs
layout(location = 0) in vec3 geometry_world_position;
layout(location = 1) in vec3 geometry_world_normal;
layout(location = 2) in vec2 geometry_texture_coordinate;
layout(location = 3) in flat vec4 geometry_AABB;
layout(location = 4) in flat int  geometry_swizzle;

// Material textures
layout(binding = 0) uniform sampler2D albedo_sampler;
layout(binding = 1) uniform sampler2D normal_sampler;
layout(binding = 2) uniform sampler2D metallic_roughness_sampler;
layout(binding = 3) uniform sampler2D occlusion_sampler;
layout(binding = 4) uniform sampler2D emissive_sampler;

// Voxel Fragment List
layout(std430, binding = 4) buffer VoxelPositions
{
    uint voxel_positions[];
};

layout(std430, binding = 5) buffer VoxelAlbedos
{
    uint voxel_albedos[];
};
layout(std430, binding = 6) buffer VoxelNormals
{
    vec4 voxel_normals[];
};

layout(std430, binding = 7) buffer VoxelEmissions
{
    vec4 voxel_emissions[];
};

// Atomic counter 
layout ( binding = 0, offset = 0 ) uniform atomic_uint voxels_count;

void main() {
  vec2 aabb_min = floor((geometry_AABB.xy * 0.5 + 0.5) * vxgi_options.dimension);
  vec2 aabb_max = ceil((geometry_AABB.zw * 0.5 + 0.5) * vxgi_options.dimension);

  if (!(all(greaterThanEqual(gl_FragCoord.xy, aabb_min)) && all(lessThanEqual(gl_FragCoord.xy, aabb_max)))) {
    discard;
  }

  mat3 swizzle_matrix = mat3(1.0);
  if (geometry_swizzle == 0) {
    swizzle_matrix = mat3(vec3(0.0, 0.0, 1.0), vec3(0.0, 1.0, 0.0), vec3(1.0, 0.0, 0.0));
  } else if (geometry_swizzle == 1) {
    swizzle_matrix = mat3(vec3(1.0, 0.0, 0.0), vec3(0.0, 0.0, 1.0), vec3(0.0, 1.0, 0.0));
  } else {
    swizzle_matrix = mat3(vec3(1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0));
  }
  mat3 swizzle_matrix_inverse = inverse(swizzle_matrix);

  // Voxel position
  ivec3 position = ivec3(swizzle_matrix_inverse * vec3(gl_FragCoord.xy, gl_FragCoord.z * vxgi_options.dimension));

  // Voxel albedo
  vec4 albedo = primitive_parameters.albedo.rgba;
  if (primitive_parameters.has_albedo_texture == 1) {
    albedo *= texture(albedo_sampler, geometry_texture_coordinate).rgba;
  }
  albedo.rgb = albedo.rgb * albedo.a;
  albedo.a = 1.0;

  // Voxel emission
  vec3 emission = primitive_parameters.emission.rgb;
  if (primitive_parameters.has_emissive_texture == 1) {
    emission *= texture(emissive_sampler, geometry_texture_coordinate).rgb;
  }

  // Voxel normal
  vec4 normal = vec4(encode_normal(normalize(geometry_world_normal)), 1.0);

  uint index = atomicCounterIncrement(voxels_count);
  uvec3 voxel_position_uint = uvec3(position);
  voxel_positions[index] = uvec3_to_uint(voxel_position_uint);
  voxel_albedos[index] = packUnorm4x8(albedo);
  voxel_normals[index] = normal;
  voxel_emissions[index] = vec4(emission, 1.0);
}