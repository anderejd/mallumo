#version 450

layout(location = 0) out vec4 fragColor;
layout(pixel_center_integer) in vec4 gl_FragCoord;

#define VXGI_OPTIONS_BINDING 0
#define INDICES_BINDING 1
#define VERTICES_BINDING 2
#define PRIMITIVE_PARAMETERS_BINDING 3

#include libs/vertices.glsl
#include libs/parameters.glsl
#include vxgi/options.glsl
#include vxgi/voxelize_shared.glsl

layout(location = 0) uniform uint is_static_geometry;

// Geometry inputs
layout(location = 0) in vec3 geometry_world_position;
layout(location = 1) in vec3 geometry_world_normal;
layout(location = 2) in vec2 geometry_texture_coordinate;
layout(location = 3) in flat vec4 geometry_AABB;
layout(location = 4) in flat int  geometry_swizzle;

// Atomic counter 
layout ( binding = 0, offset = 0 ) uniform atomic_uint voxels_count;

void main() {
  vec2 aabb_min = floor((geometry_AABB.xy * 0.5 + 0.5) * vxgi_options.dimension);
  vec2 aabb_max = ceil((geometry_AABB.zw * 0.5 + 0.5) * vxgi_options.dimension);

  if (!(all(greaterThanEqual(gl_FragCoord.xy, aabb_min)) && all(lessThanEqual(gl_FragCoord.xy, aabb_max)))) {
    discard;
  }

  atomicCounterIncrement(voxels_count);
}