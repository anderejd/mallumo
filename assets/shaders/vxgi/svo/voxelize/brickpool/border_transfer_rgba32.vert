#version 450 core

#include vxgi/svo/voxelize/shared.glsl

#define AXIS_X 0
#define AXIS_Y 1
#define AXIS_Z 2

layout(location = 0)       uniform uint level;
layout(location = 1)       uniform uint axis;

layout(rgba32f, binding = 0) uniform coherent volatile image3D brickpool;

// Nodepool
layout(std430, binding = 0) buffer NodePoolNext { uint nodepool_next[]; };
layout(std430, binding = 1) buffer NodePointsPositions { vec4 nodepoint_positions[]; };
layout(std430, binding = 2) buffer NodeNeighbours { uint neighbours[]; };

void main() {
  const uint node_address = gl_VertexID;
  const uint node_level = uint(nodepoint_positions[node_address].w);
  const uint neighbour_address = neighbours[node_address] & NODE_MASK_VALUE;

  if (node_level == level && neighbour_address != 0)  {
    const uint brickpool_size = imageSize(brickpool).x / 3;

    const ivec3 brick_address = ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * ((node_address / brickpool_size / brickpool_size) % brickpool_size)
    );

    const ivec3 neighbour_brick_address = ivec3(
      3 * (neighbour_address % brickpool_size),
      3 * ((neighbour_address / brickpool_size) % brickpool_size),
      3 * ((neighbour_address / brickpool_size / brickpool_size) % brickpool_size)
    );
    
    if (axis == AXIS_X) {
      for (int y = 0; y <= 2; ++y) {
        for (int z = 0; z <= 2; ++z) {
          ivec3 offset = ivec3(2,y,z);
          ivec3 nOffset = ivec3(0,y,z);

          vec4 borderVal = imageLoad(brickpool, brick_address + offset);
          vec4 neighbourBorderVal = imageLoad(brickpool, neighbour_brick_address + nOffset);

          vec4 finalVal = (borderVal + neighbourBorderVal) * 0.5;
          imageStore(brickpool, brick_address + offset, finalVal);
          imageStore(brickpool, neighbour_brick_address + nOffset, finalVal);
        }
      }
    }
    else if (axis == AXIS_Y) {
      for (int x = 0; x <= 2; ++x) {
        for (int z = 0; z <= 2; ++z) {
          ivec3 offset = ivec3(x,2,z);
          ivec3 nOffset = ivec3(x,0,z);
          
          vec4 borderVal = imageLoad(brickpool, brick_address + offset);
          vec4 neighbourBorderVal = imageLoad(brickpool, neighbour_brick_address + nOffset);

          vec4 finalVal = (borderVal + neighbourBorderVal) * 0.5;
          imageStore(brickpool, brick_address + offset, finalVal);
          imageStore(brickpool, neighbour_brick_address + nOffset, finalVal);
        }
      }
    }  
    else if (axis == AXIS_Z) {
      for (int x = 0; x <= 2; ++x) {
        for (int y = 0; y <= 2; ++y) {
          ivec3 offset = ivec3(x,y,2);
          ivec3 nOffset = ivec3(x,y,0);
          
          vec4 borderVal = imageLoad(brickpool, brick_address + offset);
          vec4 neighbourBorderVal = imageLoad(brickpool, neighbour_brick_address + nOffset);

          vec4 finalVal = (borderVal + neighbourBorderVal) * 0.5;
          imageStore(brickpool, brick_address + offset, finalVal);
          imageStore(brickpool, neighbour_brick_address + nOffset, finalVal);
        }
      }
    }
  }
}


