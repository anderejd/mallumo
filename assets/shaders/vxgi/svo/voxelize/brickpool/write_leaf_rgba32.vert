#version 450 core

#define VXGI_OPTIONS_BINDING 0

#include vxgi/options.glsl
#include vxgi/svo/voxelize/shared.glsl

// Voxel Data
layout(std430, binding = 0) buffer VoxelPositions
{
    uint voxel_positions[];
};

layout(std430, binding = 1) buffer VoxelAlbedos
{
    uint voxel_albedos[];
};

// Node pool data
layout(std430, binding = 2) buffer NodePoolNext
{
    uint nodepool_next[];
};

// Brick pool data
layout(rgba32f, binding = 0) uniform volatile coherent image3D brickpool_albedos;
layout(r32ui, binding = 1) uniform coherent volatile uimage3D lock_image;
layout(r32ui, binding = 2) uniform coherent volatile uimage3D count_image;

int traverse_octree(uint levels, inout vec3 voxel_position, out uint found_on_level);

void main() {
  const uint voxel_position_encoded = voxel_positions[gl_VertexID];
  const uint voxel_albedo_encoded = voxel_albedos[gl_VertexID];

  const uvec3 voxel_position_uvec3 = uint_to_uvec3(voxel_position_encoded);
         vec3 voxel_position_vec3  = vec3(voxel_position_uvec3) / vec3(vxgi_options.dimension);

  // Find node containing the voxel where this fragment belongs
  uint on_level = 0;
  const int node_address = traverse_octree(vxgi_options.levels, voxel_position_vec3, on_level);

  // Find exact coordinate of the voxel in brickpool
  const uint brickpool_size = imageSize(brickpool_albedos).x / 3;
  const ivec3 brick_coordinates = ivec3(
      3 * (node_address % brickpool_size),
      3 * ((node_address / brickpool_size) % brickpool_size),
      3 * (((node_address / brickpool_size) / brickpool_size) % brickpool_size)
  );

  const uvec3 offset_vector = uvec3(2.0 * voxel_position_vec3);
  const uint  offset = offset_vector.x + 2U * offset_vector.y + 4U * offset_vector.z;

  const ivec3 voxel_coordinates = brick_coordinates + 2 * ivec3(child_offsets[offset]);
 
  // Average new values
  const vec3 albedo = vec3(unpackUnorm4x8(voxel_albedo_encoded));

  image_average_rgba32(lock_image, count_image, brickpool_albedos, voxel_coordinates, vec4(albedo, 1.0));
}

int traverse_octree(uint levels, inout vec3 voxel_position, out uint found_on_level) 
{
  vec3 node_position = vec3(0.0);
  vec3 node_position_max = vec3(1.0);

  int node_address = 0;
  found_on_level = 0;
  float side_length = 1.0;
  
  for (uint level = 0; level < levels; ++level) {
    uint node = nodepool_next[node_address];

    // if current node points to *null* (0) nodepool
    uint child_start_address = node & NODE_MASK_VALUE;
    if (child_start_address == 0U) {
        found_on_level = level;
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * voxel_position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

    node_address = int(child_start_address + offset);
    node_position += vec3(child_offsets[offset]) * vec3(side_length);
    node_position_max = node_position + vec3(side_length);

    side_length = side_length / 2.0;
    voxel_position = 2.0 * voxel_position - vec3(offset_vec);
  } 

  return node_address;
}