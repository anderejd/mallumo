#version 450 core

#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 0) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

// Brick pool data
layout(rgba8, binding = 0) uniform coherent volatile image3D brickpool;

void interpolate_values(layout(rgba8) coherent volatile image3D brickpool, in ivec3 brick_address);

void main() {
  const uint brickpool_size = imageSize(brickpool).x / 3;
  ivec3 brick_address = ivec3(
      3 * (gl_VertexID % brickpool_size),
      3 * ((gl_VertexID / brickpool_size) % brickpool_size),
      3 * (((gl_VertexID / brickpool_size) / brickpool_size) % brickpool_size)
  );

  if (nodepoint_positions[gl_VertexID].w == level) {
    interpolate_values(brickpool, brick_address);
  }
}

vec4 interpolate_4(vec4 value1, vec4 value2, vec4 value3, vec4 value4) {
    vec4 values[4] = {
        value1, value2, value3, value4
    };
    vec4 col = vec4(0);
    int count = 0;
    for(int i = 0; i < 4; ++i) {
        if(values[i].w != 0.0) {
            col += values[i];
            count += 1;
        }
    }
    col /= float(count);

    return col;
}

vec4 interpolate_2(vec4 value1, vec4 value2) {
    vec4 values[2] = {
        value1, value2
    };
    vec4 col = vec4(0);
    int count = 0;
    for(int i = 0; i < 2; ++i) {
        if(values[i].w != 0.0) {
            col += values[i];
            count += 1;
        }
    }
    col /= float(count);

    return col;
}

void interpolate_values(layout(rgba8) coherent volatile image3D brickpool, in ivec3 brick_address) {
  vec4 values[8];
  for(int i = 0; i < 8; ++i) {
      values[i] = imageLoad(brickpool, brick_address + 2 * ivec3(child_offsets[i]));
  }

  memoryBarrier();

  vec4 col = vec4(0);

  // Center
  for (int i = 0; i < 8; ++i) {
    col += 0.125 * values[i];
  }

  imageStore(brickpool, brick_address + ivec3(1,1,1), col);

  // Face X
  col = vec4(0);
  col += 0.25 * values[1];
  col += 0.25 * values[3];
  col += 0.25 * values[5];
  col += 0.25 * values[7];
  imageStore(brickpool, brick_address + ivec3(2,1,1), col);

  // Face X Neg
  col = vec4(0);
  col += 0.25 * values[0];
  col += 0.25 * values[2];
  col += 0.25 * values[4];
  col += 0.25 * values[6];
  imageStore(brickpool, brick_address + ivec3(0,1,1), col);


  // Face Y
  col = vec4(0);
  col += 0.25 * values[2];
  col += 0.25 * values[3];
  col += 0.25 * values[6];
  col += 0.25 * values[7];
  imageStore(brickpool, brick_address + ivec3(1,2,1), col);

  // Face Y Neg
  col = vec4(0);
  col += 0.25 * values[0];
  col += 0.25 * values[1];
  col += 0.25 * values[4];
  col += 0.25 * values[5];
  imageStore(brickpool, brick_address + ivec3(1,0,1), col);


  // Face Z
  col = vec4(0);
  col += 0.25 * values[4];
  col += 0.25 * values[5];
  col += 0.25 * values[6];
  col += 0.25 * values[7];
  imageStore(brickpool, brick_address + ivec3(1,1,2), col);

  // Face Z Neg
  col = vec4(0);
  col += 0.25 * values[0];
  col += 0.25 * values[1];
  col += 0.25 * values[2];
  col += 0.25 * values[3];
  imageStore(brickpool, brick_address + ivec3(1,1,0), col);


  // Edges
  col = vec4(0);
  col += 0.5 * values[0];
  col += 0.5 * values[1];
  imageStore(brickpool, brick_address + ivec3(1,0,0), col);

  col = vec4(0);
  col += 0.5 * values[0];
  col += 0.5 * values[2];
  imageStore(brickpool, brick_address + ivec3(0,1,0), col);

  col = vec4(0);
  col += 0.5 * values[2];
  col += 0.5 * values[3];
  imageStore(brickpool, brick_address + ivec3(1,2,0), col);

  col = vec4(0);
  col += 0.5 * values[3];
  col += 0.5 * values[1];
  imageStore(brickpool, brick_address + ivec3(2,1,0), col);

  col = vec4(0);
  col += 0.5 * values[0];
  col += 0.5 * values[4];
  imageStore(brickpool, brick_address + ivec3(0,0,1), col);

  col = vec4(0);
  col += 0.5 * values[2];
  col += 0.5 * values[6];
  imageStore(brickpool, brick_address + ivec3(0,2,1), col);

  col = vec4(0);
  col += 0.5 * values[3];
  col += 0.5 * values[7];
  imageStore(brickpool, brick_address + ivec3(2,2,1), col);

  col = vec4(0);
  col += 0.5 * values[1];
  col += 0.5 * values[5];
  imageStore(brickpool, brick_address + ivec3(2,0,1), col);

  col = vec4(0);
  col += 0.5 * values[4];
  col += 0.5 * values[6];
  imageStore(brickpool, brick_address + ivec3(0,1,2), col);

  col = vec4(0);
  col += 0.5 * values[6];
  col += 0.5 * values[7];
  imageStore(brickpool, brick_address + ivec3(1,2,2), col);

  col = vec4(0);
  col += 0.5 * values[5];
  col += 0.5 * values[7];
  imageStore(brickpool, brick_address + ivec3(2,1,2), col);

  col = vec4(0);
  col += 0.5 * values[4];
  col += 0.5 * values[5];
  imageStore(brickpool, brick_address + ivec3(1,0,2), col);
}
