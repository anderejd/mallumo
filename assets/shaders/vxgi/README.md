#Voxel Cone Tracing Global Illumination

This is main folder for all shaders related to voxel cone tracing methods.

Methods for storing voxels are:
- 3D texture
- Sparse Voxel Octree

Furthermore they can be either:
- Isotropic
- Anisotropic