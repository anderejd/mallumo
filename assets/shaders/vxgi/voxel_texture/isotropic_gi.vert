#version 450 core

out vertexOut {
  vec2 texture_coordinate;
} vertex_out;

void main(void)
{
  vertex_out.texture_coordinate.x = (gl_VertexID == 2) ?  2.0 :  0.0;
  vertex_out.texture_coordinate.y = (gl_VertexID == 1) ?  -1.0 :  1.0;

  gl_Position.x = (gl_VertexID == 2) ?  3.0 : -1.0;
  gl_Position.y = (gl_VertexID == 1) ? -3.0 :  1.0;
  gl_Position.zw = vec2(0.5,1.0);
}