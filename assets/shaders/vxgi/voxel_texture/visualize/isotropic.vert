#version 450 core

#define VXGI_OPTIONS_BINDING 0

#include vxgi/options.glsl

layout(location = 0) uniform uint dimension;
layout(location = 1) uniform uint type;

layout(location = 0) out ivec3 vertex_voxel_position;

void main() {
    float voxel_size = 1.0 / float(dimension);
    float voxel_half_size = voxel_size / 2.0;

    ivec3 voxel_position = ivec3(
        gl_VertexID % dimension,
        (gl_VertexID / dimension) % dimension,
        (gl_VertexID / dimension / dimension) % dimension
     );

    vec3 position = vec3(voxel_position) * voxel_size + vec3(voxel_half_size);

    vertex_voxel_position = voxel_position;
    gl_Position = vec4(position, 1.0);
}