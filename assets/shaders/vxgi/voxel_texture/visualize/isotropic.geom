#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

layout (points) in;
layout (triangle_strip, max_vertices=14) out;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];


layout(location = 0) in  flat ivec3 vertex_voxel_position[];

layout(location = 0) out flat  vec4 geometry_voxel_color;

layout(location = 0) uniform uint dimension;
layout(location = 1) uniform uint type;
layout(binding = 0) uniform sampler3D texture;

const vec3 triangle_offsets[14] = {
  vec3(-1.0, 1.0, -1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(1.0, -1.0, -1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(1.0, 1.0, 1.0),
  vec3(-1.0, 1.0, -1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(-1.0, -1.0, 1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(1.0, 1.0, 1.0),
};

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz * 2.0 - 1.0;
    float edge_size = 1.0 / float(dimension);

    for(int i = 0; i < triangle_offsets.length(); i++) {
      geometry_voxel_color = texelFetch(texture, vertex_voxel_position[0], 0);
      gl_Position = camera.projection_view_matrix * vec4(position + triangle_offsets[i] * edge_size, 1.0);
      EmitVertex();
    }

    EndPrimitive();
}  