in vertexOut {
  vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout(binding = 0) uniform sampler2D clouds_texture;

// atmosphere
layout(location = 0) uniform vec3 sun_direction;
layout(binding = 1) uniform sampler2D transmittance_texture;
layout(binding = 2) uniform sampler3D scattering_texture;
layout(binding = 3) uniform sampler2D irradiance_texture;

layout (location = 0) out vec4 color;

const ivec2 offsets[9] = ivec2[] (
    ivec2(-1, 1),
    ivec2(0, 1),
    ivec2(1, 1),

    ivec2(-1, 0),
    ivec2(0, 0),
    ivec2(1, 0),

    ivec2(-1, -1),
    ivec2(0, -1),
    ivec2(1, -1)
);

const float weights[9] = float[] (
    0.0625, 0.125, 0.0625,
    0.125, 0.25, 0.125,
    0.0625, 0.125, 0.0625
);

// screen_position:
// 
// 0.0, 1.0       1.0, 1.0
//
//
// 0.0, 0.0       1.0, 0.0
vec3 ray_direction(vec2 screen_position)
{
    vec2 ndc = screen_position * 2.0 - 1.0;

    vec4 ray_clip = vec4(ndc, -1.0, 1.0);
    
    vec4 ray_eye = inverse(camera.projection_matrix) * ray_clip;
    ray_eye = vec4(ray_eye.xy, -1.0, 0.0);

    return normalize((inverse(camera.view_matrix) * ray_eye).xyz);
}

void main()
{
    vec3 ray_direction = ray_direction(vertex_out.texture_coordinate);

    vec2 size = textureSize(clouds_texture, 0);

    ivec2 pixel_coordinate = ivec2(floor(vertex_out.texture_coordinate * size));

    bool intersects_ground = false;
    vec4 clouds_sum = vec4(0.0);

    vec3 earth_camera = to_earth_space(camera.position.xyz, true);
    float r = length(earth_camera);
    float rmu = dot(earth_camera, ray_direction);
    float distance_to_top_atmospheric_boundary = -rmu -
        sqrt(rmu * rmu - r * r + globals.parameters.top_radius * globals.parameters.top_radius);
    if (distance_to_top_atmospheric_boundary > 0.0) {
        earth_camera = earth_camera + ray_direction * distance_to_top_atmospheric_boundary;
        r = globals.parameters.top_radius;
        rmu += distance_to_top_atmospheric_boundary;
    }
    float mu = rmu / r;
    intersects_ground = ray_intersects_ground(globals.parameters, r, mu);

    if (!intersects_ground) {
        for (int i = 0; i < 9; i++) {
            clouds_sum += texelFetch(clouds_texture, pixel_coordinate + offsets[i], 0) * weights[i];
        }
    }

    vec3 transmittance;
    vec3 luminance = get_sky_luminance(
      camera.position.xyz, ray_direction, sun_direction, transmittance, transmittance_texture, scattering_texture);

    float sun = get_sun(ray_direction, sun_direction);
    vec3 sun_luminance = sun * globals.parameters.solar_irradiance * globals.parameters.sun_spectral_radiance_to_luminance;
    luminance += transmittance * sun_luminance;

    vec4 atmosphere = (1.0 - clouds_sum.a) * vec4(adjust_exposure(luminance), 1.0);

    color = 
       vec4(pow(filmic_postprocess(clouds_sum.rgb + atmosphere.rgb), vec3(1.0/2.2)), 1.0);
}
