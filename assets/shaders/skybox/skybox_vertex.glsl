#version 450 core

layout(std140, binding = 0) uniform projectionViewMatrices 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 camera_position;
};

layout(std430, binding = 0) buffer Indices
{
    uint indices[];
};

layout(std430, binding = 1) buffer VertexAttributes
{
    vec4 vertices[];
};

out vertexOut {
    vec3 position;
} vertex_out;

void main(void)
{
  uint index = indices[gl_VertexID];
  vec3 position = vertices[index].xyz;

  vertex_out.position = position;

  mat4 rotView = mat4(mat3(view_matrix)); // remove translation from the view matrix
  vec4 clipPos = projection_matrix * rotView * vec4(position, 1.0);

  gl_Position = clipPos.xyww;
}