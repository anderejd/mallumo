#version 450 core

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/consts.glsl
#include libs/vertices.glsl
#include libs/parameters.glsl
#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl


layout(std140, binding = 0) uniform Globals {
    uint has_sun;
    uint has_point_lights;
} globals;

layout(std140, binding = 1) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

struct PointLight {
    vec4 pos_far;
    vec3 color;
    uint has_shadowmap;
};

layout(std430, binding = 3) buffer PointLights
{
    PointLight point_lights[];
};

layout(std140, binding = 2) uniform Sun {
  mat4 space_matrix;
  vec4 color;
  vec4 position;
} sun;

in vertexOut {
  mat3 TBN;
  vec3 world_position;  
  vec2 texture_coordinate;
  vec3 normal;
  vec4 sun_space_position;
} vertex_out;

layout(binding = 0) uniform sampler2D albedo_sampler;
layout(binding = 1) uniform sampler2D metallic_roughness_sampler;
layout(binding = 2) uniform sampler2D occlusion_sampler;
layout(binding = 3) uniform sampler2D normal_sampler;
layout(binding = 4) uniform sampler2D emissive_sampler;

layout(binding = 5) uniform sampler2D ibl_lut_sampler;
layout(binding = 6) uniform samplerCubeArray ibl_diffuse_sampler;
layout(binding = 7) uniform samplerCubeArray ibl_specular_sampler;

layout(binding = 8) uniform samplerCubeArray shadowmaps[8];
layout(binding = 16) uniform sampler2D sun_shadowmap;

layout(location = 0) out vec4 color;

vec3 grid_sampling_disk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1), 
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

float in_shadow(vec3 frag_pos, uint light_index)
{
    vec3 light_position = point_lights[light_index].pos_far.xyz;    
    float light_position_far = point_lights[light_index].pos_far.w;
    vec3 light_color = point_lights[light_index].color;

    // Get vector between fragment position and light position
    vec3 frag_to_light = frag_pos - light_position;
    // Get current linear depth as the length between the fragment and light position
    float current_depth = length(frag_to_light);
    // Test for shadows with PCF
    float shadow = 0.0;
    int samples = 20;
    float view_distance = length(camera.position.xyz - frag_pos);
    float bias = 0.005;
    float disk_radius = (1.0 + (view_distance / light_position_far)) / 700.0;
    for(int i = 0; i < samples; ++i)
    {
        float closest_depth = texture(shadowmaps[light_index], vec4(frag_to_light + grid_sampling_disk[i] * disk_radius, 0.0)).r;
        closest_depth *= light_position_far;   // Undo mapping [0;1]
        if(abs(current_depth - bias) > closest_depth)
            shadow += 1.0;
    }
    shadow /= float(samples);
    
    return shadow;
}

float in_sun_shadow(vec4 sun_space_position, vec3 normal)
{
    // perform perspective divide
    vec3 projection_coords = sun_space_position.xyz / sun_space_position.w;

    // transform to [0,1] range
    projection_coords.xy = projection_coords.xy * 0.5 + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closest_depth = texture(sun_shadowmap, projection_coords.xy).r; 

    // get depth of current fragment from light's perspective
    float current_depth = projection_coords.z;

    float shadow = 0.0;
    vec2 texel_size = 1.0 / textureSize(sun_shadowmap, 0);
    for(int x = -6; x <= 6; ++x)
    {
        for(int y = -6; y <= 6; ++y)
        {
            float depth = texture(sun_shadowmap, projection_coords.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth < depth ? 1.0 : 0.0;
        }    
    }
    shadow /= 13.0 * 13.0;

    return shadow;
}

void main()
{
    vec4 albedo = primitive_parameters.albedo.rgba;
    if (primitive_parameters.has_albedo_texture == 1) {
        albedo *= texture(albedo_sampler, vertex_out.texture_coordinate).rgba;
    }

    if (albedo.a <= 0.001) {
        discard;
    }

    float metalness = primitive_parameters.metallic_roughness_refraction.r;
    float roughness = primitive_parameters.metallic_roughness_refraction.g;
    if (primitive_parameters.has_metallic_roughness_texture == 1) {
        metalness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).r;
        roughness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).g;
    }

    float occlusion = 1.0;
    if (primitive_parameters.has_occlusion_texture == 1) {
        occlusion = texture(occlusion_sampler, vertex_out.texture_coordinate).r;
    }

    vec3 normal = vertex_out.normal;
    if (primitive_parameters.has_normal_texture == 1) {
        normal = texture(normal_sampler, vertex_out.texture_coordinate).rgb;
        normal = normalize(normal * 2.0 - 1.0);
        normal = vertex_out.TBN * normal; 
    }
    normal = normalize(normal);

    vec3 emission = primitive_parameters.emission.rgb;
    if(primitive_parameters.has_emissive_texture == 1) {
        emission *= texture(emissive_sampler, vertex_out.texture_coordinate).rgb;
    }
    
    vec3 Lo = vec3(0.0);

    vec3 N = normal;
    vec3 V = normalize(vec3(camera.position) - vertex_out.world_position);
    vec3 R = reflect(-V, N); 

    // Approximation of F0 for Fresnel
    vec3 f0 = mix(vec3(0.04, 0.04, 0.04), albedo.rgb, metalness);

    if (globals.has_sun == 1) {
        float sun_shadow = in_sun_shadow(vertex_out.sun_space_position, N);

        vec3 L = normalize(sun.position.xyz); 
        vec3 H = normalize(V + L);

        Lo += (1.0 - sun_shadow) * irradiance(N, H, V, L, sun.color.rgb, albedo, roughness, metalness);
    }

    if (globals.has_point_lights == 1) {
        for(uint i = 0; i < point_lights.length(); i++) {
            vec3 light_position = point_lights[i].pos_far.xyz;
            vec3 light_color = point_lights[i].color;

            vec3 L = normalize(light_position - vertex_out.world_position); 
            vec3 H = normalize(V + L);

            // Radiance
            float distance    = length(light_position - vertex_out.world_position);
            float attenuation = 1.0 / (distance * distance); // inverse-square law
            vec3 radiance     = light_color * attenuation;

            // Final Cook-Torrance BRDF
            //     DFG
            // -----------
            // 4(ωo⋅n)(ωi⋅n)            
            float D = cook_torrance_distribution(N, H, roughness);
            vec3  F = cook_torrance_fresnel(max(dot(H, V), 0.0), f0);
            float G = cook_torrance_geometry(N, V, L, roughness);

            vec3  nominator   = D * G * F;
            float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; 
            vec3  brdf        = nominator / denominator; 

            // kS = energy that gets reflected
            // kD = energy that gets refracted ( 1.0 - reflected )
            vec3 kS = F;
            vec3 kD = vec3(1.0) - kS; 

            // nullify kD if the surface is metallic
            // metallic surface does not refract light
            kD *= 1.0 - metalness;

            // light's outgoing reflectance
            // from reflectance equation
            // (                           )
            // ( kD * diffuse + kS * BRDF  ) * radiance * N ⋅ L
            // (                           )
            float NdotL = max(dot(N, L), 0.0);

            vec3 diffuse = diffuse_disney(albedo.rgb, roughness, dot(L, H), dot(N, V), dot(N, L));

            float shadow = point_lights[i].has_shadowmap == 0 || primitive_parameters.receives_shadows == 0
                        ? 0.0
                        : in_shadow(vertex_out.world_position, i);
            Lo += (1.0 - shadow) * ((kD * diffuse / PI + brdf) * radiance * NdotL);
        }
    }

    // Image Based Lighting
    vec3 ambient = vec3(0.0, 0.0, 0.0);
    if (primitive_parameters.has_diffuse_ibl == 1) {
        const float MAX_REFLECTION_LOD = 9.0;

        vec2 brdf  = texture(ibl_lut_sampler, vec2(max(dot(N, V), 0.0), roughness)).rg;

        vec3 kS = fresnelSchlickRoughness(max(dot(N, V), 0.0), f0, roughness);
        vec3 kD = 1.0 - kS;  
        
        vec3 irradiance = texture(ibl_diffuse_sampler, vec4(N, 0)).rgb;
        vec3 diffuse = irradiance * albedo.rgb;
        kD *= 1.0 - metalness;
        
        // sample both the pre-filter map and the BRDF lut and combine them together as per the Split-Sum approximation to get the IBL specular part.
        vec3 prefiltered_color = textureLod(ibl_specular_sampler, vec4(R, 0), roughness * MAX_REFLECTION_LOD).rgb;    

        vec3 specular = prefiltered_color * (kS * brdf.x + brdf.y);

        ambient = (kD * diffuse + specular) * occlusion;
    } else {
        Lo = Lo * occlusion;
    }

    Lo = Lo + ambient;
    Lo = Lo + emission;

    // HDR -> LDR using Reinhard operator
    // float min_white = 1.5;
    // Lo = (Lo * (vec3(1.0) + Lo / vec3(min_white * min_white))) / (vec3(1.0) + Lo);
    
    // Gamma correction
    Lo = pow(Lo, vec3(1.0/2.2));

    color = vec4(Lo, 1.0);
}