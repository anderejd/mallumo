layout(std140, binding = 0) uniform Globals 
{
  AtmosphereParameters parameters;
} globals;

float unit_range_to_tex_coord(float x, int texture_size) {
    return x * (1.0 - (1.0 / texture_size)) + (0.5 / texture_size);
}

float tex_coord_to_unit_range(float u, int texture_size) {
    return (u - (0.5 / texture_size)) / (1.0 - (1.0 / texture_size));
}

bool ray_intersects_ground(const AtmosphereParameters atmosphere,
    float r, float mu) {
    return r * r * (mu * mu - 1.0) + atmosphere.bottom_radius * atmosphere.bottom_radius >= 0.0 && mu < 0.0;
}

float get_density(const DensityProfile profile, float altitude) {
    if (altitude < profile.layers[0].width) {
        float density = profile.layers[0].exp_term * exp(profile.layers[0].exp_scale * altitude) +
        profile.layers[0].linear_term * altitude + profile.layers[0].constant_term;
        return clamp(density, 0.0, 1.0);
    }

    float density = profile.layers[1].exp_term * exp(profile.layers[1].exp_scale * altitude) +
    profile.layers[1].linear_term * altitude + profile.layers[1].constant_term;
    return clamp(density, 0.0, 1.0);
}

vec3 get_transmittance_to_top_atmospheric_boundary(
    const AtmosphereParameters atmosphere,
    sampler2D transmittance_texture,
    float r, float mu) {
    float H = sqrt(atmosphere.top_radius * atmosphere.top_radius - atmosphere.bottom_radius * atmosphere.bottom_radius);
    float rho = sqrt(max(r * r - atmosphere.bottom_radius * atmosphere.bottom_radius, 0.0));
    float d = max(-r * mu + sqrt(max(r * r * (mu * mu - 1.0) + atmosphere.top_radius * atmosphere.top_radius, 0.0)), 0.0);
    float d_min = atmosphere.top_radius - r;
    float d_max = rho + H;
    float x_mu = (d - d_min) / (d_max - d_min);
    float x_r = rho / H;

    vec2 uv = vec2(
        unit_range_to_tex_coord(x_mu, TRANSMITTANCE_TEXTURE_WIDTH),
        unit_range_to_tex_coord(x_r, TRANSMITTANCE_TEXTURE_HEIGHT));

    return vec3(texture(transmittance_texture, uv));
}

vec3 get_transmittance(
    const AtmosphereParameters atmosphere,
    sampler2D transmittance_texture,
    float r, float mu, float d, bool ray_r_mu_intersects_ground) {
    float r_d = clamp(sqrt(d * d + 2.0 * r * mu * d + r * r), atmosphere.bottom_radius, atmosphere.top_radius);
    float mu_d = clamp((r * mu + d) / r_d, -1.0, 1.0);

    if (ray_r_mu_intersects_ground) {
        return min(
            get_transmittance_to_top_atmospheric_boundary(
                atmosphere, transmittance_texture, r_d, -mu_d) /
            get_transmittance_to_top_atmospheric_boundary(
                atmosphere, transmittance_texture, r, -mu),
            vec3(1.0));
    } else {
        return min(
            get_transmittance_to_top_atmospheric_boundary(
                atmosphere, transmittance_texture, r, mu) /
            get_transmittance_to_top_atmospheric_boundary(
                atmosphere, transmittance_texture, r_d, mu_d),
            vec3(1.0));
    }
}

float rayleigh_phase_function(float nu) {
    float k = 3.0 / (16.0 * PI);
    return k * (1.0 + nu * nu);
}

float cornette_shanks_phase_function(float g, float nu) {
    float k = 3.0 / (8.0 * PI) * (1.0 - g * g) / (2.0 + g * g);
    return k * (1.0 + nu * nu) / pow(1.0 + g * g - 2.0 * g * nu, 1.5);
}

vec3 get_scattering(
    const AtmosphereParameters atmosphere,
    sampler3D scattering_texture,
    float r, float mu, float mu_s, float nu,
    bool ray_r_mu_intersects_ground) {
    float H = sqrt(atmosphere.top_radius * atmosphere.top_radius - atmosphere.bottom_radius * atmosphere.bottom_radius);
    float rho = sqrt(max(r * r - atmosphere.bottom_radius * atmosphere.bottom_radius, 0.0));
    float u_r = unit_range_to_tex_coord(rho / H, SCATTERING_TEXTURE_R_SIZE);
    float r_mu = r * mu;
    float discriminant = max(r_mu * r_mu - r * r + atmosphere.bottom_radius * atmosphere.bottom_radius, 0.0);
    float u_mu;

    if (ray_r_mu_intersects_ground) {
        float d = -r_mu - sqrt(discriminant);
        float d_min = r - atmosphere.bottom_radius;
        float d_max = rho;
        u_mu = 0.5 - 0.5 * unit_range_to_tex_coord(
            d_max == d_min ?
            0.0 :
            (d - d_min) / (d_max - d_min),
            SCATTERING_TEXTURE_MU_SIZE / 2);
    } else {
        float d = -r_mu + sqrt(discriminant + H * H);
        float d_min = atmosphere.top_radius - r;
        float d_max = rho + H;
        u_mu = 0.5 + 0.5 * unit_range_to_tex_coord((d - d_min) / (d_max - d_min), SCATTERING_TEXTURE_MU_SIZE / 2);
    }

    float d = max(
        -atmosphere.bottom_radius * mu_s + 
        sqrt(
            max(
                atmosphere.bottom_radius * atmosphere.bottom_radius * (mu_s * mu_s - 1.0) + atmosphere.top_radius * atmosphere.top_radius, 
                0.0
            )
        ),
        0.0);
        
    float d_min = atmosphere.top_radius - atmosphere.bottom_radius;
    float d_max = H;
    float a = (d - d_min) / (d_max - d_min);
    float A = -2.0 * atmosphere.mu_s_min * atmosphere.bottom_radius / (d_max - d_min);
    float u_mu_s = unit_range_to_tex_coord(max(1.0 - a / A, 0.0) / (1.0 + a), SCATTERING_TEXTURE_MU_S_SIZE);
    float u_nu = (nu + 1.0) / 2.0;

    vec4 uvwz = vec4(u_nu, u_mu_s, u_mu, u_r);
    float tex_coord_x = uvwz.x * (SCATTERING_TEXTURE_NU_SIZE - 1);
    float tex_x = floor(tex_coord_x);
    float lerp = tex_coord_x - tex_x;
    vec3 uvw0 = vec3((tex_x + uvwz.y) / SCATTERING_TEXTURE_NU_SIZE, uvwz.z, uvwz.w);
    vec3 uvw1 = vec3((tex_x + 1.0 + uvwz.y) / SCATTERING_TEXTURE_NU_SIZE, uvwz.z, uvwz.w);

    return vec3(texture(scattering_texture, uvw0) * (1.0 - lerp) + texture(scattering_texture, uvw1) * lerp);
}

vec3 get_scattering(
    const AtmosphereParameters atmosphere,
    sampler3D single_rayleigh_scattering_texture,
    sampler3D single_mie_scattering_texture,
    sampler3D multiple_scattering_texture,
    float r, float mu, float mu_s, float nu,
    bool ray_r_mu_intersects_ground,
    int scattering_order) {
    if (scattering_order == 1) {
        vec3 rayleigh = get_scattering(
            atmosphere, single_rayleigh_scattering_texture, r, mu, mu_s, nu,
            ray_r_mu_intersects_ground);
        vec3 mie = get_scattering(
            atmosphere, single_mie_scattering_texture, r, mu, mu_s, nu,
            ray_r_mu_intersects_ground);
        return rayleigh * rayleigh_phase_function(nu) + mie * cornette_shanks_phase_function(atmosphere.mie_phase_function_g, nu);
    } else {
        return get_scattering(
            atmosphere, multiple_scattering_texture, r, mu, mu_s, nu,
            ray_r_mu_intersects_ground);
    }
}
