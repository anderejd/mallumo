in vertexOut {
    vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 1) uniform Camera 
{
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 projection_view_matrix;
    vec4 position;
} camera;

in vec3 view_ray;

layout(location = 0) out vec3 color;

layout(location = 0) uniform vec3 sun_direction;
layout(binding = 0) uniform sampler2D transmittance_texture;
layout(binding = 1) uniform sampler3D scattering_texture;
layout(binding = 3) uniform sampler2D irradiance_texture;

// screen_position:
// 
// 0.0, 1.0       1.0, 1.0
//
//
// 0.0, 0.0       1.0, 0.0
vec3 ray_direction(vec2 screen_position) {
    vec2 ndc = screen_position * 2.0 - 1.0;

    vec4 ray_clip = vec4(ndc, -1.0, 1.0);
    
    vec4 ray_eye = inverse(camera.projection_matrix) * ray_clip;
    ray_eye = vec4(ray_eye.xy, -1.0, 0.0);

    return normalize((inverse(camera.view_matrix) * ray_eye).xyz);
}

void main() {
    vec3 ray_direction = ray_direction(vertex_out.texture_coordinate);
    vec3 ray_start = camera.position.xyz;
    vec3 transmittance;
    vec3 luminance = get_sky_luminance(camera.position.xyz, ray_direction, sun_direction, transmittance, transmittance_texture, scattering_texture);

    float sun = get_sun(ray_direction, sun_direction);
    vec3 sun_luminance = sun * globals.parameters.solar_irradiance * globals.parameters.sun_spectral_radiance_to_luminance;
    luminance += transmittance * sun_luminance;

    color = pow(filmic_postprocess(adjust_exposure(luminance)), vec3(1.0 / 2.2));
}
