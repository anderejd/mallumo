layout(location = 0) out vec3 transmittance;

void main() {
    vec2 uv = gl_FragCoord.xy / TRANSMITTANCE_TEXTURE_SIZE;

    float x_mu = tex_coord_to_unit_range(uv.x, TRANSMITTANCE_TEXTURE_WIDTH);
    float x_r = tex_coord_to_unit_range(uv.y, TRANSMITTANCE_TEXTURE_HEIGHT);
    float H = sqrt(globals.parameters.top_radius * globals.parameters.top_radius - globals.parameters.bottom_radius * globals.parameters.bottom_radius);
    float rho = H * x_r;
    float r = sqrt(rho * rho + globals.parameters.bottom_radius * globals.parameters.bottom_radius);
    float d_min = globals.parameters.top_radius - r;
    float d_max = rho + H;
    float d = d_min + x_mu * (d_max - d_min);
    float mu = d == 0.0 ?
          1.0 :
          (H * H - rho * rho - d * d) / (2.0 * r * d);
    mu = clamp(mu, -1.0, 1.0);

    const int SAMPLE_COUNT = 500;
    float dx = max(-r * mu + sqrt(max(r * r * (mu * mu - 1.0) + globals.parameters.top_radius * globals.parameters.top_radius, 0.0)), 0.0) / SAMPLE_COUNT;
    float optical_length_rayleigh = 0.0;
        for (int i = 0; i <= SAMPLE_COUNT; i++) {
        float d_i = i * dx;
        float r_i = sqrt(d_i * d_i + 2.0 * r * mu * d_i + r * r);
        float y_i = get_density(globals.parameters.rayleigh_density, r_i - globals.parameters.bottom_radius);
        float weight_i = i == 0 || i == SAMPLE_COUNT ? 0.5 : 1.0;
        optical_length_rayleigh += y_i * weight_i * dx;
    }
    float optical_length_mie = 0.0;
        for (int i = 0; i <= SAMPLE_COUNT; i++) {
        float d_i = i * dx;
        float r_i = sqrt(d_i * d_i + 2.0 * r * mu * d_i + r * r);
        float y_i = get_density(globals.parameters.mie_density, r_i - globals.parameters.bottom_radius);
        float weight_i = i == 0 || i == SAMPLE_COUNT ? 0.5 : 1.0;
        optical_length_mie += y_i * weight_i * dx;
    }
    float optical_length_absorption = 0.0;
        for (int i = 0; i <= SAMPLE_COUNT; i++) {
        float d_i = i * dx;
        float r_i = sqrt(d_i * d_i + 2.0 * r * mu * d_i + r * r);
        float y_i = get_density(globals.parameters.absorption_density, r_i - globals.parameters.bottom_radius);
        float weight_i = i == 0 || i == SAMPLE_COUNT ? 0.5 : 1.0;
        optical_length_absorption += y_i * weight_i * dx;
    }

    transmittance = exp(-(
        globals.parameters.rayleigh_scattering * optical_length_rayleigh +
        globals.parameters.mie_extinction * optical_length_mie +
        globals.parameters.absorption_extinction * optical_length_absorption));
}
