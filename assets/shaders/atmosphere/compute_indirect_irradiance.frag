layout(location = 0) out vec3 delta_irradiance;
layout(location = 1) out vec3 irradiance;

layout(binding = 0) uniform sampler3D single_rayleigh_scattering_texture;
layout(binding = 1) uniform sampler3D single_mie_scattering_texture;
layout(binding = 2) uniform sampler3D multiple_scattering_texture;

void main() {
    vec2 uv = gl_FragCoord.xy / IRRADIANCE_TEXTURE_SIZE;

    float r = globals.parameters.bottom_radius +
        tex_coord_to_unit_range(uv.y, IRRADIANCE_TEXTURE_HEIGHT) *
        (globals.parameters.top_radius - globals.parameters.bottom_radius);
    float mu_s = clamp(2.0 * tex_coord_to_unit_range(uv.x, IRRADIANCE_TEXTURE_WIDTH) - 1.0, -1.0, 1.0);

    const int SAMPLE_COUNT = 32;
    const float dphi = PI / SAMPLE_COUNT;
    const float dtheta = PI / SAMPLE_COUNT;
    vec3 delta_irr = vec3(0.0);
    vec3 omega_s = vec3(sqrt(1.0 - mu_s * mu_s), 0.0, mu_s);

    for (int j = 0; j < SAMPLE_COUNT / 2; j++) {
        float theta = (j + 0.5) * dtheta;
        for (int i = 0; i < 2 * SAMPLE_COUNT; i++) {
            float phi = (i + 0.5) * dphi;
            vec3 omega = vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta));
            float domega = dtheta * dphi * sin(theta);
            float nu = dot(omega, omega_s);
            delta_irr += get_scattering(globals.parameters, single_rayleigh_scattering_texture,
                single_mie_scattering_texture, multiple_scattering_texture,
                r, omega.z, mu_s, nu, false,
                globals.parameters.scattering_order) * omega.z * domega;
        }
    }

    delta_irradiance = delta_irr;
    irradiance = globals.parameters.luminance_from_radiance * delta_irradiance;
}
