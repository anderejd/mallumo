#version 450 core

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/consts.glsl
#include libs/vertices.glsl
#include libs/parameters.glsl
#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl

struct Light {
    vec4 pos_far;
    vec3 color;
    uint has_shadowmap;
};

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

in vertexOut {
  mat3 TBN;
  vec3 world_position;  
  vec2 texture_coordinate;
  vec3 normal;
} vertex_out;

layout(binding = 0) uniform sampler2D albedo_sampler;
layout(binding = 1) uniform sampler2D metallic_roughness_sampler;
layout(binding = 2) uniform sampler2D occlusion_sampler;
layout(binding = 3) uniform sampler2D normal_sampler;
layout(binding = 4) uniform sampler2D emissive_sampler;

layout (location = 0) out vec3 positions;
layout (location = 1) out vec4 albedos;
layout (location = 2) out vec3 orms;
layout (location = 3) out vec3 normals;
layout (location = 4) out vec3 emissions;

void main()
{
    vec4 albedo = primitive_parameters.albedo.rgba;
    if (primitive_parameters.has_albedo_texture == 1) {
        albedo *= texture(albedo_sampler, vertex_out.texture_coordinate).rgba;
    }

    if (albedo.a <= 0.001) {
        discard;
    }

    float metalness = primitive_parameters.metallic_roughness_refraction.r;
    float roughness = primitive_parameters.metallic_roughness_refraction.g;
    if (primitive_parameters.has_metallic_roughness_texture == 1) {
        metalness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).r;
        roughness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).g;
    }

    float occlusion = 1.0;
    if (primitive_parameters.has_occlusion_texture == 1) {
        occlusion = texture(occlusion_sampler, vertex_out.texture_coordinate).r;
    }

    vec3 normal = vertex_out.normal;
    if (primitive_parameters.has_normal_texture == 1) {
        normal = texture(normal_sampler, vertex_out.texture_coordinate).rgb;
        normal = normalize(normal * 2.0 - 1.0);
        normal = vertex_out.TBN * normal; 
    }
    normal = normalize(normal);

    vec3 emission = primitive_parameters.emission.rgb;
    if(primitive_parameters.has_emissive_texture == 1) {
        emission *= texture(emissive_sampler, vertex_out.texture_coordinate).rgb;
    }
    
    positions = vertex_out.world_position;
    albedos = albedo;
    orms = vec3(occlusion, roughness, metalness);
    normals = normal;
    emissions = emission;
}