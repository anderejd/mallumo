#version 450 core

#extension GL_ARB_shader_draw_parameters : enable
#extension GL_ARB_bindless_texture : enable

#define gl_DrawID gl_DrawIDARB

struct Vertex {
  vec4 position;
  vec3 normal;
  vec2 texture_coordinate;
  vec3 tangent;
  vec3 bitangent;
};

struct PrimitiveParameter {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness;             // 16B
  vec4 emission;                       // 16B
  
  uvec2 albedo_texture;                // 8B
  uvec2 metallic_roughness_texture;    // 8B
  uvec2 occlusion_texture;             // 8B
  uvec2 normal_texture;                // 8B
  uvec2 emissive_texture;              // 8B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  // Image based lighting
  uvec2 diffuse_ibl;                   // 8B
  uvec2 specular_ibl;                  // 8B

  // Padding
  uvec4 padding;                       //+16B == 192 (64*3)
};


layout(std140, binding = 0) uniform Globals 
{
    uint size;
} globals;

layout(rgba8, binding = 0) uniform coherent writeonly image3D voxel_texture;

layout ( binding = 0, offset = 0 ) uniform atomic_uint voxels_count;

layout(std430, binding = 0) buffer Indices
{
    int indices[];
} indices;

layout(std430, binding = 1) buffer VertexAttributes
{
    Vertex va[];
} vertex_attributes;

layout(std430, binding = 2) buffer PrimitiveParameters
{
    PrimitiveParameter parameters[];
} primitive_parameters;

out VertexOut {
    vec3 world_position;
    vec3 world_normal;
    vec2 texture_coordinate;
    mat3 TBN;

    flat int draw_id;
} vertex_out;

void main(void)
{
  int index = indices.indices[gl_VertexID];

  vec3 position = vertex_attributes.va[index].position.xyz;
  vec3 normal = vertex_attributes.va[index].normal.xyz;
  vec2 texture_coordinate = vertex_attributes.va[index].texture_coordinate.xy;
  vec3 tangent = vertex_attributes.va[index].tangent.xyz;
  vec3 bitangent = vertex_attributes.va[index].bitangent.xyz;

  mat4 model_matrix = primitive_parameters.parameters[gl_DrawID].model_matrix;
  mat3 normal_matrix = transpose(inverse(mat3(model_matrix)));

  vec3 T = normalize(normal_matrix * tangent);
  vec3 B = normalize(normal_matrix * bitangent);
  vec3 N = normalize(normal_matrix * normal);  
  mat3 TBN = mat3(T, B, N);

  vertex_out.world_position = vec3(model_matrix * vec4(position, 1.0f));
  vertex_out.world_normal = normalize(normal_matrix * normal);
  vertex_out.texture_coordinate = texture_coordinate;
  vertex_out.TBN = TBN;
  vertex_out.draw_id = gl_DrawID;

  gl_Position = vec4(vertex_out.world_position, 1.0);
}