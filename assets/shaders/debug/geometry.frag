// version
// parameters

in vertexOut {
  mat3 TBN;
  vec3 world_position; 
  vec2 texture_coordinate;
  vec3 normal;
} vertex_out;

in vec3 world_position;

layout(location = 0) out vec4 color;

void main()
{
    color = vec4(world_position, 1.0);    
}