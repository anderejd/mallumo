#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices=14) out;

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 camera_position;
} camera;

layout(std140, binding = 1) uniform Globals 
{
    uvec2 voxel_texture;
    uint size;
} globals;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

in vertexOut {
  flat vec4 color;
} vertex_out[];

out geometryOut {
  flat vec4 color;
} geometry_out;

const vec3 triangle_offsets[14] = {
  vec3(0.0, 1.0, 0.0),
  vec3(1.0, 1.0, 0.0),
  vec3(0.0, 0.0, 0.0),
  vec3(1.0, 0.0, 0.0),
  vec3(1.0, 0.0, 1.0),
  vec3(1.0, 1.0, 0.0),
  vec3(1.0, 1.0, 1.0),
  vec3(0.0, 1.0, 0.0),
  vec3(0.0, 1.0, 1.0),
  vec3(0.0, 0.0, 0.0),
  vec3(0.0, 0.0, 1.0),
  vec3(1.0, 0.0, 1.0),
  vec3(0.0, 1.0, 1.0),
  vec3(1.0, 1.0, 1.0),
};

void main()
{
    for(int i = 0; i < 14; i++)
    {
        geometry_out.color = vertex_out[0].color;
        gl_Position = camera.projection_view_matrix * (gl_in[0].gl_Position + vec4(triangle_offsets[i] / globals.size, 1.0));
        EmitVertex();
    }
    EndPrimitive();
}  