
#version 450

uint uvec3_to_uint(uvec3 val) {
    return (uint(val.z) & 0x000003FF)   << 20U
            |(uint(val.y) & 0x000003FF) << 10U 
            |(uint(val.x) & 0x000003FF);
}

layout(binding = 0) uniform sampler3D voxel_texture;

layout (binding = 1, offset = 0) uniform atomic_uint voxels_count;

layout(std430, binding = 0) buffer BufferPositions
{
  uint positions[];
} buffer_positions;

layout(std430, binding = 1) buffer BufferColors
{
  uint colors[];
} buffer_colors;

void main() {
  uint voxel_size = uint(textureSize(voxel_texture, 0).x);

  uint x = gl_VertexID % voxel_size;
  uint y = (gl_VertexID / voxel_size) % voxel_size;
  uint z = gl_VertexID / (voxel_size * voxel_size);

  vec4 voxel_color = texelFetch(voxel_texture, ivec3(x, y, z), 0);

  if(voxel_color != vec4(0.0, 0.0, 0.0, 0.0)) {
    uint index = atomicCounterIncrement(voxels_count);

    buffer_positions.positions[index] = uvec3_to_uint(uvec3(x, y, z));
    buffer_colors.colors[index] = packUnorm4x8(voxel_color);
  }
}
