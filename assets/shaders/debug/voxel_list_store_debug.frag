// include version.glsl
// include parameters.glsl
// include utility.glsl

in GeometryOut {
    vec3 world_position;
    vec3 world_normal;
    vec2 texture_coordinate;
    mat3 TBN;
    
    flat vec4 AABB;             // AABB of a triangle
    flat mat3 swizzle_matrix_inverse;
} geometry_in;

layout(std140, binding = 0) uniform Globals
{
    uint size;
} globals;

// Constants to key into worldaxes
const uint X = 0;
const uint Y = 1;
const uint Z = 2;

layout(binding = 0) uniform sampler2D albedo_sampler;
layout(binding = 1) uniform sampler2D metallic_roughness_sampler;
layout(binding = 2) uniform sampler2D occlusion_sampler;
layout(binding = 3) uniform sampler2D normal_sampler;
layout(binding = 4) uniform sampler2D emissive_sampler;

// Atomic counter 
layout ( binding = 5, offset = 0 ) uniform atomic_uint voxels_count;

layout (r32ui, binding = 0) uniform coherent volatile uimage3D voxel_texture;
//layout (r32ui, binding = 1) uniform coherent volatile uimage3D count_texture;

// https://rauwendaal.net/2013/02/07/glslrunningaverage/
void imageAtomicAverageRGBA8(layout(r32ui) coherent volatile uimage3D voxel_texture, ivec3 coord, vec3 nextVec3)
{
    uint nextUint = packUnorm4x8(vec4(nextVec3,1.0f/255.0f));
    uint prevUint = 0;
    uint currUint;
 
    vec4 currVec4;
 
    vec3 average;
    uint count;
 
    //"Spin" while threads are trying to change the voxel
    while((currUint = imageAtomicCompSwap(voxel_texture, coord, prevUint, nextUint)) != prevUint)
    {
        prevUint = currUint;                    //store packed rgb average and count
        currVec4 = unpackUnorm4x8(currUint);    //unpack stored rgb average and count
 
        average =      currVec4.rgb;        //extract rgb average
        count   = uint(currVec4.a*255.0f);  //extract count
 
        //Compute the running average
        average = (average*count + nextVec3) / (count+1);
 
        //Pack new average and incremented count back into a uint
        nextUint = packUnorm4x8(vec4(average, (count+1)/255.0f));
    }
}

vec3 scale_and_bias(vec3 point) { 
    return 0.5f * point + vec3(0.5f); 
}

void main() 
{  
    ivec2 viewport_size = imageSize(voxel_texture).xy;

    vec2 aabb_min = floor((geometry_in.AABB.xy * 0.5 + 0.5) * viewport_size);
	vec2 aabb_max = ceil((geometry_in.AABB.zw * 0.5 + 0.5) * viewport_size);

	if (all(greaterThanEqual(gl_FragCoord.xy, aabb_min)) && all(lessThanEqual(gl_FragCoord.xy, aabb_max)))
	{
        vec4 albedo = primitive_parameters.albedo.rgba;
        if (primitive_parameters.has_albedo_texture == 1) {
            albedo *= texture(albedo_sampler, geometry_in.texture_coordinate).rgba;
        }

        if (albedo.a <= 0.001) {
            discard;
        }

        float metalness = primitive_parameters.metallic_roughness_refraction.r;
        float roughness = primitive_parameters.metallic_roughness_refraction.g;
        if (primitive_parameters.has_metallic_roughness_texture == 1) {
            metalness *= texture(metallic_roughness_sampler, geometry_in.texture_coordinate).r;
            roughness *= texture(metallic_roughness_sampler, geometry_in.texture_coordinate).g;
        }

        float occlusion = 1.0;
        if (primitive_parameters.has_occlusion_texture == 1) {
            occlusion = texture(occlusion_sampler, geometry_in.texture_coordinate).r;
        }

        vec3 normal = geometry_in.world_normal;
        if (primitive_parameters.has_normal_texture == 1) {
            normal = texture(normal_sampler, geometry_in.texture_coordinate).rgb;
            normal = normalize(normal * 2.0 - 1.0);
            normal = geometry_in.TBN * normal; 
        }
        normal = normalize(normal);

        vec3 emission = primitive_parameters.emission.rgb;
        if(primitive_parameters.has_emissive_texture == 1) {
            emission *= texture(emissive_sampler, geometry_in.texture_coordinate).rgb;
        }

        vec3 voxel_position = geometry_in.swizzle_matrix_inverse * vec3(gl_FragCoord.xy, gl_FragCoord.z * viewport_size.x);

        uint index = atomicCounterIncrement(voxels_count);

        imageAtomicAverageRGBA8(voxel_texture, ivec3(voxel_position), albedo.rgb);
    } else {
        discard;
    }
}