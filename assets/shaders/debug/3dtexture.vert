#version 450 core

out gl_PerVertex
{
  vec4 gl_Position;
};

layout(std140, binding = 0) uniform Globals 
{
    uint size;
} globals;

layout(std140, binding = 1) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 camera_position;
} camera;

out vertexOut {
  flat uvec3 voxel_position;
} vertex_out;

void main(void)
{
  uint x = gl_VertexID % globals.size;
  uint y = (gl_VertexID / globals.size) % globals.size;
  uint z = gl_VertexID / (globals.size * globals.size);

  //float ratio = globals.size;

  float f = 2.0 / (float(globals.size) + 1.0);
  float x_center = -1.0 + f + x * f;
  float y_center = -1.0 + f + y * f;
  float z_center = -1.0 + f + z * f;

  //vec3 world_position = 2 * vec3((float(x)/ ratio) - 0.5, (float(y) / ratio) - 0.5, (float(z) / ratio));
  vertex_out.voxel_position = uvec3(x, y, z);

  gl_Position = vec4(x_center, y_center, z_center, 1.0);
}