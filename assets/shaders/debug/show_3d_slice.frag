#version 450 core

in vertexOut {
  vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 0) uniform Globals {
    uint lod;
    uint slice;
};

layout(binding = 0) uniform sampler3D tex;

layout (location = 0) out vec4 color;

void main()
{
    int size = int(textureSize(tex, int(lod)).x);

    vec3 tex_color = textureLod(tex, vec3(vertex_out.texture_coordinate, float(slice) / float(size)), float(lod)).rgb;

    color = vec4(tex_color, 1.0);
}