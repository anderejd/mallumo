
// TODO BINDING
// layout(std430, binding = ?) buffer NodePoolNext
// {
//     uint nodepool_next[];
// };

int traverse_octree_lod(
    in vec3 position, 
    in uint target_level,
    out vec3 node_position_min, 
    out vec3 node_position_max, 
    out uint parent_address, 
    out vec3 parent_position_min, 
    out vec3 parent_position_max
) {
  
  // Clear the out-parameters
  node_position_min = vec3(0.0);
  node_position_max = vec3(1.0);
  parent_address = 0;
  parent_position_min = vec3(0.0);
  parent_position_max = vec3(1.0);
  
  float side_length = 1.0;
  int node_address = 0;

  for (uint iLevel = 0; iLevel < target_level; ++iLevel) {
    uint nodeNext = nodepool_next[node_address];

    uint child_start_address = nodeNext & NODE_MASK_VALUE;
    
    if (child_start_address == 0U) {
        node_address = uint(NODE_NOT_FOUND);
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * voxel_position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

    side_length = side_length / 2.0;
    parent_address = node_address;
    node_address = int(child_start_address + offset);

    parent_position_min = node_position_min;
    node_position_min += vec3(child_offsets[offset]) * vec3(side_length);

    parent_position_max = node_position_max;
    node_position_max = node_position_min + vec3(side_length);
    position = 2.0 * position - vec3(offset_vec);
  }

  return node_address;
}