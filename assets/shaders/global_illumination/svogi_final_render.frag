// version   2
// pbr     136
// consts    2

///
/// CONSTS
///
const uint pow2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
const float node_sizes[] = {1, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625, 0.001953125, 0.0009765625};

#define NODE_MASK_VALUE 0x3FFFFFFF
#define NODE_MASK_TAG (0x00000001 << 31)
#define NODE_MASK_BRICK (0x00000001 << 30)
#define NODE_MASK_TAG_STATIC (0x00000003 << 30)
#define NODE_NOT_FOUND 0xFFFFFFFF

const uvec3 child_offsets[8] = {
  uvec3(0, 0, 0),
  uvec3(1, 0, 0),
  uvec3(0, 1, 0),
  uvec3(1, 1, 0),
  uvec3(0, 0, 1),
  uvec3(1, 0, 1),
  uvec3(0, 1, 1),
  uvec3(1, 1, 1)
};

const vec4 diffuse_cones[6] =
{
    vec4(0.000000, -0.500000, 0.866025, 0.523599),
    vec4(0.823639, -0.500000, 0.267617, 0.523599),
    vec4(0.509037, -0.500000, -0.700629, 0.523599),
    vec4(-0.509037, -0.500000, -0.700629, 0.523599),
    vec4(-0.823639, -0.500000, 0.267617, 0.523599),
    vec4(0.000000, -1.000000, 0.000000, 0.523599)
};

const vec3 diffuseConeDirections[] =
{
    vec3(0.0f, 1.0f, 0.0f),
    vec3(0.0f, 0.5f, 0.866025f),
    vec3(0.823639f, 0.5f, 0.267617f),
    vec3(0.509037f, 0.5f, -0.7006629f),
    vec3(-0.50937f, 0.5f, -0.7006629f),
    vec3(-0.823639f, 0.5f, 0.267617f)
};

const float diffuseConeWeights[] =
{
    5.0f * PI / 20.0f,
    3.0f * PI / 20.0f,
    3.0f * PI / 20.0f,
    3.0f * PI / 20.0f,
    3.0f * PI / 20.0f,
    3.0f * PI / 20.0f,
};

///
/// INPUTS
///

in VertexOut {
    vec2 texture_coordinate;
} vertex_out;

layout(std140, binding = 0) uniform Globals
{
    uint grid_resolution;
    uint levels;
} globals;

layout(std140, binding = 1) uniform Camera 
{
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 projection_view_matrix;
    vec4 position;
} camera;

layout(std140, binding = 2) uniform Sun {
    mat4 space_matrix;
    vec4 color;
    vec4 position;
} sun;

// Nodepool
layout(std430, binding = 0) buffer NodePoolNext
{
    uint nodepool_next[];
};

layout(std430, binding = 1) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

layout(std430, binding = 2) buffer Cones
{
    vec4 cones[];
};

// G-Buffer
layout(binding = 0) uniform sampler2D position_texture;
layout(binding = 1) uniform sampler2D albedo_texture;
layout(binding = 2) uniform sampler2D orm_texture;
layout(binding = 3) uniform sampler2D normal_texture;
layout(binding = 4) uniform sampler2D emission_texture;

// Shadowmap
layout(binding = 5) uniform sampler2D sun_shadowmap;

// Brickpool
layout(binding = 6) uniform sampler3D brickpool_albedos;
layout(binding = 7) uniform sampler3D brickpool_irradiances;

///
/// OUTPUTS
///

// Result
layout (location = 0) out vec4 color;

///
/// FUNCTIONS
///

float in_sun_shadow(vec4 sun_space_position, vec3 normal)
{
    // perform perspective divide
    vec3 projection_coords = sun_space_position.xyz / sun_space_position.w;

    // transform to [0,1] range
    projection_coords.xy = projection_coords.xy * 0.5 + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closest_depth = texture(sun_shadowmap, projection_coords.xy).r; 

    // get depth of current fragment from light's perspective
    float current_depth = projection_coords.z;

    float shadow = 0.0;
    vec2 texel_size = 1.0 / textureSize(sun_shadowmap, 0);
    for(int x = -6; x <= 6; ++x)
    {
        for(int y = -6; y <= 6; ++y)
        {
            float depth = texture(sun_shadowmap, projection_coords.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth < depth ? 1.0 : 0.0;
        }    
    }
    shadow /= 13.0 * 13.0;

    return shadow;
}

void correct_alpha(inout vec4 color, in float alpha_correction) {
    float color_old = color.a;
    color.a = 1.0 - pow(1.0 - color.a, alpha_correction);
    color.xyz = color.xyz * (color.a / clamp(color_old, 0.0001, 10000.0));
}

bool intersect_ray_with_aabb(in vec3 origin,
                             in vec3 direction,
                             in vec3 box_min, 
                             in vec3 box_max,
                             out float t_enter,
                             out float t_leave)
{
    vec3 temp_min = (box_min - origin) / direction; 
    vec3 temp_max = (box_max - origin) / direction;
    
    vec3 v3_max = max(temp_max, temp_min);
    vec3 v3_min = min(temp_max, temp_min);
    
    t_leave = min (v3_max.x, min (v3_max.y, v3_max.z));
    t_enter = max (max (v3_min.x, 0.0), max (v3_min.y, v3_min.z));    
    
    return t_leave > t_enter;
}

// Take 'vector and' move it onto the plane formed by 'normal'
vec3 transform_normal(vec3 vector, vec3 normal)
{
    // Generate tangent and bitangent           
    vec3 c1 = cross(normal, vec3(0.0, 0.0, 1.0));
    vec3 c2 = cross(normal, vec3(0.0, 1.0, 0.0));

    vec3 tangent = normalize(dot(c1, c1) > dot(c2, c2) ? c1 : c2);
    vec3 bitangent = normalize(cross(normal, tangent));

    // Create TBN matrix to transform vector into normal space, so that
    // they form around the normal
    mat3 tbn = mat3(tangent, normal, bitangent);

    return tbn * vector;
}

ivec3 brick_address(int node_address)
{
    const uint brickpool_size = textureSize(brickpool_albedos, 0).x / 3;

    return ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * (node_address / (brickpool_size * brickpool_size))
    );
}

int traverse_octree_level(in vec3 position, 
                          in uint target_level,
                          inout vec3 node_position_min,
                          inout vec3 node_position_max,
                          inout int parent_address,
                          inout vec3 parent_min,
                          inout vec3 parent_max)
{  
    // Clear the out-parameters
    node_position_min = vec3(0.0);
    node_position_max = vec3(1.0);

    parent_address = 0;
    parent_min = vec3(0.0);
    parent_max = vec3(1.0);
  
    int node_address = 0;
    float side_length = 1.0;

    position = clamp(position, vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0));
    
    for (uint level = 0; level < target_level; ++level) {
        uint node = nodepool_next[node_address] & NODE_MASK_VALUE;

        // if current node points to *null* (0) nodepool
        uint child_start_address = node & NODE_MASK_VALUE;
        if (child_start_address == 0U) {
            node_address = int(NODE_NOT_FOUND);
            break;
        }
    
        uvec3 offset_vec = uvec3(2.0 * position);
        uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

        side_length = side_length / 2.0; 

        parent_address = node_address;
        node_address = int(child_start_address + offset);

        parent_min = node_position_min;
        parent_max = node_position_max;

        node_position_min += vec3(child_offsets[offset]) * vec3(side_length);
        node_position_max = node_position_min + vec3(side_length);
       
        position = 2.0 * position - vec3(offset_vec);
    }

    return node_address;
}


vec4 cone_trace(vec3 ray_origin, vec3 normal, vec3 ray_dir, float cone_half_angle, float max_distance) {
    float t_enter = 0.0;
    float t_leave = 0.0;

    vec3 min_child_node = vec3(0.0);
    vec3 max_child_node = vec3(1.0);
    vec3 min_parent_node = vec3(0.0);
    vec3 max_parent_node = vec3(1.0);

    const float brickpool_size = float(textureSize(brickpool_irradiances, 0).x);
    const float voxel_step = 1.0 / brickpool_size;

    ray_origin += normal * node_sizes[globals.levels - 1];
    
    if (!intersect_ray_with_aabb(ray_origin, ray_dir, vec3(0.0), vec3(1.0), t_enter, t_leave)) {
        return vec4(0.0, 0.0, 0.0, 0.0);
    }

    vec4 output_color = vec4(0.0);

    float t = t_enter + node_sizes[globals.levels - 1];
    while(output_color.a < 1.0 && t < t_leave && t <= max_distance) {
        const vec3 position = ray_origin + ray_dir * t;
        const float cone_diameter = 2.0f * tan(cone_half_angle) * t;

        // Calculate expected mipmap level(+node size) based on distance
        float node_size = clamp(cone_diameter, node_sizes[globals.levels - 1], 1.0);
        const float sample_lod = clamp(log2(1.0 / cone_diameter), 0.0, float(globals.levels) - 1.00001);
            
        // Readjust mipmap level(+it's associated size) based on actual available sizes of mipmaps
        const uint child_level = uint(ceil(sample_lod));
        node_size = node_sizes[child_level];

        int parent_address;
        int child_address = traverse_octree_level(position, child_level, 
                                                min_child_node, max_child_node, 
                                                parent_address, min_parent_node, max_parent_node);

        t += (1.0f / 3.0f) * cone_diameter;

        if (child_address == int(NODE_NOT_FOUND)) {
            continue;
        }

        // Find bricks coressponding to child and parent nodes
        const ivec3 child_brick_address = brick_address(child_address);
        const ivec3 parent_brick_address = brick_address(parent_address);

        // Texture position of brick
        const vec3 child_brick_enter = (vec3(child_brick_address) + 0.5) / brickpool_size;
        const vec3 parent_brick_enter = (vec3(parent_brick_address) + 0.5) / brickpool_size;

        // Local position inside the child and parent node in texture space
        const vec3 child_node_position = (position - min_child_node) / node_size;
        const vec3 parent_node_position = (position - min_parent_node) / (node_size * 2.0);

        // Calculate global texture position in brickpool
        const vec3 child_interpolate_position = child_brick_enter + child_node_position * (2.0 * voxel_step);
        const vec3 parent_interpolate_position = parent_brick_enter + parent_node_position * (2.0 * voxel_step);

        // Retrieve trilineraly interpolated values from child and parent bricks
        vec4 child_color = texture(brickpool_irradiances, child_interpolate_position).rgba;
        vec4 parent_color = texture(brickpool_irradiances, parent_interpolate_position).rgba;

        // Alpha correct
        const float alpha_correction = float(pow2[child_level + 1]) / float(pow2[globals.levels]);

        correct_alpha(child_color, alpha_correction);
        correct_alpha(parent_color, alpha_correction * 2);

        // Mix them into one
        const vec4 new_color = mix(parent_color, child_color, fract(sample_lod));
        output_color += (1.0 - output_color.a) * new_color;
    }
  
    return output_color;
}

vec4 gather_indirect_illumination(in vec3 position, in vec3 normal, float max_distance) {
    vec4 color = vec4(0);

    vec3 coneDirection = vec3(0.0f);

    // diffuse cone setup
    const float aperture = 0.57735f;
    vec3 guide = vec3(0.0f, 1.0f, 0.0f);

    if (abs(dot(normal,guide)) == 1.0f)
    {
        guide = vec3(0.0f, 0.0f, 1.0f);
    }

    // Find a tangent and a bitangent
    vec3 right = normalize(guide - dot(normal, guide) * normal);
    vec3 up = cross(right, normal);

    for(int i = 0; i < 6; i++)
    {
        coneDirection = normal;
        coneDirection += diffuseConeDirections[i].x * right + diffuseConeDirections[i].z * up;
        coneDirection = normalize(coneDirection);

        // cumulative result
        color += diffuseConeWeights[i] * cone_trace(position, normal, coneDirection, aperture, max_distance);
    }

    return color;
}

vec4 cone_trace_ao(in vec3 ray_origin, in vec3 ray_dir, in float cone_half_angle, in float max_distance) {
    float t_enter = 0.0;
    float t_leave = 0.0;

    vec3 min_child_node = vec3(0.0);
    vec3 max_child_node = vec3(1.0);
    vec3 min_parent_node = vec3(0.0);
    vec3 max_parent_node = vec3(1.0);

    const float brickpool_size = float(textureSize(brickpool_irradiances, 0).x);
    const float voxel_step = 1.0 / brickpool_size;
    
    if (!intersect_ray_with_aabb(ray_origin, ray_dir, vec3(0.0), vec3(1.0), t_enter, t_leave)) {
        return vec4(0);
    }

    float output_alpha = 0.0;
    float node_size_step = node_sizes[globals.levels];
    for (float t = t_enter + node_sizes[globals.levels - 1]; 
        t < t_leave; 
        t += (1.0 / 3.0) * node_size_step) 
    {
        const vec3 position = ray_origin + ray_dir * t;
        const float cone_diameter = 2.0 * tan(cone_half_angle) * t;

        // Calculate expected mipmap level(+node size) based on distance
        node_size_step = clamp(cone_diameter, node_sizes[globals.levels - 1], 1.0);
        const float sample_lod = clamp(log2(1.0 / node_size_step), 0.0, float(globals.levels) - 1.00001);
            
        // Readjust mipmap level(+it's associated size) based on actual available sizes of mipmaps
        const uint child_level = uint(ceil(sample_lod));
        node_size_step = node_sizes[child_level];

        int parent_address;
        int child_address = traverse_octree_level(position, child_level, 
                                                min_child_node, max_child_node, 
                                                parent_address, min_parent_node, max_parent_node);

        if (child_address == int(NODE_NOT_FOUND)) {
            continue;
        }

        // Find bricks coressponding to child and parent nodes
        const ivec3 child_brick_address = brick_address(child_address);
        const ivec3 parent_brick_address = brick_address(parent_address);

        // Texture position of brick
        const vec3 child_brick_enter = (vec3(child_brick_address) + 0.5) / brickpool_size;
        const vec3 parent_brick_enter = (vec3(parent_brick_address) + 0.5) / brickpool_size;

        // Local position inside the child and parent node in texture space
        const vec3 child_node_position = (position - min_child_node) / node_size_step;
        const vec3 parent_node_position = (position - min_parent_node) / (node_size_step * 2.0);

        // Calculate global texture position in brickpool
        const vec3 child_interpolate_position = child_brick_enter + child_node_position * (2.0 * voxel_step);
        const vec3 parent_interpolate_position = parent_brick_enter + parent_node_position * (2.0 * voxel_step);

        // Retrieve trilineraly interpolated values from child and parent bricks
        float child_alpha = texture(brickpool_albedos, child_interpolate_position).a;
        float parent_alpha = texture(brickpool_albedos, parent_interpolate_position).a;

        // Alpha correct
        const float alpha_correction = float(pow2[child_level + 1]) / float(pow2[globals.levels]);
        child_alpha = 1.0 - pow(1.0 - child_alpha, alpha_correction);
        parent_alpha = 1.0 - pow(1.0 - parent_alpha, alpha_correction * 2.0);

        // Mix them into one
        const float new_alpha = mix(parent_alpha, child_alpha, fract(sample_lod));
        output_alpha += (1.0 - output_alpha) * new_alpha;

        // Break if there is accumulated enough alpha or we are too far from the beggining of the ray
        if (output_alpha > 0.99 || (max_distance > 0.000001 && t >= max_distance)) {
            break;
        }
    }
  
    return vec4(output_alpha, output_alpha, output_alpha, output_alpha);
}

vec4 gather_indirect_illumination_ao(in vec3 position, in vec3 normal, float max_distance) {
    vec4 color = vec4(0);

    for(int i = 0; i < cones.length(); i++) {
        vec3 cone = transform_normal(vec3(cones[i]), normal);

        color += (1.0 / float(cones.length())) * cone_trace_ao(position, cone, (cones[i]).w, max_distance);
    }

    return color;
}

void main()
{
    vec3 position = texture(position_texture, vertex_out.texture_coordinate).xyz;

    // PBR values
    vec4 albedo = texture(albedo_texture, vertex_out.texture_coordinate);
    vec3 orm = texture(orm_texture, vertex_out.texture_coordinate).rgb;
    float occlusion = orm.r;
    float roughness = orm.g;
    float metalness = orm.b;
    vec3 normal = normalize(texture(normal_texture, vertex_out.texture_coordinate).xyz);
    vec3 emission = texture(emission_texture, vertex_out.texture_coordinate).xyz;

    if (albedo == vec4(0.0, 0.0, 0.0, 0.0)) {
        discard;
    }

    // Output color
    vec3 Lo = vec3(0.0);

    // Direct Light
    vec3 N = normal;
    vec3 V = normalize(vec3(camera.position) - position);
    vec3 L = normalize(sun.position.xyz); 
    vec3 H = normalize(V + L);
    Lo += (1.0 - in_sun_shadow(sun.space_matrix * vec4(position, 1.0), N)) * irradiance(N, H, V, L, sun.color.rgb, albedo, roughness, metalness);

    // Indirect bounce
    Lo += vec3(albedo) * vec3(gather_indirect_illumination(position * 0.5 + vec3(0.5), normal, 1.0));

    // Ambient occlusion
    // float ao = gather_indirect_illumination_ao(position * 0.5 + vec3(0.5), normal, 3.0).a;

    // Lo = Lo * (1.0 - ao);

    // Lo += emission;

    // Specular
    // Lo += cone_trace();

    // Reinhard
    Lo = Lo / (Lo + vec3(1.0));

    // Gamma correction
    Lo = pow(Lo, vec3(1.0/2.2));

    color = vec4(Lo, 1.0);
    //color = vec4(vec3(1.0 - ao), 1.0);
    // color = vec4(normal * 0.5 + vec3(0.5), 1.0);
}