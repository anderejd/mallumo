#version 450 core

#define NODE_MASK_VALUE 0x3FFFFFFF
#define NODE_NOT_FOUND 0xFFFFFFFF

out vertexOut {
  vec3 view_dir;
  float pixel_size;
  vec2 texture_coordinate;
} vertex_out;

const uvec3 child_offsets[8] = {
  uvec3(0, 0, 0),
  uvec3(1, 0, 0),
  uvec3(0, 1, 0),
  uvec3(1, 1, 0),
  uvec3(0, 0, 1),
  uvec3(1, 0, 1),
  uvec3(0, 1, 1),
  uvec3(1, 1, 1)
};

const uint pow2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
const float node_sizes[] = {1, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625, 0.001953125, 0.0009765625};

layout(std140, binding = 0) uniform Globals 
{
  uint grid_resolution;
  uint leaf_node_resolution;
  uint levels;
  float cone_angle;
} globals;

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout (location = 0) out vec4 color;

bool intersect_ray_with_aabb(
  in vec3 origin, 
  in vec3 direction,
  in vec3 box_min, 
  in vec3 box_max,
  out float t_enter,
  out float t_leave
) {
    vec3 temp_min = (box_min - origin) / direction; 
    vec3 temp_max = (box_max - origin) / direction;
    
    vec3 v3_max = max(temp_max, temp_min);
    vec3 v3_min = min(temp_max, temp_min);
    
    t_leave = min (v3_max.x, min (v3_max.y, v3_max.z));
    t_enter = max (max (v3_min.x, 0.0), max (v3_min.y, v3_min.z));    
    
    return t_leave > t_enter;
}

int traverse_octree_lod(
    in vec3 position, 
    in uint target_level,
    out vec3 node_position_min, 
    out vec3 node_position_max, 
    out uint parent_address, 
    out vec3 parent_position_min, 
    out vec3 parent_position_max
) {
  
  // Clear the out-parameters
  node_position_min = vec3(0.0);
  node_position_max = vec3(1.0);
  parent_address = 0;
  parent_position_min = vec3(0.0);
  parent_position_max = vec3(1.0);
  
  float side_length = 1.0;
  int node_address = 0;

  for (uint iLevel = 0; iLevel < target_level; ++iLevel) {
    uint nodeNext = nodepool_next[node_address];

    uint child_start_address = nodeNext & NODE_MASK_VALUE;
    
    if (child_start_address == 0U) {
        node_address = uint(NODE_NOT_FOUND);
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * voxel_position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

    side_length = side_length / 2.0;
    parent_address = node_address;
    node_address = int(child_start_address + offset);

    parent_position_min = node_position_min;
    node_position_min += vec3(child_offsets[offset]) * vec3(side_length);

    parent_position_max = node_position_max;
    node_position_max = node_position_min + vec3(side_length);
    position = 2.0 * position - vec3(offset_vec);
  }

  return node_address;
}

vec4 correct_alpha(in vec4 color, in float alpha_correction) {
  const float old_col_alpha = color.a;
  color.a = 1.0 - pow((1.0 - color.a), alpha_correction);
  color.xyz *= color.a / clamp(old_col_alpha, 0.0001, 10000.0);
  return color;
}

// requires number of levels
vec4 raycast_brick(
  in uint node_id,
  in vec3 enter,
  in vec3 leave,
  in vec3 dir,
  in uint level,
  in float nodeSideLength
) {
    float brick_resolution = 512.0;
    float voxel_step = 1.0 / 512.0;
    vec4 color = vec4(0);

    ivec3 brick_coordinates = ivec3(
        3 * (node_id % 512),
        3 * ((node_id / 512) % 512),
        3 * (node_id / (512 * 512))
    );
    vec3 brick_coordinates_tex = vec3(brick_coordinates) / brick_resolution + vec3(voxel_step / 2.0);
    
    // sampling voxel position
    //
    //          |VOXEL  STEP|
    // _______________________________
    // |        , - ~ ~ ~ - ,  LEAVE |
    // |    , ' |           | ' O    |
    // |  ,     |           |     ,  |
    // | ,------|-----------|------, |
    // |,       |           |       ,|
    // |,       |           |       ,|
    // |,       |           |       ,|
    // | ,------|-----------|------, |
    // |  ,     |           |     ,  |
    // |    O   |           |  , '   |
    // | ENTER' - , _ _ _ ,  '       |
    // -------------------------------

    // 2 * voxel_step == diameter
    vec3 enter_tex = brick_coordinates_tex + enter * (2 * voxel_step);
    vec3 leave_tex = brick_coordinates_tex + leave * (2 * voxel_step);
    
    // calculating step size, more sampling rate, more steps
    const float sampling_rate = 3.0;
    const float step_length = length(leave_tex - enter_tex);
    const float step_size = voxel_step / sampling_rate;

    const float f_sample_count = step_length / step_size;
    const int i_sample_count = int(ceil(f_sample_count));

    // Higher alphaCorrection -> higher alpha
    float alpha_correction = float(1 << num_levels) /
                            (float(1 << (level + 1)) * sampling_rate);
    
    for (int i = 0; i < i_sample_count; ++i) {
      vec3 sample_pos = enter_tex + dir * step_size * float(i);

      vec4 new_color;
      new_color = texture(brickpool_irradiance, sample_pos);

      if (i == i_sample_count - 1) {
        alpha_correction *= fract(f_sample_count);
      }

      color += (1 - color.a) * correct_alpha(new_col, alpha_correction);

      if (color.a > 0.99) {
         break;
      }
    }
    
  return color;
}

vec4 cone_trace(in vec3 ray_origin_tex, in vec3 ray_dir_tex, in float cone_diameter, in float max_distance) {
  vec4 return_color = vec4(0);
  
  float t_enter = 0.0;
  float t_leave = 0.0;
    
  // if ray is outside of the scene
  if (!intersectRayWithAABB(ray_origin_tex, ray_dir_tex, vec3(0.0), vec3(1.0), t_enter, t_leave)) {
    return return_color;
  }
  
  // limit to start of ray in case ray starts inside the box
  t_enter = max(t_enter, 0.0);

  float end = t_leave;
  float sample_diameter = 0.0;
  float e = 0.00001;
  for (float f = t_enter + node_sizes[numLevels - 1]; f < end; ) {
    vec3 pos_tex = (ray_origin_tex + ray_dir_tex * f);
    
    float target_size = cone_diameter * f;
    
    sample_diameter = clamp(target_size, 1.0 / float(LEAF_NODE_RESOLUTION), 1.0);
    float sample_lod = clamp(abs(log2(1.0 / sample_diameter)), 0.0, float(numLevels) - 1.00001);
    
    uint parent_address = uint(NODE_NOT_FOUND);
    uint child_level = uint(ceil(sample_lod));

    uint child_address = traverse_octree_lod(pos_tex, child_level, vec3(0.0), vec3(1.0), parent_address, vec3(0.0), vec3(1.0));

    bool advance = intersect_ray_with_aabb(pos_tex, ray_dir_tex, vec3(0.0), vec3(1.0), t_enter, t_leave);
        
    if (child_address != int(NODE_NOT_FOUND)) {
       float child_size = node_sizes[child_level];
       
       vec3 child_enter = pos_tex / child_size;
       vec3 child_leave = (pos_tex + ray_dir_tex * t_leave)  / child_size;
       
       vec3 parent_enter = pos_tex / (child_size * 2.0);
       vec3 parent_leave = (pos_tex + ray_dir_tex * t_leave) / (child_size * 2.0);
       
       vec4 c_col = raycast_brick(child_address, child_enter, child_leave, ray_dir_tex, child_level, child_size);
       vec4 p_col = raycast_brick(parent_address, parent_enter, parent_leave, ray_dir_tex, child_level - 1, child_size * 2);
       vec4 new_col = mix(p_col, c_col, fract(sample_lod));

       return_color += (1.0 - return_color.a) * new_col;
       if (return_color.a > 0.99 || (max_distance > 0.000001 && f >= max_distance)) {
         break;
       }
    }

    f += t_leave + e;
        
    if (!advance) {
        break; // prevent infinite loop
    }
  }

  return return_color;
}

void main() {
  vec3 view_dir_world = normalize((inverse(camera.view_matrix) * vec4(vertex_in.view_dir, 0.0)).xyz);
  
  vec3 ray_origin_tex = camera.position.xyz * 0.5 + 0.5;
  
  float pixelSizeTS = coneAngle;

  color = coneTrace(ray_origin_tex, view_dir_world, pixelSizeTS, 0.0);
}