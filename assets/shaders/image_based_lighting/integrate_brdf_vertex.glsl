#version 450 core

layout(std430, binding = 0) buffer Indices
{
    int indices[];
} indices;

layout(std430, binding = 1) buffer VertexAttributes
{
    vec4 va[];
} vertexAttributes;

out vertexOut { 
  vec2 texture_coordinate;
} vertex_out;

void main()
{
    int index = indices.indices[gl_VertexID];

    vec3 position = vertexAttributes.va[index * 2].xyz;
    vec2 texture_coordinate = vertexAttributes.va[index * 2 + 1].xy;

    vertex_out.texture_coordinate = texture_coordinate;

	gl_Position = vec4(position, 1.0);
}