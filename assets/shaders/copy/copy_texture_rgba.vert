#version 450 core

in vertexOut {
  vec2 texture_coordinate;
} vertex_out;

layout(binding = 0) uniform sampler2D tex;

layout (location = 0) out vec4 color;

void main()
{
    color = texture(tex, vertex_out.texture_coordinate);
}