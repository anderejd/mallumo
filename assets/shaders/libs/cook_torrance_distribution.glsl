///
/// Normal Distribution Function
///
///                                   α^2
/// Trowbridge-Reitz GGX = ----------------------------
///                        π( (n⋅h)^2 * (α^2 − 1) + 1)^2
///
/// N - normal vector 
/// H - half-way vector
/// a - α = roughness
///
float distributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;
	
    return nom / denom;
}

float cook_torrance_distribution(vec3 N, vec3 H, float roughness)
{
    return distributionGGX(N, H, roughness);
}
