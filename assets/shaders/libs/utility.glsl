#define NODE_MASK_VALUE 0x3FFFFFFF
#define NODE_MASK_TAG (0x00000001 << 31)
#define NODE_MASK_BRICK (0x00000001 << 30)
#define NODE_MASK_TAG_STATIC (0x00000003 << 30)
#define NODE_NOT_FOUND 0xFFFFFFFF

const uvec3 child_offsets[8] = {
  uvec3(0, 0, 0),
  uvec3(1, 0, 0),
  uvec3(0, 1, 0),
  uvec3(1, 1, 0),
  uvec3(0, 0, 1),
  uvec3(1, 0, 1),
  uvec3(0, 1, 1),
  uvec3(1, 1, 1)
};

const uint pow2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
const float node_sizes[] = {1, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125, 0.00390625, 0.001953125, 0.0009765625};

uint uvec3_to_uint(uvec3 val) {
    return (uint(val.z) & 0x000003FF)   << 20U
            |(uint(val.y) & 0x000003FF) << 10U 
            |(uint(val.x) & 0x000003FF);
}

uvec3 uint_to_uvec3(uint val) {
    return uvec3(uint((val & 0x000003FF)),
                 uint((val & 0x000FFC00) >> 10U), 
                 uint((val & 0x3FF00000) >> 20U));
}

bool is_flagged(in uint node) {
  return (node & NODE_MASK_TAG) != 0U;
}

bool has_brick(in uint node) {
  return (node & NODE_MASK_BRICK) != 0;
}

bool next_empty(in uint node) {
  return (node & NODE_MASK_VALUE) == 0U;
}

bool intersect_ray_with_aabb(
  in vec3 origin, 
  in vec3 direction,
  in vec3 box_min, 
  in vec3 box_max,
  out float t_enter,
  out float t_leave
) {
    vec3 temp_min = (box_min - origin) / direction; 
    vec3 temp_max = (box_max - origin) / direction;
    
    vec3 v3_max = max(temp_max, temp_min);
    vec3 v3_min = min(temp_max, temp_min);
    
    t_leave = min (v3_max.x, min (v3_max.y, v3_max.z));
    t_enter = max (max (v3_min.x, 0.0), max (v3_min.y, v3_min.z));    
    
    return t_leave > t_enter;
}