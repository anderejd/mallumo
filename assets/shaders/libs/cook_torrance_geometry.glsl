/// 
/// Geometry Function
/// 
///                    n⋅v
/// SchlickGGX = ---------------
///                (n⋅v)(1−k)+k
/// 
/// k - remapping of α based on whether one uses direct or IBL lighting
///     for direct k = (α + 1)^2 / 8
///     for IBL    k = (α^2) / 2
/// N ⋅ V
///
float geometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

/// 
/// Smith's method for taking into account the view direction(geometry obstruction) 
/// and the light direction vector(geometry shadowing)
/// 
/// G(n, v, l, roughness) = SchlickGGX(n, v, roughness) * SchlickGGX(n, l, roughness)
/// 
float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx1 = geometrySchlickGGX(NdotV, roughness);
    float ggx2 = geometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

float cook_torrance_geometry(vec3 N, vec3 V, vec3 L, float roughness)
{
    return geometrySmith(N, V, L, roughness);
}
