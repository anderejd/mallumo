struct Vertex {
    vec4 position;
    vec4 normal;
    vec4 texture_coordinate;
    vec4 tangent;
};

layout(std430, binding = INDICES_BINDING) buffer Indices
{
    int indices[];
};

layout(std430, binding = VERTICES_BINDING) buffer VertexAttributes
{
    Vertex vertices[];
};
